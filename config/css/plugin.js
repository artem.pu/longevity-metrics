// const postcss = require('postcss');
const {splitCssBundle} = require('./css');
const HtmlWebpackPlugin = require('html-webpack-plugin');
class ExplodeCssPlugin {
    static defaultOptions = {
        disabled: false,
    };
    constructor(options = {}) {
      this.options = {
            ...ExplodeCssPlugin.defaultOptions,
            ...options
        };
    }

    getBundleSource(assets) {
        const bundles = Object.keys(assets).filter(filename => /\.css$/.test(filename));
        const sources = bundles.map(filename => {
            const asset = !filename ? null : assets[filename];
            const src = asset.sourceAndMap ? asset.sourceAndMap() : { source: asset.source() };
            return {filename, source: src?.source || null};
        });
        return sources;
    }

    cssChunksList = [];
    async splitCssBundle(compiler, compilation) {
        const {sources} = compiler.webpack;
        const {hash, assets} = compilation;

        const bundles = this.getBundleSource(assets); // {filename, source}[]
        const bundleSource = bundles.map(bs => bs.source).join('\n');
        const sourceChunks = await splitCssBundle(bundleSource);

        if (!sourceChunks) return;

        // добавляем чанки в сборку
        this.cssChunksList = sourceChunks.map((source, k) => {
            const chunkPath = `css/chunk${k+1}.${hash}.css`;
            compilation.emitAsset(chunkPath, new sources.RawSource(source, true));
            return chunkPath;
        });

        // убираем бандлы из сборки
        bundles.forEach(b => compilation.deleteAsset(b.filename));
    }

    // добавляем ссылки на чанки в HtmlWebpackPlugin
    injectCssLinks(data, callback) {
        const baseUrl = '/';
        const assets = [...data.assets.css];
        const chunks = this.cssChunksList || [];

        chunks.forEach(path => assets.push(`${baseUrl}${path}`));

        data.assets.css = assets;
        callback(null, data);
    }

    apply(compiler) {
        if (this.options.disabled) return;

        const pluginName = ExplodeCssPlugin.name;
        compiler.hooks.thisCompilation.tap(pluginName, (compilation) => {
            compilation.hooks.additionalAssets.tapPromise(pluginName, (assets) => this.splitCssBundle(compiler, compilation));
            const htmlHooks = HtmlWebpackPlugin.getHooks(compilation);
            htmlHooks.beforeAssetTagGeneration.tapAsync(pluginName, (data, callback) => this.injectCssLinks(data, callback));
        });
    }
}
module.exports = ExplodeCssPlugin;