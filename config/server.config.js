const baseCfg = require('./base.config');
const {merge} = require('webpack-merge');
const { WebpackManifestPlugin } = require('webpack-manifest-plugin');
const nodeExternals = require('webpack-node-externals');

module.exports = merge(baseCfg, {
    target: 'node',
    entry: {main: './src/entry-server.ts'},
    output: {
        publicPath: '/',
        filename: '[name].js',
        libraryTarget: 'commonjs2',
    },
    module: {
        rules: [
            {test: /\.css$/i, loader: 'null-loader'},
            {test: /\.less$/i, loader: 'null-loader'},
        ]
    },
    externals: nodeExternals({allowlist: /\.(css|vue)$/}),
    plugins: [
        new WebpackManifestPlugin({fileName: 'vue-ssr-client-manifest.json'}),
    ],
    optimization: {
        splitChunks: false,
        runtimeChunk: false,
    },
});
