const path = require('path');
const webpack = require('webpack');
const { merge } = require('webpack-merge');
const common = require('./client.config.js');

module.exports = merge(common, {
    devtool: 'inline-source-map',
    output: {
        path: path.resolve(__dirname, '../dist'),
        publicPath: '/',
        assetModuleFilename: 'asset/[name].[hash][ext]',
        filename: '[name].js',
    },

    // Spin up a server for quick development
    watchOptions: {
        poll: false, // true | 1000ms,
        aggregateTimeout: 250,
    },

    devServer: {
        headers: {
            'Content-Security-Policy': 'frame-ancestors \'self\' https://autofill.yandex.ru',
            'X-Frame-Options': 'ALLOW-FROM https://autofill.yandex.ru',
        },
        historyApiFallback: true,
        compress: true,
        open: false,
        port: 8080,
        hot: true,
        static: [
            {
                publicPath: '/static',
                directory: path.resolve(__dirname, '../static')
            },
            {
                publicPath: '/',
                directory: path.resolve(__dirname, '../static')
            }
        ],
        setupMiddlewares: (middlewares, devServer) => {
            // pipeline-id
            if (!devServer) throw new Error('webpack-dev-server is not defined');
            devServer.app.get('/pipeline-id/', (req, res) => res.json('100500'));

            return middlewares;
        },
    },

    plugins: [
        // new webpack.HotModuleReplacementPlugin(),
    ],
})
