const path = require('path');
const webpack = require('webpack');
const { VueLoaderPlugin } = require('vue-loader');
const TerserPlugin = require('terser-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
// const ESLintPlugin = require('eslint-webpack-plugin');

/**
 * append .env to process environment
 */
if (!process.env.NODE_ENV) process.env.NODE_ENV = 'production';
const ProcessEnvExtend = require('./dotenv');
const processEnv = new ProcessEnvExtend();

/**
 * apply environment
 */
const PROD = process.env.NODE_ENV === 'production';
const MINIFY = process.env.MINIFY === 'minify';
const PUBLIC_PATH = process.env.PUBLIC_PATH || '/';
const PATH = path.resolve(__dirname, `../build/`);

/**
 * trace environment
 */
const trace = 'build'
    + `-${PROD ? 'production' : 'development'}`
    + `${MINIFY ? '-minified' : ''}`;
console.log(`\x1b[93mCOMPILE \x1b[96m${trace}\x1b[93m to ${PATH} path:${PUBLIC_PATH}\x1b[0m`);

module.exports = {
    mode: PROD ? 'production' : 'development',
    devtool: PROD ? false : 'source-map',
    stats: {
        // preset: 'errors',
        entrypoints: false,
        cachedAssets: false,
        children: false,
        assets: false,
        publicPath: false,
        modules: false,
        moduleTrace: false,
        outputPath: true,
        version: true,
    },
    output: {
        path: PATH,
        publicPath: `${PUBLIC_PATH}`,
        assetModuleFilename: 'asset/[name].[hash][ext]',
        pathinfo: true,
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    extractCSS: true,
                    optimizeSSR: true,
                    sourceMap: !PROD,
                    cssSourceMap: !PROD,
                    compilerOptions: {
                        whitespace: 'condense',
                    },
                },
            },
            {
                test: /\.ts$/,
                loader: 'ts-loader',
                options: {
                    transpileOnly: true,
                    appendTsSuffixTo: [/\.vue$/],
                },
            },
            {
                test: /\.(png|jp?eg|gif|webp)(\?.*)?$/,
                type: 'asset/resource',
                generator: {filename: 'img/[name].[hash][ext][query]'},
            },
            {
                test: /\.(woff?2|ttf|eot)(\?.*)?$/,
                type: 'asset/resource',
                generator: {filename: 'font/[name].[hash][ext][query]'},
            },
            {
                test: /\.(svg)(\?.*)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[hash].[ext]',
                            outputPath: 'svg',
                            // publicPath: `${PUBLIC_PATH}svg`,
                        },
                    },
                ],
            },
        ],
    },
    optimization: {
        runtimeChunk: false, // 'single'
        minimize: !!MINIFY,
        minimizer: !MINIFY ? [] : [
            new TerserPlugin({
                parallel: true,
                extractComments: false,
                terserOptions: {
                    keep_classnames: true,
                    compress: {keep_classnames: true},
                },
            }),
        ],
        splitChunks: !MINIFY ? false : {
            chunks: 'all',
            name: 'bundle',
            minChunks: 1,
            minSize: 30e3,
            maxSize: 244e3,
            automaticNameDelimiter: '.',
            cacheGroups: {
                // default: {
                //     minChunks: 2,
                //     priority: -20,
                //     reuseExistingChunk: true,
                //     filename: '[name]-chunk.js',
                // },
                default: false,
                commons: {
                    test: /\.js$/,
                    chunks: 'all',
                    minChunks: 2,
                    name: 'common',
                    enforce: true,
                },
                defaultVendors: {
                    test: /[\\/]node_modules[\\/]/,
                    priority: -10,
                    reuseExistingChunk: true,
                    filename: '[name]-vendor.js',
                },
            },
        },
    },
    performance: {hints: false},
    resolve: {
        symlinks: false,
        alias: {
            '@': path.resolve(__dirname, '../src'),
            vue: path.resolve(__dirname, `../node_modules/vue`),
            'vue$': 'vue/dist/vue.esm-bundler.js',
        },
        extensions: ['.ts', '.js', '.vue', '.json'],
        modules: [path.resolve(__dirname, '../node_modules')],
    },
    plugins: [
        new VueLoaderPlugin(),
        processEnv.webpackPlugin(),
        new CleanWebpackPlugin({
            dry: false,
            verbose: false,
            cleanOnceBeforeBuildPatterns: ['**/*'],
        }),
        new webpack.DefinePlugin({
            BASE_URL: PUBLIC_PATH,
            __VUE_OPTIONS_API__: true,
            __VUE_PROD_DEVTOOLS__: false,
        }),
        // new ESLintPlugin({
        //     extensions: ['ts', 'vue'],
        //     failOnError: PROD ? true : false,
        //     exclude: ['node_modules/'],
        //     emitWarning: false,
        //     quiet: true,
        // }),
    ],
};
