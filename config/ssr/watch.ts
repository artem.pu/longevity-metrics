import webpack from 'webpack';

const watchOptions = {aggregateTimeout: 300, poll: undefined};
const target:string = process.env.APP_TARGET || 'client';
const config = target === 'server' ? require('../server.config.js') : require('../client.config.js');
const compiler = webpack(config);

// compiler.hooks.beforeRun.tap('MyPlugin', (params) => {
//     console.log('beforeRun');
// });
compiler.watch(watchOptions, (err, stats) => {
    const options = {swallowErrors: false};
    const payload:{type: string, data: any} = {
        type: err ? 'error' : 'rebuild',
        data: err || 'success',
    };
    process.send!(payload, null, options, (e:Error|null) => !e ? null : console.log('PROCESS_SEND_REJECT', e));
});