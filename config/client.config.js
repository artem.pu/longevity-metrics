const path = require('path');
const webpack = require('webpack');
const baseCfg = require('./base.config');
const {merge} = require('webpack-merge');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ExplodeCssPlugin = require('./css/plugin');
const postcssLess = require('postcss-less');

const PROD = baseCfg.mode === 'production';
const CLOUD = process.env.CLOUD_STATIC !== 'local';
const MINIFY = process.env.MINIFY === 'minify';

const srcDir = path.resolve(__dirname, '../src/');
const buildRoot = path.resolve(baseCfg.output.path, '../build');

// нужно добавить путь
// const publicPath = CLOUD ? `${process.env.CLOUD_STATIC_HOST}/site/client/` : baseCfg.output.publicPath;

const cssOptions = {url: true, sourceMap: !PROD, esModule: false};
module.exports = merge(baseCfg, {
    target: PROD ? 'browserslist' : 'web',
    entry: {main: './src/entry-client.ts'},
    output: {
        // publicPath,
        filename: PROD ? `js/[name].[fullhash].js` : 'js/[name].[fullhash].js',
    },
    module: {
        rules: [
            {
                test: /\.css$/i,
                use: [
                    {loader: MiniCssExtractPlugin.loader},
                    {loader: 'css-loader', options: cssOptions},
                ]
            },
            {
                test: /\.less$/i,
                use: [
                    {loader: MiniCssExtractPlugin.loader},
                    {loader: 'css-loader', options: cssOptions},
                    {loader: 'less-loader', options: {lessOptions: {strictMath: true}}},
                    {loader: 'postcss-loader', options: {
                        postcssOptions: {syntax: postcssLess},
                    }},
                ]
            },
        ]
    },
    optimization: {
        minimizer: !MINIFY ? [`...`] : [
            `...`,
            new CssMinimizerPlugin({
                minimizerOptions: {
                    preset: ["default", {discardComments: {removeAll: true}}],
                },
            }),
        ],
        splitChunks: {
            cacheGroups: {
                styles: {
                    name: "bundle",
                    type: "css/mini-extract",
                    chunks: "all",
                    enforce: true,
                    reuseExistingChunk: true,
                },
            },
        },
    },
    plugins: [
        new webpack.ProvidePlugin({Promise: 'es6-promise'}),
        new MiniCssExtractPlugin({
            filename: 'css/[name].[fullhash].css',
            ignoreOrder: true,
        }),
        // new ExplodeCssPlugin({disabled: !MINIFY}),
        // new CopyWebpackPlugin({
        //     patterns: [
        //         {
        //             from: path.resolve(srcDir, '../static'),
        //             to: path.join(buildRoot, `./${process.env.STATIC_BUNDLE_PATH}`),
        //         },
        //         {
        //             from: path.resolve(srcDir, '../node_modules/simplebar/dist/simplebar.esm.js.map'),
        //             to: path.resolve(baseCfg.output.path, './js/simplebar.esm.js.map'),
        //         },
        //     ],
        // }),
        new HtmlWebpackPlugin({
            minfy: true,
            xhtml: true,
            // title: '',
            favicon: path.resolve(srcDir, '../static/favicon.png'),
            template: path.resolve(srcDir, '../static/index.template.html'),
            templateParameters: {
                props: {title: '🐼'},
            },
        }),
    ]
});
