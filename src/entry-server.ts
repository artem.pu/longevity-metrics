import { createVueApp } from './entry';
import { RouteLocationRaw } from 'vue-router';
// import SSRContext from './types/server';
// import {CrawlerLabel} from './utils/crawler';

export default (context:any) => {
    return new Promise((resolve, reject) => {
        const {app, router, store} = createVueApp(true);

        const url:RouteLocationRaw = context?.url || '/';
        router.push(url).catch(error => console.log(`ENTRY_ERROR ${error}`));

        const readyResolve = function () {
            // const robot = (context as any)?.robot;
            // if (!robot || robot === CrawlerLabel.lighthouse) context.rendered = () => context.state = store.state;
            resolve({app, router, store});
        };
        router.isReady().then(readyResolve).catch(reject);
    });
};
