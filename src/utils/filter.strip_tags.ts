// strip_tags
export default function (value:string = '') {
    const naked = value.replace(/<[^>]*>/g, '');
    return naked.trim();
}
