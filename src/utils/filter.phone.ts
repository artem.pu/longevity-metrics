const filterPhone = function (phoneString: string): string {

    const phoneNumber = phoneString.replace(/\D/g, '');
    const number = phoneNumber.substr(-10);

    let countryCode = phoneNumber.replace(number, '');
    if (countryCode === '7' || countryCode === '8') countryCode = '+7';

    const re = /([0-9]{3})([0-9]{3})([0-9]{4})/;
    const nums = re.exec(number) || [];

    if (!nums[1] || !nums[2] || !nums[3]) return phoneString;
    return `${countryCode || '+7'} (${nums[1] || 'XXX'}) ${nums[2] || 'XXX'} ${nums[3] || 'XXXX'}`;
};

export default filterPhone;