const capitalize = function (string?:string) {
    if (!string || typeof string !== 'string') return string;
    return string.replace(/\w\S*/g, (w:string) => (w.replace(/^\w/, (c) => c.toUpperCase())));
};

const filterCapitalize = function (value: string) {
    if (!value) return '';
    return value.toString().charAt(0).toUpperCase() + value.toString().slice(1).toLowerCase();
};

export {
    filterCapitalize as default,
    filterCapitalize,
    capitalize,
};
