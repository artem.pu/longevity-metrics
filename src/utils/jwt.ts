interface AuthTokenJSON {
    [key:string]: any;
}

// nodejs polyfill
function polyAtob(a:string):string {
    let b = '';
    if (typeof atob !== 'undefined') b = atob(a);
    // @ts-ignore
    if (typeof Buffer !== 'undefined') b = Buffer.from(a, 'base64').toString('binary');
    return b;
}

const JWT = {
    decode(tokenValue:string):AuthTokenJSON|null {

        const token = tokenValue.split('.')[1];
        if (!token) return null;

        const maper = (c:string) => `%${('00'.concat(c.charCodeAt(0).toString(16))).slice(-2)}`;
        const strArr = polyAtob(token.replace(/-/g, '+').replace(/_/g, '/')).split('').map(maper);

        let payload = null;
        try { payload = decodeURIComponent(strArr.join('')); }
        catch (e) { console.error('JWT decode error'); }
        if (!payload) return null;

        let tokenJson:AuthTokenJSON|null = null;
        try { tokenJson = JSON.parse(payload); }
        catch (e) { console.error('JWT decode parse error'); }

        let tokenData:AuthTokenJSON|null = null;
        if (tokenJson) {
            let expires:Date|undefined = new Date(tokenJson.exp * 1e3);
            if (isNaN(expires.getTime())) expires = new Date(Date.now() + 3600000); // 1h
            tokenData = {...tokenJson, expires};
        }

        return tokenData;
    },
};

export {JWT as default, JWT};
