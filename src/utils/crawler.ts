enum CrawlerLabel {
    lighthouse = 'lighthouse',
}
const crawlersNames = [
    'facebookexternalhit',
    'duckduckbot',
    'baiduspider',
    'ia_archiver',
    'sogou',
];
const crawlersLabel = crawlersNames.concat([
    'exabot',
    'yahoo! slurp',
    'bingbot',
    'ahrefsbot',
    'googlebot',
    'yandexbot',
    'yandexmobilebot',
    'lighthouse',
]);

function getCrawlerByUserAgent(agent?:string):CrawlerLabel|string|false {
    const userAgent = agent || (typeof window !== 'undefined' ? window.navigator.userAgent : '');
    if (!userAgent) return 'unknown'; // клиент без UA считается роботом

    let crawler:string|undefined;

    crawler = crawlersNames.find((name:string) => userAgent.startsWith(name));
    if (crawler) return crawler;

    const compatible = !userAgent ? null : new RegExp('compatible; (.*);').exec(userAgent.toLowerCase());
    if (compatible) {
        const label = compatible[1].split('/')[0];
        crawler = crawlersLabel.find((ua:string) => label.indexOf(ua) >= 0);
    }

    if (!crawler) {
        if (-1 !== userAgent.indexOf('Google Page Speed Insights')) crawler = CrawlerLabel.lighthouse;
        else if (-1 !== userAgent.indexOf('Chrome-Lighthouse')) crawler = CrawlerLabel.lighthouse;
        else if (/(bot|crawl|spider)/i.test(userAgent)) crawler = userAgent;
    }

    return crawler || false;
}

export {
    CrawlerLabel,
    getCrawlerByUserAgent as default,
    getCrawlerByUserAgent,
};
