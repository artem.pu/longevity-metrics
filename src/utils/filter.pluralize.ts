const filterPluralize = function (value:number, n1:string = '', n2:string = '', s5?:string) {
    const n5 = s5 !== undefined ? s5 : n2;
    let num = Math.abs(value);
    num %= 100;
    if (num >= 5 && num <= 20) return n5;
    num %= 10;
    return num === 1 ? n1 : (num >= 2 && num <= 4 ? n2 : n5);
};

export default filterPluralize;
