export async function loadScript(src:string, globalObjName?:string):Promise<any> {

    const script = window.document.createElement('script');
    script.setAttribute('async', '');
    script.setAttribute('defer', '');
    script.setAttribute('src', src);

    const promise = new Promise<void>((resolve, reject) => {
        script.onerror = (e:string|Event) => reject(e.toString());
        script.onabort = (e:string|Event) => reject(e.toString());
        script.onload = () => resolve(globalObjName ? window[globalObjName] : undefined);
    });

    const head = document.querySelector('head');
    script.setAttribute('src', src);
    head?.appendChild(script);
    console.info(`insert script "${src}"`);

    return promise;
}
