import {CookieOptions, SameSite} from '@/types/cookie';

const cookie = {
    get: (name: string): string | undefined => {
        if ('undefined' === typeof document) return;
        const matches = 'undefined' !== typeof document ? document.cookie.match(new RegExp(
            `(?:^|; )${name.replace(/([.$?*|{}()\[\]\\\/+^])/g, '\\$1')}=([^;]*)`,
        ))
            : undefined;
        if (!matches || !matches[1]) return undefined;

        let value = undefined;
        try { value = decodeURIComponent(matches[1]); } catch (e) {e;}
        return value;
    },
    set: (name: string, value: string, options:CookieOptions = {}) => {
        if ('undefined' === typeof document) return;
        let cookie = `${name}=${encodeURIComponent(value)}`;

        if (options.expires) {
            const date = new Date();
            date.setTime(date.getTime() + options.expires * 60 * 1000);
            cookie += `; Expires=${date.toUTCString()}`;
        }

        if (options.expireDate) {
            const date = new Date(options.expireDate as Date|string|number);
            cookie += `; Expires=${date.toUTCString()}`;
        }
        document.cookie = `${cookie}; path=${options.path || '/'}; SameSite=${SameSite.Lax}`;
    },
    del: (name:string, path:string = '/', domain?:string) => {
        if ('undefined' === typeof document) return;
        const value = cookie.get(name);
        const expires = -1;
        if (value) cookie.set(name, value, {path, domain, expires});
    },
};

enum CookieName {
    FOUND_NOTIFY_OFF = 'aru.disallowFoundNotify',
}

export {CookieName, cookie, cookie as default};
