const filterPrice = function (price:string, float?:number): string {
    const parsePrice = Number.prototype.toFixed.call(parseFloat(price) || 0, float || 0);
    return parsePrice.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ');
};

export default filterPrice;