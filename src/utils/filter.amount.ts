const filterAmount = function (number:number, float:null|number = null, space:string = '\u00A0'): string {
    const fractionDigits = (!float && float !== 0) ? undefined : float;
    const value = Number.prototype.toFixed.call(number, fractionDigits);
    const string = value.toString();
    const [num, frac] = string.split('.');
    return num.replace(/(\d)(?=(\d{3})+(?!\d))/g, `$1${space}`).concat(frac || '');
};

export default filterAmount;