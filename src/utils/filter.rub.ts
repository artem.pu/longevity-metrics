/*
symbol - дописать знак рубля
zeroPennies - выводить 00 копеек
*/
const filterRub = function (sum:number, symbol:boolean = true, zeroPennies:boolean = false): string {

    const money = !sum ? 0 : parseFloat(sum.toString());

    let value:string = money.toFixed(2);
    if (!isNaN(money)) {
        const roubles = Math.trunc(sum);
        const pennies = Math.abs(Math.round((sum - roubles) * 1e2));
        value = roubles.toString();
        if (pennies > 0 || zeroPennies) value += `.${pennies < 10 ? '0' : ''}${pennies}`;
    }

    return symbol ? `${value}\u00A0\u20BD` : value;
};

export default filterRub;