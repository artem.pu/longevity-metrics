/* eslint-disable no-underscore-dangle */
import {createVueApp} from './entry';
import {NavigationFailure} from 'vue-router';

// this is callback redirect from yandex OAuth form
// throw message to parent, then close
const hash = (window.location.hash || '').substring(1);
if (window.opener && hash.indexOf('access_token=') !== -1) {
    window.opener.postMessage(hash, location.origin);
    window.close();
}

const {app, router, store} = createVueApp();

const INITIAL_STATE = (window as any).__INITIAL_STATE__;
if (INITIAL_STATE) console.log('initial', {...INITIAL_STATE});
if (INITIAL_STATE) store.replaceState(INITIAL_STATE);

// Убрать __INITIAL_STATE__ из window
delete (window as any).__INITIAL_STATE__;

// Убрать __INITIAL_STATE__ из HTML
const scripts: HTMLCollectionOf<HTMLScriptElement> = document.getElementsByTagName('script');
for (const script of Array.from(scripts)) {
    const {parentNode, innerText} = script;
    if (!parentNode || !innerText || innerText.indexOf('window.__INITIAL_STATE__') === -1) continue;
    else parentNode.removeChild(script);
}

router.isReady().then(() => {
    const mount = () => {
        if (document.readyState !== 'loading') app.mount('#app', true);
        else document.addEventListener('DOMContentLoaded', () => app.mount('#app', true));
    };

    const {href, origin, hash} = window.location;
    const path:string = href.replace(hash, '').replace(origin, '') || '/';
    const rout = router.resolve(path);

    if (path === rout.fullPath) return mount();

    router.push(path).then((fail?:NavigationFailure|void) => {
        if (fail) console.error('router start error', fail);
        mount();
    });
});