export enum MetricLabels {
    /** The total number of sessions. */
    'ga:sessions' = 'Sessions',
    /** The total number of pageviews for the property. */
    'ga:pageviews' = 'Pageviews',
    /** Total number of 1-day active users for each day in the requested time period.
     * At least one of ga:nthDay, ga:date, or ga:day must be specified as a dimension to query this metric.
     * For a given date, the returned value will be the total number of unique users
     * for the 1-day period ending on the given date. */
    'ga:1dayUsers' = '1dayUsers',
}