import {Store} from 'vuex';
import {RootState} from '@/types/root-state';
import {Router, RouteLocation} from 'vue-router';

declare module '@vue/runtime-core' {
    interface ComponentCustomProperties {
        $data: {[key:string]: any};
        $ssrContext: {[key:string]: any};
        $router: Router;
        $route: RouteLocation;
        $store: Store<RootState>;
    }
}
