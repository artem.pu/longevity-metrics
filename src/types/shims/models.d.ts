declare namespace Models {
    export interface ProblemDetails {
        [name: string]: unknown;

        type?: string | null;
        title?: string | null;
        status?: null | number; // int32
        detail?: string | null;
        instance?: string | null;
    }

    export interface BsonObjectId {
        readonly timestamp?: number; // int32
        readonly machine?: number; // int32
        readonly pid?: number; // int32
        readonly increment?: number; // int32
        readonly creationTime?: string; // date-time
    }

    namespace Net {
        export type HttpStatusCode =
            'Continue'
            | 'SwitchingProtocols'
            | 'Processing'
            | 'EarlyHints'
            | 'OK'
            | 'Created'
            | 'Accepted'
            | 'NonAuthoritativeInformation'
            | 'NoContent'
            | 'ResetContent'
            | 'PartialContent'
            | 'MultiStatus'
            | 'AlreadyReported'
            | 'IMUsed'
            | 'Ambiguous'
            | 'Moved'
            | 'Redirect'
            | 'RedirectMethod'
            | 'NotModified'
            | 'UseProxy'
            | 'Unused'
            | 'TemporaryRedirect'
            | 'PermanentRedirect'
            | 'BadRequest'
            | 'Unauthorized'
            | 'PaymentRequired'
            | 'Forbidden'
            | 'NotFound'
            | 'MethodNotAllowed'
            | 'NotAcceptable'
            | 'ProxyAuthenticationRequired'
            | 'RequestTimeout'
            | 'Conflict'
            | 'Gone'
            | 'LengthRequired'
            | 'PreconditionFailed'
            | 'RequestEntityTooLarge'
            | 'RequestUriTooLong'
            | 'UnsupportedMediaType'
            | 'RequestedRangeNotSatisfiable'
            | 'ExpectationFailed'
            | 'MisdirectedRequest'
            | 'UnprocessableEntity'
            | 'Locked'
            | 'FailedDependency'
            | 'UpgradeRequired'
            | 'PreconditionRequired'
            | 'TooManyRequests'
            | 'RequestHeaderFieldsTooLarge'
            | 'UnavailableForLegalReasons'
            | 'InternalServerError'
            | 'NotImplemented'
            | 'BadGateway'
            | 'ServiceUnavailable'
            | 'GatewayTimeout'
            | 'HttpVersionNotSupported'
            | 'VariantAlsoNegotiates'
            | 'InsufficientStorage'
            | 'LoopDetected'
            | 'NotExtended'
            | 'NetworkAuthenticationRequired';
    }

    namespace Admin {
        namespace Action {
            /**
             * Модель входящих параметров для редактирования акции
             */
            export interface EditActionParams {
                /**
                 * Id акции
                 */
                id: string;
                /**
                 * Имя
                 */
                name: string;
                /**
                 * Символьный код
                 */
                url: string;
                /**
                 * Дата начала
                 */
                startDate: string; // date-time
                /**
                 * Дата окончания
                 */
                endDate: string; // date-time
                /**
                 * Список городов (опционально)
                 */
                iPharmTownIds?: string[] | null;
                /**
                 * Список Uid'ов товаров или Id'ов групп (опционально)
                 */
                goodUids?: string[] | null;
                /**
                 * URL для апи запроса товаров (опционально)<br />
                 * Примеры:<br />
                 * Search/CategoryUrl?categoryUrl=derma_cosmetics%2Fsunscreen<br />
                 * Search/ByPhrase?phrase=yjigf<br />
                 * Brand/Items?brandUrl=NUTRILON<br />
                 * Search/Mnn?interNames=%D0%9C%D0%B5%D1%82%D1%80%D0%BE%D0%BD%D0%B8%D0%B4%D0%B0%D0%B7%D0%BE%D0%BB<br />
                 */
                apiSearchUrlPath?: string | null;
                /**
                 * Описание акции
                 */
                body?: string | null;
                /**
                 * Включить в рассылку
                 */
                toMailing?: boolean;
            }

            /**
             * Модель входящих параметров для создания акции
             */
            export interface NewActionParams {
                /**
                 * Имя
                 */
                name: string;
                /**
                 * Символьный код
                 */
                url: string;
                /**
                 * Дата начала
                 */
                startDate: string; // date-time
                /**
                 * Дата окончания
                 */
                endDate: string; // date-time
                /**
                 * Список городов (опционально)
                 */
                iPharmTownIds?: string[] | null;
                /**
                 * Список Uid'ов товаров или Id'ов групп (опционально)
                 */
                goodUids?: string[] | null;
                /**
                 * URL для апи запроса товаров (опционально)<br />
                 * Примеры:<br />
                 * Search/CategoryUrl?categoryUrl=derma_cosmetics%2Fsunscreen<br />
                 * Search/ByPhrase?phrase=yjigf<br />
                 * Brand/Items?brandUrl=NUTRILON<br />
                 * Search/Mnn?interNames=%D0%9C%D0%B5%D1%82%D1%80%D0%BE%D0%BD%D0%B8%D0%B4%D0%B0%D0%B7%D0%BE%D0%BB<br />
                 */
                apiSearchUrlPath?: string | null;
                /**
                 * Описание акции
                 */
                body?: string | null;
                /**
                 * Включить в рассылку
                 */
                toMailing?: boolean;
            }

            /**
             * Акция
             */
            export interface ShortActionInfoWithTownNamesModel {
                /**
                 * Список городов(опционально)
                 */
                cityNames?: string[] | null;
                /**
                 * Тело (HTML)
                 */
                body?: string | null;
                /**
                 * URL для апи запроса товаров(опционально)
                 * Примеры:
                 * Search/CategoryUrl?categoryUrl=derma_cosmetics%2Fsunscreen
                 * Search/ByPhrase?phrase=yjigf
                 * Brand/Items?brandUrl=NUTRILON
                 * Search/Mnn?interNames=%D0%9C%D0%B5%D1%82%D1%80%D0%BE%D0%BD%D0%B8%D0%B4%D0%B0%D0%B7%D0%BE%D0%BB
                 */
                apiSearchUrlPath?: string | null;
                /**
                 * Акция действует в этом городе
                 */
                activeInThisCity?: boolean;
                /**
                 * Список городов(опционально)
                 */
                iPharmTownIds?: string[] | null;
                /**
                 * Кол-во просмотров
                 */
                viewsCount?: number; // int32
                /**
                 * Идентификатор
                 */
                id?: string | null;
                /**
                 * Изображение
                 */
                photoPath?: string | null;
                /**
                 * Имя
                 */
                name?: string | null;
                /**
                 * Дата начала
                 */
                startDate?: string; // date-time
                /**
                 * Дата окончания
                 */
                endDate?: string; // date-time
                /**
                 * Урл акции
                 */
                url?: string | null;
                /**
                 * Акция скоро закончится
                 */
                readonly hotOffer?: boolean;
            }

            /**
             * Акция
             */
            export interface ShortActionModel {
                /**
                 * Идентификатор
                 */
                id?: string | null;
                /**
                 * Изображение
                 */
                photoPath?: string | null;
                /**
                 * Имя
                 */
                name?: string | null;
                /**
                 * Дата начала
                 */
                startDate?: string; // date-time
                /**
                 * Дата окончания
                 */
                endDate?: string; // date-time
                /**
                 * Урл акции
                 */
                url?: string | null;
                /**
                 * Акция скоро закончится
                 */
                readonly hotOffer?: boolean;

                viewsCount?: number | null;
            }
        }
        namespace AdminUserManagment {
            /**
             * Параметры запроса для создания администратора
             */
            export interface AddAdminUserRequest {
                /**
                 * Имя
                 */
                userName?: string | null;
                /**
                 * Пароль
                 */
                password?: string | null;
                /**
                 * Роли
                 */
                roles?: /* Административная роль */ Mongo.AdminRole[] | null;
            }

            /**
             * Запрос для авторизации авдминистратора
             */
            export interface AuthAdminRequest {
                /**
                 * Токен
                 */
                userName?: string | null;
                /**
                 * Время жизни
                 */
                password?: string | null;
            }

            /**
             * Ответ для авторизации авдминистратора
             */
            export interface AuthAdminResponse {
                /**
                 * Токен
                 */
                readonly token?: string | null;
                /**
                 * Время жизни
                 */
                readonly lifeTime: number; // int32
                /**
                 * Имеет ограничеиня по функционалу
                 */
                readonly isCastrated: boolean;
            }

            /**
             * Параметры запроса для редактирования пользователя
             */
            export interface EditAdminUserRequest {
                /**
                 * Идентификатор
                 */
                id?: string | null;
                /**
                 * Имя
                 */
                userName?: string | null;
                /**
                 * Роли
                 */
                roles?: /* Административная роль */ Mongo.AdminRole[] | null;
            }

            /**
             * Параметры запроса на сброс пароля
             */
            export interface ResetPasswordRequest {
                /**
                 * Id администратора
                 */
                id?: string | null;
                /**
                 * Новый пароль
                 */
                password?: string | null;
            }
        }
        namespace Article {
            /**
             * Параметры автора статьи
             */
            export interface ArticleAuthorParams {
                /**
                 * Имя автора
                 */
                name?: string | null;
                /**
                 * Дополнительная информация
                 */
                additionalInfo?: string | null;
            }

            /**
             * Комментраий к статье
             */
            export interface ArticleComment {
                /**
                 * Id объекта
                 */
                readonly id?: string | null;
                /**
                 * Имя пользователя
                 */
                readonly userName?: string | null;
                /**
                 * Дата создания комментария
                 */
                readonly creationDate?: string; // date-time
                /**
                 * Текст комментария
                 */
                readonly text?: string | null;
                /**
                 * Дата редактирования комментария
                 */
                readonly edited?: string | null; // date-time
                /**
                 * Реакция редакции Apteka.ru
                 */
                adminResponse: ArticleCommentAdminResponse;
                /**
                 * Текущий пользователь является автором данного комментария
                 */
                readonly isOwner?: boolean;
                /**
                 * Комментарий можно отредактировать
                 */
                readonly isEditable?: boolean;
            }

            /**
             * Реакция редакции Apteka.ru
             */
            export interface ArticleCommentAdminResponse {
                /**
                 * Текст сообщения
                 */
                text: string | null;
                /**
                 * Время сообщения
                 */
                responseDate: string;
            }

            /**
             * Параметры нового комментраия к статье
             */
            export interface ArticleCommentParams {
                /**
                 * Id комментария
                 */
                commentId?: string | null;
                /**
                 * Текст комментария
                 */
                text?: string | null;
            }

            /**
             * Детальная информация из статьи
             */
            export interface ArticleDetails {
                /**
                 * Id объекта
                 */
                id?: string | null;
                /**
                 * Наименование статьи
                 */
                name?: string | null;
                /**
                 * Дата начала действия статьи
                 */
                activeFrom?: string; // date-time
                /**
                 * Картинка на превью
                 */
                previewPicture?: string | null;
                /**
                 * Текст на превью
                 */
                previewText?: string | null;
                /**
                 * Картинка
                 */
                detailPicture?: string | null;
                /**
                 * Текст статьи
                 */
                detailText?: string | null;
                /**
                 * Ссылка на статью
                 */
                url?: string | null;
                /**
                 * Теги
                 */
                tags?: string[] | null;
                /**
                 * Кол-во просмотров
                 */
                viewsCount?: number; // int32
                /**
                 * Отзывы отключены
                 */
                commentsDisabled?: boolean;
                /**
                 * Кол-во комментариев
                 */
                commentsCount?: number; // int32
                /**
                 * Среднее время прочтения
                 */
                readTime?: number; // int32
                category?: /* Модель категории статей */ Mongo.ArticleCategoryModel;
                author?: /* Автор статьи */ Mongo.ArticleAuthorModel;
                /**
                 * Название для SEO
                 */
                seoTitle?: string | null;
                /**
                 * Описание для SEO
                 */
                seoDescription?: string | null;
                /**
                 * Ключевые слова для SEO
                 */
                seoKeywords?: string | null;
                /**
                 * Список идентификаторов товаров
                 */
                goodUids?: string[] | null;
            }

            /**
             * Параметры статьи
             */
            export interface ArticleParams {
                /**
                 * Наименование статьи
                 */
                name?: string | null;
                /**
                 * Дата начала действия статьи
                 */
                activeFrom?: string | null; // date-time
                /**
                 * Текст на превью
                 */
                previewText?: string | null;
                /**
                 * Текст статьи
                 */
                detailText?: string | null;
                /**
                 * Ссылка на статью
                 */
                url?: string | null;
                /**
                 * Список тегов
                 */
                tags?: string[] | null;
                /**
                 * Отзывы отключены
                 */
                commentsDisabled?: boolean;
                /**
                 * Id категории
                 */
                categoryId?: string | null;
                /**
                 * Id автора
                 */
                authorId?: string | null;
                /**
                 * Название для SEO
                 */
                seoTitle?: string | null;
                /**
                 * Описание для SEO
                 */
                seoDescription?: string | null;
                /**
                 * Ключевые слова для SEO
                 */
                seoKeywords?: string | null;
            }

            /**
             * Тип изображения в статье
             */
            export type ArticlePictureType = 'PreviewPicture' | 'DetailPicture';

            /**
             * Превью статьи
             */
            export interface ArticlePreview {
                /**
                 * Id объекта
                 */
                id?: string | null;
                /**
                 * Наименование статьи
                 */
                name?: string | null;
                /**
                 * Дата начала действия статьи
                 */
                activeFrom?: string; // date-time
                /**
                 * Картинка на превью
                 */
                previewPicture?: string | null;
                /**
                 * Текст на превью
                 */
                previewText?: string | null;
                /**
                 * Ссылка на статью
                 */
                url?: string | null;
                /**
                 * Теги
                 */
                tags?: string[] | null;
                /**
                 * Кол-во просмотров
                 */
                viewsCount?: number; // int32
                /**
                 * Отзывы отключены
                 */
                commentsDisabled?: boolean;
                /**
                 * Кол-во комментариев
                 */
                commentsCount?: number; // int32
                /**
                 * Среднее время прочтения
                 */
                readTime?: number; // int32
                category?: /* Модель категории статей */ Mongo.ArticleCategoryModel;
                author?: /* Автор статьи */ Mongo.ArticleAuthorModel;
            }

            /**
             * Параметры нового комментраия к статье
             */
            export interface NewArticleCommentParams {
                /**
                 * Id статьи
                 */
                articleId?: string | null;
                /**
                 * Текст комментария
                 */
                text?: string | null;
            }
        }
        namespace AutoDestReviews {
            /**
             * Модель отзыва
             */
            export interface AutoDestReviewForAdminModel {
                /**
                 * Id объекта
                 */
                id?: string | null;
                /**
                 * Наименование пункта доставки
                 */
                autoDestName?: string | null;
                /**
                 * Адрес пункта доставки
                 */
                autoDestAddr?: string | null;
                /**
                 * Оценка
                 */
                rating?: number; // int32
                /**
                 * Отзыв
                 */
                review?: string | null;
                /**
                 * Дата публикации отзыва
                 */
                created?: string; // date-time
                /**
                 * Дата удаления отзыва
                 */
                deleted?: string | null; // date-time
                /**
                 * Дата редактирования отзыва
                 */
                edited?: string | null; // date-time
                /**
                 * Номер заказа, по которому сделан отзыв
                 */
                orderNum?: string | null;
                user?: /* Информация о пользователе оставившем отзыв */ ItemReviews.ReviewUserInfo;
                /**
                 * Id пункта доставки
                 */
                autoDestId?: string | null;
            }

            /**
             * Отзыв с его автором
             */
            export interface ReviewWithOwnerForAdmin {
                /**
                 * Id объекта
                 */
                readonly id?: string | null;
                /**
                 * Наименование пункта доставки
                 */
                readonly autoDestName?: string | null;
                /**
                 * Адрес пункта доставки
                 */
                readonly autoDestAddr?: string | null;
                /**
                 * Оценка
                 */
                readonly rating?: number; // int32
                /**
                 * Отзыв
                 */
                readonly review?: string | null;
                /**
                 * Дата публикации отзыва
                 */
                readonly created?: string; // date-time
                /**
                 * Дата редактирования отзыва
                 */
                readonly edited?: string | null; // date-time
                /**
                 * Дата удаления отзыва
                 */
                readonly deleted?: string | null; // date-time
                /**
                 * Отзыв доступен для редактирования
                 */
                readonly editable?: boolean;
                /**
                 * Номер заказа, по которому сделан отзыв
                 */
                readonly orderNum?: string | null;
                user?: /* Информация о пользователе оставившем отзыв */ ItemReviews.ReviewUserInfo;
                /**
                 * Id пункта доставки
                 */
                autoDestId?: string | null;
            }
        }
        namespace Banner {
            /**
             * Раздел банеров
             */
            export interface BannerSectionParams {
                /**
                 * Имя
                 */
                name?: string | null;
                /**
                 * Url раздела
                 */
                url?: string | null;
                /**
                 * Обязательность дополнительного поля
                 */
                mandatoryAdditionalField?: boolean;
                /**
                 * Текстовое пояснение к дополнительному полю
                 */
                additionalFieldDescription?: string | null;
                imageSize?: /* Размер картинки */ Mongo.ImageSize;
            }

            /**
             * Параметры для редактирования баннера
             */
            export interface EditBannerParams {
                /**
                 * Id объекта
                 */
                id?: string | null;
                /**
                 * Имя
                 */
                name?: string | null;
                /**
                 * Порядок сортировки баннеров (меньше значение - более приоритетен в выдаче)
                 */
                sortOrder?: null | number; // int32
                /**
                 * Дата начала
                 */
                startDate?: string; // date-time
                /**
                 * Дата окончания
                 */
                endDate?: string; // date-time
                /**
                 * Ссылка, на которую ведет баннер
                 */
                url?: string | null;
                /**
                 * Подсказка
                 */
                hint?: string | null;
                /**
                 * Города, в которых баннер активен
                 */
                iPharmTownIds?: string[] | null;
                /**
                 * Дополнительные параметры
                 */
                additionalParams?: string | null;
                /**
                 * Контрольная сумма картинки
                 */
                crc32?: number; // int32
            }
        }
        namespace DataImport {
            /**
             * Тип импортируеммых данных (метод)
             */
            export type DataImportType =
                'Actions'
                | 'MiniShops'
                | 'SalePrograms'
                | 'SaleProgramsValues'
                | 'AfinaSaleProgramValues'
                | 'AutoDestsReviews'
                | 'ItemsReviews'
                | 'VitaminsBalance'
                | 'VitaminsHistory'
                | 'Favorites'
                | 'UsersInvitations'
                | 'WaitingList'
                | 'PromotionSubscriptions'
                | 'ReviewRequestSubscriptions'
                | 'ServiceMessagesSubscriptions';
        }
        namespace ItemReviews {
            /**
             * Информация для поиска по бренду
             */
            export interface BrandSearchInfo {
                /**
                 * Наименование
                 */
                name?: string | null;
                /**
                 * Uid
                 */
                uid?: string; // uuid
            }

            /**
             * Информация для поиска по категории
             */
            export interface CategorySearchInfo {
                /**
                 * Наименование корневой категории
                 */
                rootName?: string | null;
                /**
                 * Наименование
                 */
                name?: string | null;
                /**
                 * Uid
                 */
                uid?: string; // uuid
            }

            /**
             * Модель отзыва для товара
             */
            export interface ItemReviewForAdminModel {
                /**
                 * Id отзыва
                 */
                id?: string | null;
                /**
                 * Id товара
                 */
                itemId?: string | null;
                /**
                 * Оценка
                 */
                rating?: number; // int32
                /**
                 * Комментарий
                 */
                review?: string | null;
                /**
                 * Дата составления отзыва
                 */
                created?: string; // date-time
                /**
                 * Покупал ли пользователь этот товар у нас
                 */
                boughtFromUs?: boolean;
                /**
                 * Дата удаления отзыва
                 */
                deleted?: string | null; // date-time
                /**
                 * Дата последнего редактирования отзыва
                 */
                edited?: string | null; // date-time
                /**
                 * Отзыв доступен для редактирования
                 */
                readonly editable?: boolean;
                user?: /* Информация о пользователе оставившем отзыв */ ReviewUserInfo;
                /**
                 * Наименование товара
                 */
                itemName?: string | null;
            }

            /**
             * Информация о пользователе оставившем отзыв
             */
            export interface ReviewUserInfo {
                /**
                 * Идентификатор
                 */
                id?: string | null;
                /**
                 * телефон
                 */
                custPhone?: string | null;
                /**
                 * ФИО
                 */
                fio?: string | null;
                /**
                 * Почта
                 */
                email?: string | null;
            }
        }
        namespace KcsDebug {
            /**
             * Товары и пользователи программы Афина
             */
            export interface CloakedItemsAndUsers {
                /**
                 * Uid'ы товаров
                 */
                items?: string /* uuid */[] | null;
                /**
                 * Номера телефонов пользователей
                 */
                users?: string[] | null;
            }
        }
        namespace SalePartner {
            /**
             * Параметры запроса на добавление партнерской программы
             */
            export interface AddSalePartnerRequest {
                /**
                 * Наименование
                 */
                name?: string | null;
                /**
                 * Ссылка
                 */
                url?: string | null;
                /**
                 * Признак активности
                 */
                active?: boolean;
                /**
                 * Описание
                 */
                description?: string | null;
                /**
                 * Участвующе товары
                 */
                products?: /* Товар в партнерской программе */ Mongo.SaleProgramProduct[] | null;
            }

            /**
             * Параметры запроса на редактирование партнерской программы
             */
            export interface EditSalePartnerRequest {
                /**
                 * Id партнерской программы
                 */
                id?: string | null;
                /**
                 * Наименование
                 */
                name?: string | null;
                /**
                 * Ссылка
                 */
                url?: string | null;
                /**
                 * Признак активности
                 */
                active?: boolean;
                /**
                 * Описание
                 */
                description?: string | null;
                /**
                 * Участвующе товары
                 */
                products?: /* Товар в партнерской программе */ Mongo.SaleProgramProduct[] | null;
            }

            /**
             * Полная информация о партнерской программе
             */
            export interface FullSaleProgramInfo {
                /**
                 * Id программы
                 */
                id?: string | null;
                /**
                 * Наименование
                 */
                name?: string | null;
                /**
                 * Ссылка
                 */
                url?: string | null;
                /**
                 * Активность
                 */
                active?: boolean;
                /**
                 * Описание
                 */
                description?: string | null;
                /**
                 * Товары в партнерской программе
                 */
                products?: /* Информация о товаре в партнерской программе */ FullSaleProgramProductInfo[] | null;
            }

            /**
             * Информация о товаре в партнерской программе
             */
            export interface FullSaleProgramProductInfo {
                /**
                 * Uid товара
                 */
                uid?: string; // uuid
                /**
                 * Наименование
                 */
                name?: string | null;
                saleType?: /* Тип скидки по партнерской программе */ Mongo.SaleType;
                /**
                 * Размер скидки
                 */
                saleValue?: number; // double
            }

            /**
             * Информация о партнерской программе
             */
            export interface SalePartnerInfo {
                /**
                 * Id программы
                 */
                id?: string | null;
                /**
                 * Наименование
                 */
                name?: string | null;
                /**
                 * Ссылка
                 */
                url?: string | null;
                /**
                 * Кол-во товаров в программе
                 */
                productsCount?: number; // int32
            }
        }
        namespace SearchHints {
            /**
             * Модель хинта
             */
            export interface SearchHintParams {
                /**
                 * Сам хинт
                 */
                searchHint: string;
                /**
                 * Дата начала
                 */
                startDate: string; // date-time
                /**
                 * Дата конца
                 */
                endDate: string; // date-time
                hintType: /* Тип хинта */ Mongo.HintType;
            }
        }
        namespace SuperUser {
            /**
             * Входные параметры для обновления подписок пользователя
             */
            export interface ChangeUserSubscriptionsRequest {
                /**
                 * Id пользователя
                 */
                userId?: string | null;
                /**
                 * Промо-рассылки (email)
                 */
                promotionEmail?: boolean;
                /**
                 * Промо-рассылки (sms)
                 */
                promotionSms?: boolean;
                /**
                 * Оставьте отзыв (email)
                 */
                reviewRequestEmail?: boolean;
                /**
                 * Оставьте отзыв (sms)
                 */
                reviewRequestSms?: boolean;
                /**
                 * Сервисные сообщения (email)
                 */
                serviceMessagesEmail?: boolean;
                /**
                 * Сервисные сообщения (sms)
                 */
                serviceMessagesSms?: boolean;
            }

            /**
             * Параметры запроса на обновление баланса витаминок пользователя
             */
            export interface ChangeUserVitaminsRequest {
                /**
                 * Id пользователя
                 */
                userId?: string | null;
                updatingType?: /* Тип обновления баланса */ VitBalanceUpdatingType;
                /**
                 * Сумма к начислению/списанию
                 */
                value?: number; // int32
            }

            /**
             * Параметры запроса на выдачу пользователю скидки
             */
            export interface DiscountPromoCodeRequest {
                /**
                 * Id пользователя
                 */
                userId?: string | null;
            }

            /**
             * Параметры запроса на блокировку заказа
             */
            export interface OrderBlockRequest {
                /**
                 * Id заказа
                 */
                orderId: string;
            }

            /**
             * Товар в заказе
             */
            export interface OrderUpdateItem {
                /**
                 * Id товара
                 */
                itemId?: string | null;
                /**
                 * Колличество
                 */
                amount?: number; // int32
            }

            /**
             * Параметры запроса на изменение заказа
             */
            export interface OrderUpdateRequest {
                /**
                 * Id заказа
                 */
                orderId?: string | null;
                /**
                 * Позиции в заказе
                 */
                items?: /* Товар в заказе */ OrderUpdateItem[] | null;
                /**
                 * Предпросмотр без сохранения
                 */
                dryRun?: boolean;
            }

            /**
             * Модель заказа с данными о пользователе
             */
            export interface OrderWithUserData {
                userData?: /* Данные о пользователе, сделавшем заказ */ UserData;
                /**
                 * SuperUser, изменивший заказ
                 */
                readonly superUserName?: string | null;
                /**
                 * Id объекта
                 */
                id?: string | null;
                /**
                 * Активный заказ.
                 */
                inWork?: boolean;
                /**
                 * Id города
                 */
                iPharmTownId?: string | null;
                /**
                 * Наименование города ИА
                 */
                iPharmTownName?: string | null;
                /**
                 * Наименование филиала
                 */
                regBodyName?: string | null;
                /**
                 * Id пункта доставки
                 */
                autoDestId?: string | null;
                /**
                 * Адрес пункта доставки
                 */
                autoDestAddr?: string | null;
                /**
                 * Имя сайта, на котором был оформлен заказ
                 */
                fromSite?: string | null;
                /**
                 * Номер заказа
                 */
                orderNum?: string | null;
                /**
                 * Дата заказа
                 */
                orderDate?: string; // date-time
                /**
                 * Дата удаления заказа
                 */
                deleted?: string | null; // date-time
                /**
                 * Дата подтверждения валидности заказа для отправки на склад
                 */
                confirmed?: string | null; // date-time
                /**
                 * Дата + время редактирования заказа
                 */
                edit?: string | null; // date-time
                /**
                 * Прогнозируемая дата и время доставки
                 */
                plannedShippedDate?: string | null; // date-time
                /**
                 * Прогнозируемая дата и время доставки (окончание)
                 */
                plannedEndShippedDate?: string | null; // date-time
                /**
                 * Заказ отправлен на склад
                 */
                onDepot?: string | null; // date-time
                /**
                 * Заказ отправлен на склад (source)
                 */
                trackingOnDepot?: string | null; // date-time
                /**
                 * По заказу выведены сборочные листы
                 */
                asmList?: string | null; // date-time
                /**
                 * Прогнозируемое время доставки в пункт выдачи
                 */
                deliveryTime?: string | null; // date-time
                /**
                 * Прогнозируемое время доставки в пункт выдачи (source)
                 */
                trackingDeliveryTime?: string | null; // date-time
                /**
                 * Заказ доставлен в пункт выдачи
                 */
                orderShipped?: string | null; // date-time
                /**
                 * Заказ доставлен в пункт выдачи (source)
                 */
                trackingOrderShipped?: string | null; // date-time
                status?: /* Статус заказа */ Mongo.OrderStatus;
                /**
                 * Имя промо-кода
                 */
                promoCodeName?: string | null;
                /**
                 * Пользователь хочет обратного звонка
                 */
                needCall?: boolean;
                /**
                 * Требуется уведомление о статусах на email
                 */
                needEmail?: boolean;
                /**
                 * Дата совершения звонка
                 */
                callMaded?: string | null; // date-time
                /**
                 * Пользовательское название для заказа
                 */
                customName?: string | null;
                /**
                 * Список имен промо-кодов
                 */
                readonly promoCodes?: string[] | null;
                /**
                 * Список товаров
                 */
                items?: /* Позиция в заказе */ Mongo.OrderItem[] | null;
                /**
                 * Общая сумма по корзине
                 */
                totalSum?: number; // double
                /**
                 * Общая выгода по корзине
                 */
                totalProfit?: number; // double
                /**
                 * Общая сумма по корзине без учета скидок
                 */
                totalNoDiscSum?: number; // double
                /**
                 * Витаминок будет начислено при выполнении заказа
                 */
                vitaminsToBeCredited?: number; // int32
                /**
                 * Дата начисления витаминок
                 */
                vitaminsCredited?: string | null; // date-time
                /**
                 * Планируемая дата начисления витаминок
                 */
                readonly plannedVitaminsCredited?: string | null; // date-time
                /**
                 * Витаминок будет списано
                 */
                vitaminsUsed?: number; // int32
                /**
                 * Промо витаминок будет начислено при выполнении заказа
                 */
                extraVits?: number; // int32
                /**
                 * Дата выкупа товара
                 */
                done?: string | null; // date-time
                /**
                 * Получение заказа подтверждено
                 */
                receiptConfirmed?: string | null; // date-time
                /**
                 * Партнер заказа
                 */
                salePartner?: /* Партнер заказа */ Mongo.OrderSalePartner[] | null;
                /**
                 * Порядковый номер заказа у пользователя
                 */
                usersIndexNumber?: number; // int64
                /**
                 * История изменения заказа для суперпользователя.
                 */
                readonly orderHistory?:
                    /* набор позиций по заказу в определённый момент времени жизни заказа. */
                    Mongo.OrderState[] | null;

                needCallManager?: boolean;
            }

            /**
             * Информация о промо-коде для SuperUser'а
             */
            export interface PromoCodeInfo {
                /**
                 * Наименование промо-кода
                 */
                name?: string | null;
                /**
                 * Дата начала действия промо-кода
                 */
                startDate?: string; // date-time
                /**
                 * Дата окончания действия промо-кода
                 */
                endDate?: string; // date-time
                /**
                 * Разовый промо-код
                 */
                onceOnly?: boolean;
                /**
                 * Промо-код по товару
                 */
                forGoods?: boolean;
                /**
                 * Нелимитированное использование ПК (в рамках лимита в поле SumLimit)
                 */
                unlimited?: boolean;
                /**
                 * Сумма лимита по товарному промо-коду
                 */
                sumLimit?: number | null; // double
                /**
                 * Сумма скидки
                 */
                discountSum?: number | null; // double
                /**
                 * Процент скидочной карты по промо-коду
                 */
                discountPercent?: null | number; // int32
                /**
                 * Был использован
                 */
                wasUsed?: boolean;
            }

            /**
             * Параметры запроса на участие в благотворительной программе
             */
            export interface UserCharityRequest {
                /**
                 * Id пользователя
                 */
                userId?: string | null;
                /**
                 * Кол-во витаминок, которое требуется передать
                 */
                vitamins?: number; // int32
                /**
                 * Имя участника акции
                 */
                fio?: string | null;
                /**
                 * Скрыть участника в рэйтинге
                 */
                hideInRating?: boolean;
            }

            /**
             * Данные о пользователе, сделавшем заказ
             */
            export interface UserData {
                /**
                 * Id пользователя
                 */
                id?: string | null;
                /**
                 * Имя пользователя
                 */
                fio?: string | null;
                /**
                 * Номер телефона
                 */
                custPhone?: string | null;
                /**
                 * Подтвержденный адрес электронной почты
                 */
                email?: string | null;
                /**
                 * Нужен звонок менеджера
                 */
                needCallManager?: boolean;
            }

            /**
             * Тип обновления баланса
             */
            export type VitBalanceUpdatingType = 'Credit' | 'Debit';
        }
    }
    namespace Announcement {
        /**
         * Модель объявления
         */
        export interface AnnouncementModel {
            /**
             * Заголовок
             */
            title?: string | null;
            /**
             * Текст
             */
            text?: string | null;
        }

        /**
         * Место размещения
         */
        export type AnnouncementPlace = 'Cart';

        /**
         * Параметры для создания
         */
        export interface PutAnnouncement {
            /**
             * Заголовок
             */
            title?: string | null;
            /**
             * Текст
             */
            text?: string | null;
            /**
             * Список филиалов
             */
            iPharmTownIdList?: string[] | null;
            /**
             * Дата начала
             */
            startDate?: string; // date-time
            /**
             * Дата окончания
             */
            endDate?: string; // date-time
            place?: /* Место размещения */ AnnouncementPlace;
        }
    }
    namespace Auth {
        /**
         * Класс входных параметров для метода /AuthController/SendSms
         */
        export interface AuthCodeRequest {
            /**
             * Телефон
             */
            phone?: string | null;
            /**
             * Антибот
             */
            u?: 'U';
            /**
             * Запрос от залогиненного забывчивого юзера
             */
            forChange?: '1';
        }

        /**
         * Ответ на запрос смс-кода
         */
        export interface AuthCodeResponse {
            /**
             * Время до повторной попытки отправки кода
             */
            nextAttemptDelayMs?: number; // int32
            /**
             * Новый пользователь или нет
             */
            newUser?: boolean;
        }

        /**
         * Тип авторизации
         */
        export type AuthType = 'BySms' | 'BySecret';

        /**
         * Класс входных параметров для метода /AuthController/Athenticate
         */
        export interface AuthUserRequest {
            type?: /* Тип авторизации */ AuthType;
            /**
             * Непосредственно сам код доступа из смс или из секретов(для внутренних сервисов)
             */
            code?: string | null;
            /**
             * Телефон
             */
            phone?: string | null;
            /**
             * Часовой пояс
             */
            timeZone?: null | number; // int32
            /**
             * Реферальная ссылка
             */
            referalId?: string | null;
            /**
             * Группа экспериментов информирования о промокодах
             */
            group?: number | null;
        }

        /**
         * Класс ответа при запросе аутентификации /AuthController/Athenticate
         */
        export interface AuthUserResponse {
            /**
             * Идентификатор пользователя
             */
            userId?: string | null;
            /**
             * Токен пользователя
             */
            readonly token?: string | null;
            /**
             * Телефон пользователя
             */
            readonly custPhone?: string | null;
            /**
             * Id города пользователя
             */
            readonly townId?: string | null;
            /**
             * Id города (географ.)
             */
            readonly cityId?: string | null;
            /**
             * Id аптеки пользователя
             */
            readonly autoDestId?: string | null;
            /**
             * Время жизни токена
             */
            readonly lifetimeInMinutes?: number; // int32
        }
    }
    namespace AutoDest {
        /**
         * Координаты пункта доставки
         */
        export interface AutoDestCoords {
            /**
             * Id пункта доставки
             */
            id?: string | null;
            /**
             * идентификатор города
             */
            iPharmTownId?: string | null;
            /**
             * Наименование пункта доставки
             */
            name?: string | null;
            /**
             * Адрес пункта доставки
             */
            address?: string | null;
            /**
             * Кол-во отзывов
             */
            reviewsCount?: number; // int32
            /**
             * Рэйтинг
             */
            rating?: number; // double
            /**
             * Разрешенные типы товаров
             */
            allowed?: string | null;
            /**
             * Есть безналичная оплата
             */
            cashless?: boolean;
            /**
             * Время работы пункта выдачи
             */
            workTime?: string | null;
            /**
             * Широта (значение)
             */
            latitudeNum?: number | null; // double
            /**
             * Долгота (значение)
             */
            longitudeNum?: number | null; // double
            /**
             * Широта (координаты)
             */
            latitudeGrad?: string | null;
            /**
             * Широта (координаты)
             */
            longitudeGrad?: string | null;
            /**
             * Телефон
             */
            phone?: string | null;
            /**
             * Собственная фирменная аптека
             */
            own?: boolean;
        }

        /**
         * Запрос на редактирование отзыва
         */
        export interface EditReviewRequest {
            /**
             * Id отзыва
             */
            id?: string | null;
            /**
             * Оценка
             */
            rating?: number; // int32
            /**
             * Отзыв
             */
            review?: string | null;
            /**
             * Номер заказа
             */
            orderNum?: string | null;
            /**
             * Причины недовольства аптекой
             */
            complaints?: any; // new field
        }

        /**
         * Ближайшее время доставки
         */
        export interface NearDeliveryDate {
            /**
             * Планируемая дата и время начала доставки
             */
            shipTimeStart?: string | null; // date-time
            /**
             * Планируемая дата и время окончания доставки
             */
            shipTimeEnd?: string | null; // date-time
        }

        /**
         * Причины недовольства аптекой
         */
        export interface AutoDestComplaints {
            standardReasons?: ('Staff'|'Delivery'|'Cashless'|'Location'|'Schedule')[] | null;
            customReason?: string | null;
        }

        /**
         * Входные дынные для создания отзыва
         */
        export interface NewReviewRequest {
            /**
             * Id аптеки
             */
            autoDestId?: string | null;
            /**
             * Оценка
             */
            rating?: number; // int32
            /**
             * Отзыв
             */
            review: string;
            /**
             * Имя пользователя
             */
            fio?: string | null;
            /**
             * Номер заказа
             */
            orderNum?: string | null;
            /**
             * Причины недовольства аптекой
             */
            complaints?: AutoDestComplaints;
        }

        /**
         * Автор отзыва
         */
        export interface ReviewOwner {
            /**
             * Имя пользователя
             */
            readonly fio?: string | null;
            /**
             * Кол-во отзывов, оставленное пользователем
             */
            readonly itemReviewsCount?: number; // int32
        }

        /**
         * Отзыв пользователя
         */
        export interface ReviewWithAutoDestTotals {
            /**
             * Рейтинг аптеки (от 1 до 5)
             */
            totalRating?: number; // double
            /**
             * Кол-во отзывов
             */
            totalReviewsCount?: number; // int32
            /**
             * Отзыв доступен для редактирования
             */
            readonly editable?: boolean;
            /**
             * Id объекта
             */
            id?: string | null;
            /**
             * Id пункта доставки
             */
            autoDestId?: string | null;
            /**
             * Наименование пункта доставки
             */
            autoDestName?: string | null;
            /**
             * Адрес пункта доставки
             */
            autoDestAddr?: string | null;
            /**
             * Оценка
             */
            rating?: number; // int32
            /**
             * Отзыв
             */
            review?: string | null;
            /**
             * Дата публикации отзыва
             */
            created?: string; // date-time
            /**
             * Дата редактирования отзыва
             */
            edited?: string | null; // date-time
            /**
             * Номер заказа, по которому сделан отзыв
             */
            orderNum?: string | null;
        }

        /**
         * Отзыв с его автором
         */
        export interface ReviewWithOwner {
            /**
             * Id объекта
             */
            readonly id?: string | null;
            /**
             * Наименование пункта доставки
             */
            readonly autoDestName?: string | null;
            /**
             * Адрес пункта доставки
             */
            readonly autoDestAddr?: string | null;
            /**
             * Оценка
             */
            readonly rating?: number; // int32
            /**
             * Отзыв
             */
            readonly review?: string | null;
            /**
             * Дата публикации отзыва
             */
            readonly created?: string; // date-time
            /**
             * Дата редактирования отзыва
             */
            readonly edited?: string | null; // date-time
            /**
             * Отзыв доступен для редактирования
             */
            readonly editable?: boolean;
            /**
             * Номер заказа, по которому сделан отзыв
             */
            readonly orderNum?: string | null;
            owner?: /* Автор отзыва */ ReviewOwner;
        }

        /**
         * Входная модель данных для метода /AutoDest/UserAutoDest
         */
        export interface UserAutoDestRequest {
            /**
             * Id пункта доставки
             */
            id?: string | null;
            /**
             * (SuperUser only) Id пользователя, в контексте которого ведется работа
             */
            userId?: string | null;
        }
    }
    namespace Cart {
        /**
         * Класс входных данных для добавления промо-кода
         */
        export interface AddPromoCodeRequest {
            /**
             * Промо-код
             */
            promoCode?: string | null;
            /**
             * (SuperUser only) Id пользователя, в контексте которого ведется работа
             */
            userId?: string | null;
        }

        /**
         * Товар в корзине
         */
        export interface CartUpdateItem {
            /**
             * Id товара
             */
            itemId?: string | null;
            /**
             * Количество
             */
            amount?: number; // int32
            /**
             * Товар отложен
             */
            deferred?: boolean;
        }

        /**
         * Входные данные для обновления корзины
         */
        export interface CartUpdateRequest {
            /**
             * Список позиций в корзине
             */
            items?: /* Товар в корзине */ CartUpdateItem[] | null;
            /**
             * (SuperUser only) Id пользователя, в контексте которого ведется работа
             */
            userId?: string | null;
        }

        /**
         * Исходящая модель данных по корзине
         */
        export interface FullCart {
            /**
             * Перечень товаров
             */
            items?: /* Товар в корзине */ FullCartItem[] | null;
            /**
             * Список промо-кодов, добавленных пользователем
             */
            promoCodes?: /* Промо-код в корзине пользователя */ Mongo.CartPromoCode[] | null;
            vitaminsInfo?: /* Информация по витаминкам */ VitaminsInfo;
            /**
             * Минимальная сумма поставки
             */
            minQuerySum?: number; // double
            deliveryDate?: /* Расписание отгрузки */ Mongo.DeliveryDate;
            /**
             * Соответствие пункта доставки и товаров по признаку маркировки
             */
            autoDestEDrugConsistency?: boolean;
            /**
             * Суммарные скидки
             */
            totalAppliedDiscounts?: AppliedDiscount[];
            /**
             * Общее кол-во позиций
             */
            totalAmount?: number; // int32
            /**
             * Общая сумма по корзине
             */
            totalSum?: number; // double
            /**
             * Общая выгода по корзине
             */
            totalProfit?: number; // double
            /**
             * Общая сумма по корзине без учета скидок
             */
            totalNoDiscSum?: number; // double
            /**
             * Количество уникальных товаров в корзине
             */
            totalPositions?: number; // int32
        }

        /**
         * Товар в корзине
         */
        export interface FullCartItem {
            /**
             * Имя товара
             */
            itemName?: string | null;
            goodNaming?: /* Составное наименование товара */ Mongo.GoodNaming;
            /**
             * Производитель
             */
            vendor?: string | null;
            /**
             * Цена без дискаунта
             */
            noDiscPrice?: number; // double
            /**
             * Цена с применением всех скидок
             */
            price?: number; // double
            /**
             * Профит
             */
            profit?: number; // double
            /**
             * Цена с учетом дисконтной карты и прогрессирующей скидки
             */
            cartPrice?: number; // double
            /**
             * Выгода с учетом дисконтной карты и прогрессирующей скидки
             */
            cartProfit?: number; // double
            /**
             * Выгода с учетом дисконтной карты и прогрессирующей скидки (в процентах)
             */
            cartProfitPercent?: number; // double
            /**
             * Кол-во для ограничения продажи особых товаров
             */
            restrictionQuantity?: number; // int32
            /**
             * Фотографии
             */
            fileInst?: /* Фото объекта */ Mongo.FileInst[] | null;
            /**
             * Id группы товаров
             */
            itemGroupId?: string | null;
            /**
             * Промо витаминки
             */
            extraVits?: number; // int32
            purchasedGood?: /* Рекомендуемый товар в корзине */ PurchasedGood;
            /**
             * рецептурный препарат
             */
            prescriptionDrug?: boolean;
            /**
             * Рецепт остаётся в аптеке
             */
            recipeInPh?: boolean;
            /**
             * Id товара
             */
            itemId?: string | null;
            /**
             * Количество
             */
            amount?: number; // int32
            /**
             * Товар отложен
             */
            deferred?: boolean;
            /**
             * Уведомить о появлении
             */
            notifyAppearance?: boolean;
            itemType?: /* Тип */ Mongo.ItemType;
            /**
             * SourceUids
             */
            sourceUids?: string /* uuid */[] | null;
            /**
             * Примечание
             */
            note?: string | null;
            /**
             * Прогрессирующая скидка по товару в корзине
             */
            progressiveDiscount: Mongo.CartItemPrDisc;
            /**
             * Электронные рецепты подходящие для данного товара
             */
            prescriptions?: ShortPrescription[];
            categoryUrls?: string[] | null;
        }

        /**
         * Рецепт подходящий для товара в корзине
         */
        export interface ShortPrescription {
            /**
             * Номер рецепта
             */
            prescriptionNumber: string | null;
            /**
             * Действующее вещество
             */
            interName: string | null;
            /**
             * Дозировка
             */
            dosage: string | null;
            /**
             * Количество
             */
            amount: string | null;
        }

        /**
         * Рекомендуемый товар в корзине
         */
        export interface PurchasedGood {
            /**
             * Id товара
             */
            id?: string | null;
            /**
             * Имя товара
             */
            name?: string | null;
            /**
             * Цена
             */
            price?: number; // double
            /**
             * Выгода
             */
            profit?: number; // double
            /**
             * Цена без скидки
             */
            noDiscPrice?: number; // double
            photo?: /* Пути до фотографий */ Mongo.Photo;
        }

        /**
         * Модель запроса об уведомлении о появлении товара
         */
        export interface SetNotifyRequest {
            /**
             * Id товара
             */
            itemId?: string | null;
            /**
             * Установить уведомление
             */
            notify?: boolean;
            /**
             * (SuperUser only) Id пользователя, в контексте которого ведется работа
             */
            userId?: string | null;
        }

        /**
         * Урезаная модель корзины
         */
        export interface ShortCart {
            /**
             * Общее кол-во позиций
             */
            totalAmount?: number; // int32
            /**
             * Общая сумма по корзине
             */
            totalSum?: number; // double
            /**
             * Общая выгода по корзине
             */
            totalProfit?: number; // double
            /**
             * Общая сумма по корзине без учета скидок
             */
            totalNoDiscSum?: number; // double
            /**
             * Количество уникальных товаров в корзине
             */
            totalPositions?: number; // int32
        }

        /**
         * Примененная скидка
         */
        export interface AppliedDiscount {
            /**
             * Описание
             */
            description: string | null;
            /**
             * Процент скидки
             */
            percent: number;
            /**
             * Сумма скидки
             */
            sum: number;
            /**
             * Тип скидки
             */
            type: string | null;
        }

        /**
         * Параметры запроса на применение витаминок
         */
        export interface UseVitaminsRequest {
            /**
             * Применяемое кол-во витаминок
             */
            vitaminsCount?: number; // int32
            /**
             * (SuperUser only) Id пользователя, в контексте которого ведется работа
             */
            userId?: string | null;
        }

        /**
         * Информация по витаминкам
         */
        export interface VitaminsInfo {
            /**
             * Минимальная скидка
             */
            minDiscount?: number; // int32
            /**
             * Текущая скидка
             */
            currentDiscount?: number; // int32
            /**
             * Максимальная скидка
             */
            maxDiscount?: number; // int32
            /**
             * Витаминок за процент скидки
             */
            vitaminsPerPercent?: number; // int32
            /**
             * Витаминок начислено
             */
            vitaminsAccrued?: number; // int32
            /**
             * Промо витаминок начислено
             */
            promoVitsAccrued?: number; // int32
            /**
             * Витаминок потрачено
             */
            vitaminsUsed?: number; // int32
            /**
             * Баланс пользователя
             */
            vitBalance?: number; // int32
        }
    }
    namespace Catalog {
        /**
         * Категория в каталоге
         */
        export interface CatalogCategory {
            /**
             * Uid категории
             */
            uid?: string; // uuid
            /**
             * Имя категории
             */
            name?: string | null;
            /**
             * SEO имя категории
             */
            seoName?: string | null;
            /**
             * Имя для урла
             */
            url?: string | null;
            /**
             * Подкатегория
             */
            subGroup?: /* Категория в каталоге */ CatalogCategory[] | null;
            /**
             * Количество товаров в разделе
             */
            itemsCount?: number; // int32
            photo?: /* Пути до фотографий */ Mongo.Photo;
        }

        /**
         * Модель каталога
         */
        export interface CatalogInfo {
            /**
             * Список категорий
             */
            category?: /* Категория в каталоге */ CatalogCategory[] | null;
            /**
             * Список симптомов
             */
            goodGroup?: /* Тематическая группа */ Mongo.GoodGroupModel[] | null;
        }

        /**
         * Банер для корневой категории
         */
        export interface CategoryBanner {
            /**
             * Путь к фото
             */
            photoPath?: string | null;
            /**
             * Имя
             */
            name?: string | null;
            /**
             * Ссылка
             */
            url?: string | null;
        }

        /**
         * Дополнительная информация для корневой категории
         */
        export interface RootCategoryAdditional {
            categoryBanner?: /* Банер для корневой категории */ CategoryBanner;
            /**
             * Потребности в категории
             */
            needIaCat?: /* Потребность для каталога */ Mongo.NeedIaCatModel[] | null;
            /**
             * Брэнды в категории
             */
            brands?: /* Модель данных бренда (короткий вариант) */ Mongo.ShortBrandModel[] | null;
            /**
             * Текстовый блок для описания бренда
             */
            seoText1?: string | null;
            /**
             * Доп. текстовый блок для описания бренда”
             */
            seoText2?: string | null;
        }

        /**
         * Категория с подкатегорией
         */
        export interface UnwoundCategory {
            /**
             * Uid категории
             */
            uid?: string; // uuid
            /**
             * Имя категории
             */
            name?: string | null;
            /**
             * Имя для урла
             */
            url?: string | null;
            subCategory?: /* Категория с подкатегорией */ UnwoundCategory;
            /**
             * Количество товаров в разделе
             */
            itemsCount?: number; // int32
            photo?: /* Пути до фотографий */ Mongo.Photo;
        }
    }
    namespace Charity {
        /**
         * Параметры запроса на участие в благотворительной программе
         */
        export interface CharityRequest {
            /**
             * Кол-во витаминок, которое требуется передать
             */
            vitamins?: number; // int32
            /**
             * Имя участника акции
             */
            fio?: string | null;
            /**
             * Скрыть участника в рэйтинге
             */
            hideInRating?: boolean;
        }

        /**
         * Объем пожертвований пользователя за период
         */
        export interface CharityTopRecord {
            /**
             * Имя пользователя
             */
            fio?: string | null;
            /**
             * Кол-во витаминок
             */
            vitamins?: number; // int32
        }
    }
    namespace City {
        /**
         * Класс входных параметров для метода /CityController/PutUserCity
         */
        export interface ChangeUserCityRequest {
            /**
             * Id выбранного города
             */
            id?: string | null;
            /**
             * Выбранный город указан пользователем вручную
             */
            manualChange?: boolean;
            /**
             * (SuperUser only) Id пользователя, в контексте которого ведется работа
             */
            userId?: string | null;
        }

        /**
         * Url города
         */
        export interface CityUrl {
            /**
             * Id города
             */
            id?: string | null;
            /**
             * Url города
             */
            url?: string | null;
            /**
             * Название города
             */
            name?: string | null;
        }

        /**
         * Район города
         */
        export interface District {
            /**
             * Название
             */
            name?: string | null;
            /**
             * Широта левой верхней точки
             */
            latitudeLeft?: number; // double
            /**
             * Долгота левой верхней точки
             */
            longitudeTop?: number; // double
            /**
             * Широта нижней правой точки
             */
            latitudeRight?: number; // double
            /**
             * Долгота нижней правой точки
             */
            longitudeBottom?: number; // double
            /**
             * Кол-во пунктов доставки
             */
            autoDestCount?: number; // int32
        }

        /**
         * Информация о районах города
         */
        export interface DistrictsInfo {
            /**
             * Районы города
             */
            districts?: /* Район города */ District[] | null;
            /**
             * Районы метро в городе
             */
            metros?: /* Район города */ District[] | null;
        }

        /**
         * Филиал с перечнем городов
         */
        export interface RegBodyInfo {
            /**
             * Города филиала
             */
            towns?: /* Модель города */ Mongo.IPharmTownModel[] | null;
            /**
             * Id объекта
             */
            id?: string | null;
            /**
             * Наименование филиала
             */
            name?: string | null;
        }
    }
    namespace Favorite {
        /**
         * Параметры запроса на добавление пачки товаров в список избарнного
         */
         export interface FavoriteGoodsToListParams {
            /**
             * Id списка избранных товаров
             */
            favoriteListId?: string;
            /**
             * Товары для добавления в список
             */
            itemsToAdd?: string[];
            /**
             * Товары для удаления из списка
             */
            itemsToDelete?: string[];
            /**
             * Id пользователя
             */
            userId?: string;
        }

        /**
         * Параметры запроса на добавление в избранное
         */
         export interface PutFavoriteParams {
            /**
             * Id товара
             */
            itemId?: string | null;
            /**
             * Id пользователя
             */
            userId?: string | null;
            /**
             * Id списков избранных товаров
             */
            favoriteListIds?:string[];
        }

        /**
         * Список избранных товаров пользователя
         */
        export interface UserFavoriteList {
            /**
             * Id списка
             */
            favoriteListId?: string | null;
            /**
             * Название
             */
            name?: string | null;
            /**
             * кол-во
             */
            goodsCount: number;
        }
    }
    namespace Feedback {
        /**
         * Запрос на добавление сообщения к беседе
         */
        export interface FeedbackMessageRequest {
            /**
             * Uid беседы
             */
            dialogUid?: string; // uuid
            /**
             * Текст сообщения
             */
            message?: string | null;
        }

        /**
         * Запрос обратной связи
         */
        export interface FeedbackRequest {
            requestType?: /* Типы запроса обратной связи */ Mongo.RequestType;
            /**
             * Текст обращения
             */
            message?: string | null;
            /**
             * Имя пользователя
             */
            fio?: string | null;
            /**
             * Адрес электронной почты
             */
            email?: string | null;
            /**
             * Id города, в котором зарегестрировано обращение
             */
            cityId?: string | null;
            /**
             * Id заказа, связанного с сообщением
             */
            orderId?: string | null;
            requestLabel?: /* Оценка при обратной связи */ Mongo.RequestLabel;
        }
    }
    namespace Helpers {
        /**
         * Результаты для GroupInfo
         */
        export interface GroupInfoResult {
            groupType?: /* Тип */ Mongo.ItemType;
            /**
             * Данные по товарам в группе
             */
            groupItems?: /* Информация об уровне вариантов товаров в группе */ Mongo.ItemInfoLevel[] | null;
            /**
             * Общие св-ва
             */
            commonProperties?: /* Св-ва товара */ Mongo.ItemPropertyInfo[] | null;
            /**
             * Человекочитаемый Url
             */
            humanableUrl?: string | null;
            /**
             * Варианты по наборам товаров
             */
            goodSetItems?: /* asdf */ Mongo.ItemInfo[] | null;
            goodSetInfo?: /* Акционный набор товаров */ Mongo.IPhGoodSetWithPriceAndRest;
            preparationInfo?: /* Информация о препарейшине */ Mongo.IPharmInsInfo;
            /**
             * Суммарное кол-во товаров в группе
             */
            itemsTotalCount?: number; // int32
            /**
             * Требуется группировка товаров в выпадающий список на больших экранах (десктоп)
             */
            needGroupForLargeScreen?: boolean;
            /**
             * Требуется группировка товаров в выпадающий список на маленьких экранах (мобильные версии)
             */
            needGroupForSmallScreen?: boolean;
            outOfStock?: boolean;
        }
    }
    namespace ImageStorage {
        /**
         * ImageStorageModel
         */
        export interface ImageStorageModel {
            /**
             * Id
             */
            id?: string | null;
            /**
             * Имя
             */
            name?: string | null;
            /**
             * Путь
             */
            path?: string | null;
            photo?: /* Пути до фотографий */ Mongo.Photo;
            /**
             * Дата создания
             */
            created?: string; // date-time
        }
    }
    namespace Item {
        /**
         * Входные дынные для создания отзыва
         */
        export interface AddReviewRequest {
            /**
             * Id товара
             */
            itemId?: string | null;
            /**
             * Оценка
             */
            rating?: number; // int32
            /**
             * Отзыв
             */
            review?: string | null;
            /**
             * Имя пользователя
             */
            fio?: string | null;
        }

        /**
         * Входные дынные для редактирования отзыва
         */
        export interface EditReviewRequest {
            /**
             * Id отзыва
             */
            id?: string | null;
            /**
             * Оценка
             */
            rating?: number; // int32
            /**
             * Отзыв
             */
            review?: string | null;
            /**
             * Имя пользователя
             */
            fio?: string | null;
        }

        /**
         * Автор отзыва
         */
        export interface ReviewOwner {
            /**
             * Имя пользователя
             */
            readonly fio?: string | null;
            /**
             * Кол-во отзывов, оставленное пользователем
             */
            readonly itemReviewsCount?: number; // int32
        }

        /**
         * Отзыв пользователя на товар
         */
        export interface ReviewWithItemTotals {
            /**
             * Наименование товара
             */
            itemName?: string | null;
            /**
             * Рейтинг аптеки (от 1 до 5)
             */
            totalRating?: number; // double
            /**
             * Кол-во отзывов
             */
            totalReviewsCount?: number; // int32
            /**
             * Отзыв доступен для редактирования
             */
            readonly editable?: boolean;
            /**
             * Id отзыва
             */
            id?: string | null;
            /**
             * Id товара
             */
            itemId?: string | null;
            /**
             * Оценка
             */
            rating?: number; // int32
            /**
             * Комментарий
             */
            review?: string | null;
            /**
             * Дата составления отзыва
             */
            created?: string; // date-time
            /**
             * Покупал ли пользователь этот товар у нас
             */
            boughtFromUs?: boolean;
            /**
             * Дата последнего редактирования отзыва
             */
            edited?: string | null; // date-time
        }

        /**
         * Отзыв пользователя на товар
         */
        export interface ReviewWithOwner {
            /**
             * Id отзыва
             */
            readonly id?: string | null;
            /**
             * Оценка
             */
            readonly rating?: number; // int32
            /**
             * Комментарий
             */
            readonly review?: string | null;
            /**
             * Дата создания
             */
            readonly created?: string; // date-time
            /**
             * Покупал ли пользователь этот товар у нас
             */
            readonly boughtFromUs?: boolean;
            /**
             * Дата редактирования
             */
            readonly edited?: string | null; // date-time
            owner?: /* Автор отзыва */ ReviewOwner;
            /**
             * Отзыв доступен для редактирования
             */
            readonly editable?: boolean;
            /**
             * Текущий пользователь является автором данного комментария
             */
            readonly isOwner?: boolean;
        }
    }
    namespace MiniShop {
        /**
         * Тело запроса на удаление фото
         */
        export interface DeleteMiniShopImageDescription {
            /**
             * Id мини маркета
             */
            miniShopId?: string | null;
            miniShopPhotoType?: /* Тип фотографии */ MiniShopPhotoType;
        }

        /**
         * Модель данных для создания нового мини-магазина
         */
        export interface MiniShopAdminModel {
            /**
             * Активен
             */
            active?: boolean;
            /**
             * Название миинмаркета. (Заголовок на странице такового)
             */
            caption?: string | null;
            /**
             * Урл
             */
            urlPath?: string | null;
            /**
             * Перечень идентификаторов товаров и их групп
             */
            items?: string[] | null;
            /**
             * Дата начала
             */
            startDate?: string | null; // date-time
            /**
             * Дата конца
             */
            endDate?: string | null; // date-time
            /**
             * Подробное текстовое описание минимаркета. Поддерживает простейшие HTML тэги.
             */
            body?: string | null;
            /**
             * Разделенные группы
             */
            splitedGroups?: boolean;
        }

        /**
         * Тип фотографии
         */
        export type MiniShopPhotoType = 'FullPhoto' | 'SmallPhoto';

        /**
         * результат поиска по мини маркету
         */
        export interface MiniShopSearchResult {
            searchResult?: /* Результат поиска */ Shared.ItemGroupSearchResult;
            /**
             * Картинка
             */
            imagePath?: string | null;
            /**
             * Описание
             */
            caption?: string | null;
            /**
             * Подробное текстовое описание минимаркета. Поддерживает простейшие HTML тэги.
             */
            body?: string | null;
        }
    }
    namespace Mongo {
        /**
         * Прогрессирующая скидка по товару в корзине
         */
        export interface CartItemPrDisc {
            /**
             * Название скидки
             */
            discountName: string | null;
            /**
             * Кол-во товара в корзине
             */
            requiredAmount: number;
            /**
             * Купленное кол-во товара
             */
            redeemedAmount: number;
            /**
             * Кол-во выкупленного товара у текущего пользователя
             */
            confirmedAmount?: number;
            /**
             * Текущий процент по скидке
             */
            currentDiscount: number;
            /**
             * Текущий процент по скидке
             */
            currentDiscountSum: number;
            /**
             * Кол-во до следующего этапа
             */
            currentLimit: number;
            /**
             * Процент скидки на следующем этапе
             */
            nextDiscount: number | null;
            nextDiscountSum: number | null;
            /**
             * Скидка доступна для использования
             */
            isUsed: boolean;
        }
        /**
         * Акция
         */
        export interface ActionModel {
            /**
             * Идентификатор
             */
            id?: string | null;
            /**
             * Имя
             */
            name?: string | null;
            /**
             * Дата начала
             */
            startDate?: string; // date-time
            /**
             * Дата окончания
             */
            endDate?: string; // date-time
            /**
             * Список городов(опционально)
             */
            iPharmTownIds?: string[] | null;
            /**
             * Список идентификаторов товаров
             */
            goodUids?: string[] | null;
            /**
             * URL для апи запроса товаров(опционально)<br />
             * Примеры:<br />
             * Search/CategoryUrl?categoryUrl=derma_cosmetics%2Fsunscreen<br />
             * Search/ByPhrase?phrase=yjigf<br />
             * Brand/Items?brandUrl=NUTRILON<br />
             * Search/Mnn?interNames=%D0%9C%D0%B5%D1%82%D1%80%D0%BE%D0%BD%D0%B8%D0%B4%D0%B0%D0%B7%D0%BE%D0%BB<br />
             */
            apiSearchUrlPath?: string | null;
            /**
             * Символьный код
             */
            url?: string | null;
            /**
             * Тело (HTML)
             */
            body?: string | null;
            /**
             * Изображение
             */
            photoPath?: string | null;
            /**
             * Акция удалена
             */
            deleted?: string | null; // date-time
            /**
             * Идентификаторы групп товаров
             */
            itemGroupIds?: string[] | null;
            /**
             * Дата создания
             */
            creationDate?: string; // date-time
            /**
             * Кол-во просмотров
             */
            viewsCount?: number; // int32
            /**
             * Включить в рассылку
             */
            toMailing?: boolean;
            /**
             * Акция скоро закончится
             */
            readonly hotOffer?: boolean;
        }

        /**
         * Административная роль
         */
        export type AdminRole = 'Admin' | 'Marketing' | 'SuperUser' | 'Sales';

        /**
         * Администратор сайта
         */
        export interface AdminUserModel {
            /**
             * Идентификатор
             */
            id?: string | null;
            /**
             * Имя пользователя
             */
            userName?: string | null;
            /**
             * Роль
             */
            roles?: /* Административная роль */ AdminRole[] | null;
            /**
             * Дата создания
             */
            created?: string; // date-time
            /**
             * Дата последнего изменения пользователя
             */
            lastUpdate?: string | null; // date-time
            /**
             * Дата удаления
             */
            deleted?: string | null; // date-time
            /**
             * Кто создал данного пользователя
             */
            whoCreatedId?: string | null;
            /**
             * Кто последний раз редактировал
             */
            whoUpdatesLastId?: string | null;
            /**
             * Кто удалил
             */
            whoDeletedId?: string | null;
        }

        /**
         * Область на карте
         */
        export interface AreaRectangle {
            leftTopPoint?: /* Класс, описывающий точку на карте */ Point;
            rightBottomPoint?: /* Класс, описывающий точку на карте */ Point;
        }

        /**
         * Автор статьи
         */
        export interface ArticleAuthorModel {
            /**
             * Id объекта
             */
            id?: string | null;
            /**
             * Имя автора
             */
            name?: string | null;
            /**
             * Фото автора
             */
            photo?: string | null;
            /**
             * Дополнительная информация
             */
            additionalInfo?: string | null;
            /**
             * Кол-во статей, написанных автором
             */
            articlesCount?: number; // int32
        }

        /**
         * Модель категории статей
         */
        export interface ArticleCategoryModel {
            /**
             * Id объекта
             */
            id?: string | null;
            /**
             * Ссылка на категорию
             */
            url?: string | null;
            /**
             * Наименование категории
             */
            name?: string | null;
            /**
             * Кол-во статей в категории
             */
            articlesCount?: number; // int32
        }

        /**
         * Модель пункта доставки
         */
        export interface AutoDestModel {
            /**
             * Id объекта
             */
            id?: string | null;
            /**
             * Наименование пункта доставки
             */
            name?: string | null;
            /**
             * Наименование юридического лица
             */
            firmName?: string | null;
            /**
             * Адрес пункта доставки
             */
            address?: string | null;
            /**
             * Широта (значение)
             */
            latitudeNum?: number | null; // double
            /**
             * Долгота (значение)
             */
            longitudeNum?: number | null; // double
            /**
             * Телефоны пункта доставки
             */
            phones?: /* Модель номера телефона */ Phone[] | null;
            /**
             * Время работы пункта выдачи
             */
            workTime?: string | null;
            /**
             * Id города пункта выдачи
             */
            cityId?: string | null;
            /**
             * Район, в котором находится пункт выдачи
             */
            district?: string | null;
            /**
             * Станция метро, ближайшая к пункту выдачи
             */
            metro?: string | null;
            /**
             * наличие безналичного рассчета
             */
            cashless?: boolean;
            /**
             * Фотографии пункта доставки
             */
            fileInst?: /* Фото объекта */ FileInst[] | null;
            /**
             * Расписание отгрузки
             */
            deliveryDates?: /* Расписание отгрузки */ DeliveryDate[] | null;
            /**
             * Рейтинг аптеки (от 1 до 5)
             */
            rating?: number; // double
            /**
             * Кол-во отзывов
             */
            reviewsCount?: number; // int32
            /**
             * Кол-во положительных отзывов
             */
            positiveReviewsCount?: number; // int32
            /**
             * Кол-во отрицательных отзывов
             */
            negativeReviewsCount?: number; // int32
            /**
             * Собственная фирменная аптека
             */
            own?: boolean;
        }

        /**
         * Аптека требующая отзыва
         */
        export interface AutoDestNeedReview {
            /**
             * Идентификатор аптеки
             */
            autoDestId?: string;
            /**
             * Сумма заказа
             */
            totalOrderSum?: number; // int32
            /**
             * Номер заказа
             */
            orderId?: string;
        }

        /**
         * Модель отзыва
         */
        export interface AutoDestReviewModel {
            /**
             * Id объекта
             */
            id?: string | null;
            /**
             * Id пункта доставки
             */
            autoDestId?: string | null;
            /**
             * Наименование пункта доставки
             */
            autoDestName?: string | null;
            /**
             * Адрес пункта доставки
             */
            autoDestAddr?: string | null;
            /**
             * Оценка
             */
            rating?: number; // int32
            /**
             * Отзыв
             */
            review?: string | null;
            /**
             * Дата публикации отзыва
             */
            created?: string; // date-time
            /**
             * Дата редактирования отзыва
             */
            edited?: string | null; // date-time
            /**
             * Номер заказа, по которому сделан отзыв
             */
            orderNum?: string | null;
        }

        /**
         * Информация о баннере для отображения на сайте
         */
        export interface BannerInfoModel {
            /**
             * Картинка
             */
            readonly photoPath?: string | null;
            /**
             * Хинт
             */
            readonly hint?: string | null;
            /**
             * Ссылка на которую ведёт баннер
             */
            readonly url?: string | null;
        }
        export interface BannerInfo {
            /**
             * id
             */
            readonly id?: string | null;
            /**
             * Картинка
             */
            readonly photoPath?: string | null;
            /**
             * Картинка
             */
            readonly photoPathWebp?: string | null;
            /**
             * Хинт
             */
            readonly bannerName?: string | null;
            /**
             * Хинт
             */
            readonly hint?: string | null;
            /**
             * Ссылка на которую ведёт баннер
             */
            readonly url?: string | null;
            readonly imageSize?: ImageSize | null;
        }

        /**
         * Баннер
         */
        export interface BannerModel {
            /**
             * Id
             */
            id?: string | null;
            /**
             * Имя
             */
            name?: string | null;
            /**
             * Порядок сортировки (меньше число - приоритетнее в выдаче)
             */
            sortOrder?: null | number; // int32
            /**
             * Дата начала
             */
            startDate?: string; // date-time
            /**
             * Дата окончания
             */
            endDate?: string; // date-time
            /**
             * Картинка
             */
            photoPath?: string | null;
            /**
             * Дата создания
             */
            creationDate?: string; // date-time
            /**
             * Дата удаления
             */
            deleted?: string | null; // date-time
            /**
             * Хинт
             */
            hint?: string | null;
            /**
             * Ссылка на которую ведёт баннер
             */
            url?: string | null;
            /**
             * Идентификатор раздела банера
             */
            bannerSectionId?: string | null;
            /**
             * Контрольная сумма
             */
            crc32?: number; // int32
            /**
             * Дополнительные параметры (в виде строки для фронта)
             */
            readonly additionalParams?: string | null;
            /**
             * Города для которых банер активен
             */
            iPharmTownIds?: string[] | null;
            /**
             * Описание
             */
            description?: string | null;
        }

        /**
         * Раздел банеров
         */
        export interface BannerSectionModel {
            /**
             * Идентификатор
             */
            id?: string | null;
            /**
             * Имя
             */
            name?: string | null;
            /**
             * Url раздела
             */
            url?: string | null;
            /**
             * Обязательность дополнительного поля
             */
            mandatoryAdditionalField?: boolean;
            /**
             * Текстовое пояснение к дополнительному полю
             */
            additionalFieldDescription?: string | null;
            imageSize?: /* Размер картинки */ ImageSize;
        }

        /**
         * Информация по бренду
         */
        export interface BrandInfo {
            /**
             * Наименование бренда
             */
            name?: string | null;
            /**
             * Url бренда
             */
            url?: string | null;
        }

        /**
         * Бренд
         */
        export interface BrandModel {
            /**
             * Русское имя
             */
            nameRu?: string | null;
            /**
             * Слоган описания бренда
             */
            txtTitle?: string | null;
            /**
             * Текстовый блок описания бренда
             */
            txtBlock?: string | null;
            /**
             * Категории в которых есть товары по данному бренду
             */
            categories?: /* Модель категории */ CategoryModel[] | null;
            /**
             * Имя
             */
            name?: string | null;
            /**
             * Url бренда
             */
            url?: string | null;
            /**
             * Пути до фотографий
             */
            readonly photo?: string | null;
        }

        /**
         * Промо-код в корзине пользователя
         */
        export interface CartPromoCode {
            /**
             * Текст промо-кода
             */
            promoCode?: string | null;
            /**
             * Промо-код содержит ошибку
             */
            hasError?: boolean;
            /**
             * Код применен в корзине
             */
            isUsed?: boolean;
            /**
             * Примечание
             */
            hint?: string | null;
        }

        /**
         * Лимит на товар по промо-коду
         */
        export interface CartPromoGoodLimit {
            /**
             * Uid товара
             */
            sourceUid?: string; // uuid
            /**
             * Лимит на товар
             */
            limit?: number; // int32
            /**
             * Лимит по текущей корзине
             */
            currentAmount?: number; // int32
        }

        /**
         * Банер для каталога
         */
        export interface CategoryBannerFullModel {
            /**
             * Uid банера для каталога
             */
            uid?: string; // uuid
            /**
             * Наименование
             */
            name?: string | null;
            /**
             * Ссылка
             */
            url?: string | null;
            /**
             * Начало действия банера
             */
            start?: string; // date-time
            /**
             * Окончание действия банера
             */
            end?: string; // date-time
            photo?: /* Пути до фотографий */ Photo;
        }

        /**
         * Модель категории
         */
        export interface CategoryModel {
            /**
             * Имя категории
             */
            name?: string | null;
            /**
             * Имя для урла
             */
            url?: string | null;
            /**
             * Подкатегория
             */
            subGroup?: /* Модель категории */ CategoryModel[] | null;
            /**
             * Количество товаров в разделе
             */
            itemsCount?: number; // int32
            photo?: /* Пути до фотографий */ Photo;
            /**
             * Банеры
             */
            banners?: /* Банер для каталога */ CategoryBannerFullModel[] | null;
        }

        /**
         * Модель записи о благотворительном взносе
         */
        export interface CharityModel {
            /**
             * Кол-во витаминок
             */
            vitamins?: number; // int32
            /**
             * Дата пожертвования
             */
            donated?: string; // date-time
            /**
             * Скрыть в рэйтинге
             */
            hideInRating?: boolean;
        }

        /**
         * Модель города
         */
        export interface CityModel {
            /**
             * Id объекта
             */
            id?: string | null;
            /**
             * Название
             */
            name?: string | null;
            /**
             * Имя в предложном падеже
             */
            prepositionalName?: string | null;
            /**
             * Регион
             */
            state?: string | null;
            /**
             * Ссылка
             */
            url?: string | null;
            centerCoords?: /* Класс, описывающий точку на карте */ Point;
            areaRectangle?: /* Область на карте */ AreaRectangle;
            /**
             * Кол-во пунктов доставки
             */
            autoDestCount?: number; // int32
            /**
             * Адрес филиала
             */
            regBodyAddress?: string | null;
            /**
             * Близлежащие города
             */
             nearCities: null | NearCity[];
            deliveryDate?: /* Расписание отгрузки */ Mongo.DeliveryDate;
        }

        /**
         * Близлижайший город
         */
        export interface NearCity {
            /**
             * Id города
             */
            id?:string;
            /**
             * Название города
             */
            name?:string;
            /**
             * Регион
             */
            state?:string;
            /**
             * Расстояние от центра до центра (в метрах)
             */
            distance?:number;
        }

        /**
         * Произвольные составные наименования
         */
        export interface CustomField {
            /**
             * Имя параметра
             */
            key?: string | null;
            /**
             * Значение параметра
             */
            value?: string | null;
        }

        /**
         * Отладочная инфомрация по группе товаров
         */
        export interface DebugInfo {
            /**
             * Значение рейтинга по слову
             */
            wordRating?: number | null; // double
            /**
             * Значение рейтинга по МНН
             */
            mnnRating?: number | null; // double
            /**
             * Поисковые теги привязанные к группе
             */
            searchTags?: string[] | null;
            /**
             * Синонимы, привязанные к группе.
             */
            synonyms?: string[] | null;
        }

        /**
         * Расписание отгрузки
         */
        export interface DeliveryDate {
            /**
             * Планируемая дата и время начала доставки
             */
            shipTimeStart?: string | null; // date-time
            /**
             * Планируемая дата и время окончания доставки
             */
            shipTimeEnd?: string | null; // date-time
            /**
             * Крайнее время включения в отгрузку
             */
            deadline?: string | null; // date-time
        }

        /**
         * Статус обращения
         */
        export type DialogStatus = 'Pending' | 'Answered' | 'Closed';

        /**
         * Цены со скидками
         */
        export interface Discount {
            /**
             * Guid дискаунта
             */
            uid?: string; // uuid
            /**
             * Цена с дискаунтом
             */
            price?: number; // double
            /**
             * Сумма скидки от первоначальной цены
             */
            profit?: number; // double
        }

        /**
         * Тип шаблона
         */
        export type EmailTemplateType =
            'EmailConfirmation'
            | 'WithoutBuying'
            | 'GoodsTable'
            | 'Good'
            | 'OrderCreatedGood'
            | 'OrderCreatedSpacer'
            | 'OrderCreated'
            | 'OrderSendedToAutoDest'
            | 'ItemOnSale'
            | 'FakeGood'
            | 'WithoutOrders'
            | 'AutoDestReview'
            | 'NewActions'
            | 'Action'
            | 'AbandonedCart'
            | 'AbandonedCartGood'
            | 'AbandonedCartAnalogGood'
            | 'AbandonedCartDefferedBlock'
            | 'AbandonedCartDiscountBlock'
            | 'AbandonedCartUsefullCollection'
            | 'AbandonedCartAnalogBlock'
            | 'AbandonedCartAction'
            | 'AbandonedCartActionBlock';

        /**
         * Фото объекта
         */
        export interface FileInst {
            /**
             * Путь до файла
             */
            filePath?: string | null;
            photos?: /* Пути до фотографий */ Photo;
        }

        /**
         * Атрибут категории
         */
        export interface FilterAttribute {
            /**
             * Урл атрибута
             */
            url?: string | null;
            /**
             * Uid атрибута
             */
            uid?: string; // uuid
            /**
             * Имя атрибута
             */
            name?: string | null;
            type?: /* Тип атрибута категории */ FilterAttributeType;
            /**
             * Порядок отображения
             */
            sortOrder?: null | number; // int32
            /**
             * Перечислимое значение
             */
            choiceValues?: /* Перечислимое значение */ FilterChoiceValue[] | null;
            /**
             * Минимальное значение атрибута
             */
            fromValue?: number; // double
            /**
             * Максимальное значение атрибута
             */
            toValue?: number; // double
        }

        /**
         * Тип атрибута категории
         */
        export type FilterAttributeType = 'Mark' | 'Str' | 'Num' | 'Range' | 'Choice' | 'Multi';

        /**
         * Перечислимое значение
         */
        export interface FilterChoiceValue {
            /**
             * Uid перечислимого значения
             */
            uid?: string; // uuid
            /**
             * Имя перечислимого значения
             */
            name?: string | null;
            /**
             * Имя перечислимого значения для сео
             */
            humanName?: string | null;
            /**
             * Url значения
             */
            url?: string | null;
            /**
             * Порядок отображения
             */
            sortOrder?: null | number; // int32
        }

        /**
         * Пол
         */
        export type Gender = 'NotSpecified' | 'Male' | 'Female';

        /**
         * Атрибуты товара
         */
        export interface GoodAttributeForSearch {
            /**
             * Тип атрибута. Значения: Mark, Str, Num, Range, Choice, Multi
             */
            type?: string | null;
            /**
             * Bool. Атрибут = True/False
             */
            markValue?: boolean;
            /**
             * Строковое значение
             */
            strValue?: string | null;
            /**
             * Money. Числовое значение
             */
            numValue?: number; // double
            /**
             * Money. Начало числового диапазона
             */
            fromValue?: number; // double
            /**
             * Money. Конец числового диапазона
             */
            toValue?: number; // double
        }

        /**
         * Тематический каталог товаров.
         */
        export interface GoodGroup {
            /**
             * Uid тематической группы
             */
            uid?: string; // uuid
            /**
             * Имя тематической группы
             */
            name?: string | null;
            /**
             * Url тематической группы
             */
            url?: string | null;
            subGroup?: /* Тематический каталог товаров. */ GoodGroup;
        }

        /**
         * Тематическая группа
         */
        export interface GoodGroupModel {
            /**
             * Uid тематической группы
             */
            uid?: string; // uuid
            /**
             * Имя тематической группы
             */
            name?: string | null;
            /**
             * Url тематической группы
             */
            url?: string | null;
            /**
             * Подгруппа
             */
            subGroup?: /* Тематическая группа */ GoodGroupModel[] | null;
            /**
             * Кол-во товаров в категории
             */
            itemsCount?: number; // int32
        }

        /**
         * Модель категории для товара
         */
        export interface GoodItemCategory {
            /**
             * Имя категории
             */
            name?: string | null;
            /**
             * Имя для урла
             */
            url?: string | null;
            subCategory?: /* Модель категории для товара */ GoodItemCategory;
        }

        /**
         * Составное наименование товара
         */
        export interface GoodNaming {
            /**
             * Торговое наименование товара
             */
            tradeName?: string | null;
            /**
             * Дозировка
             */
            dosage?: number | null; // double
            /**
             * Доп строка к дозировке
             */
            dosageStr?: string | null;
            /**
             * вторая доп строка к дозировке
             */
            dosageAdd?: string | null;
            /**
             * количество в упаковке
             */
            packing?: number | null; // double
            /**
             * первичная упаковка, краткое имя
             */
            primaryPackingShort?: string | null;
            /**
             * первичная упаковка, полное имя
             */
            primaryPackingFull?: string | null;
            /**
             * форма выпуска, краткое имя
             */
            formReleaseShort?: string | null;
            /**
             * форма выпуска, полное имя
             */
            formReleaseFull?: string | null;
            /**
             * порма выпуска, доп строка
             */
            formReleaseAdd?: string | null;
            /**
             * особенности
             */
            features?: string | null;
            /**
             * Дополнительные параметры наименования
             */
            customFields?: /* Произвольные составные наименования */ CustomField[] | null;
        }

        /**
         * GoodSetInfo
         */
        export interface GoodSetInfo {
            /**
             * Id
             */
            id?: string | null;
            /**
             * Имя
             */
            name?: string | null;
            /**
             * Фото
             */
            photos?: /* Пути до фотографий */ Photo[] | null;
        }

        /**
         * Бренд в товаре
         */
        export interface GoodVendorBrand {
            /**
             * Урл бренда
             */
            url?: string | null;
            /**
             * Имя бренда
             */
            name?: string | null;
        }

        /**
         * Линейка бренда в товаре
         */
        export interface GoodVendorLine {
            /**
             * Урл линейки бренда
             */
            url?: string | null;
            /**
             * Имя линейки бренда
             */
            name?: string | null;
        }

        /**
         * Модель товара
         */
        export interface GoodVendorModel {
            /**
             * Id объекта
             */
            id?: string | null;
            /**
             * GUID объекта
             */
            uid?: string; // uuid
            /**
             * Имя товара
             */
            name?: string | null;
            /**
             * Наименование для сайта user-friendly.
             */
            humanReadableName?: string | null;
            /**
             * Наименование производителя
             */
            vendor?: string | null;
            /**
             * Страна производитель
             */
            country?: string | null;
            /**
             * Штрих-код по товару
             */
            barCode?: string | null;
            /**
             * Номер рег.удостоверения
             */
            regCert?: string | null;
            /**
             * Код Видаля
             */
            vidalCode?: string | null;
            goodGroupInfo?: /* Тематический каталог товаров. */ GoodGroup;
            /**
             * особенности продажи (строка)
             */
            goodMove?: string | null;
            /**
             * Рецептурный препарат
             */
            prescriptionDrug?: boolean;
            /**
             * Артикул
             */
            articul?: string | null;
            /**
             * обосые уловия
             */
            cautions?: string | null;
            /**
             * общее описание
             */
            genDesc?: string | null;
            /**
             * Описание
             */
            pharmAct?: string | null;
            /**
             * Фармакокинетика
             */
            pharmKin?: string | null;
            /**
             * Фармакодинамика
             */
            pharmDyn?: string | null;
            /**
             * Состав
             */
            struct?: string | null;
            /**
             * побочные действия
             */
            sideEff?: string | null;
            /**
             * Комплектация
             */
            goodKit?: string | null;
            /**
             * противопоказания
             */
            contraIndic?: string | null;
            /**
             * взаимодействие с другими лекарственными средствами
             */
            drugInter?: string | null;
            /**
             * способы применения
             */
            dosage?: string | null;
            /**
             * особые условия хранения
             */
            keepSpecial?: string | null;
            /**
             * Ссылка из свойства ссылкан а видео
             */
            videoLink?: string | null;
            /**
             * текст из свойства красткое описание
             */
            shortDesc?: string | null;
            /**
             * Bool. Хранить в сухом месте
             */
            keepDry?: boolean | null;
            /**
             * Bool. беречь от детей
             */
            keepChild?: boolean | null;
            /**
             * Bool. хранить в защищенном от света месте
             */
            keepLight?: boolean | null;
            /**
             * хранить при температуре от
             */
            keepTempFrom?: null | number; // int32
            /**
             * Хранить при температуре до
             */
            keepTempTo?: null | number; // int32
            /**
             * Bool. Новинка
             */
            newGood?: boolean;
            /**
             * Bool. Жизненно важные
             */
            vitImport?: boolean;
            /**
             * Bool. По товару стоит метка "Благотв. Фонд"
             */
            fund?: boolean;
            /**
             * Bool. По товару стоит метка «Не Дженерик»
             */
            notGeneric?: boolean;
            /**
             * Bool. Хранить в холодильнике
             */
            fridge?: boolean;
            /**
             * Bool. Ограничение по кол-ву для продажи. Значение кол-ва - в пакете с прайсом
             */
            saleLimit?: boolean;
            /**
             * Bool. Рецепт остается в аптеке
             */
            recipeInPh?: boolean;
            /**
             * Форма выпуска и упаковка
             */
            release?: string | null;
            /**
             * Лекарственная форма
             */
            dosDesc?: string | null;
            /**
             * показания
             */
            indic?: string | null;
            /**
             * передозировка
             */
            overdose?: string | null;
            photoPack?: /* 3D фото упаковки */ PhotoPack;
            /**
             * Фасовка (Составное наименование товара-Фасовка)
             */
            packing?: string | null;
            /**
             * Значение Фасовка или Значение для расчета за ед.
             */
            packingCalc?: string | null;
            packDesc?: /* Описание фасовки */ PackDesc;
            /**
             * Bool. Скрывать информацию о фасовке на сайте
             */
            hidePacking?: boolean;
            /**
             * Имя бренда. На языке оригинала
             */
            brand?: string | null;
            /**
             * Перечень тэгов по товару
             */
            readonly tags?: string[] | null;
            /**
             * Список действующих веществ
             */
            interNames?: /* Действующее вещество */ InterNames[] | null;
            category?: /* Модель категории для товара */ GoodItemCategory;
            /**
             * Список атрибутов
             */
            readonly attributes?: /* Атрибуты товара */ GoodAttributeForSearch[] | null;
            /**
             * фотографии товара
             */
            fileInst?: /* Фото объекта */ FileInst[] | null;
            /**
             * Id группы товаров
             */
            itemGroupId?: string | null;
            goodNaming?: /* Составное наименование товара */ GoodNaming;
            claimsOrganization?: /* Организация по приему вопросов и претензий */ ClaimsOrganization | null;
            /**
             * Урл Good Gorup
             */
            goodGroupUrl?: string | null;
            /**
             * Урл категории
             */
            categoryUrl?: string | null;
            /**
             * Человекочитаемый Url
             */
            humanableUrl?: string | null;
            /**
             * Промо витаминки
             */
            promoVits?: number; // int32
            brandDescription?: /* Бренд в товаре */ GoodVendorBrand;
            lineDescription?: /* Линейка бренда в товаре */ GoodVendorLine;
            /**
             * Пути до фотографий
             */
            photos?: /* Пути до фотографий */ Photo[] | null;
            reviewType?: /* Тип отзывов */ ReviewType;
            /**
             * Итоговый рэйтинг
             */
            rating?: number | null; // double
            /**
             * Кол-во отзывов
             */
            reviewsCount?: number; // int32
            /**
             * Кол-во положительных отзывов
             */
            positiveReviewsCount?: number; // int32
            /**
             * Кол-во отрицательных отзывов
             */
            negativeReviewsCount?: number; // int32
            /**
             * В избранном у пользователя
             */
            readonly inFavorites?: boolean;
            /**
             * Скрывать блок отображения аналогов
             */
            hideMnn: boolean;
            /**
             * Имя фармакотерапевтической группы
             */
            pharmGroupName?: string;
        }

        /**
         * Тип хинта
         */
        export type HintType = 'Name' | 'Vendor' | 'ActiveSubstance' | 'Symptom' | 'Action';

        /**
         * Модель диалога обратной связи
         */
        export interface IPhDialogModel {
            /**
             * Uid диалога
             */
            uid?: string; // uuid
            typeCode?: /* Типы запроса обратной связи */ RequestType;
            /**
             * Id заказа
             */
            orderId?: string | null;
            /**
             * Номер заказа, связанного с обращением
             */
            orderNum?: string | null;
            /**
             * Оценка при обратной связи
             */
            iPhComment?: RequestLabel;
            /**
             * Кол-во сообщений в диалоге
             */
            messagesCount?: number; // int32
            /**
             * Кол-во непрочитанных сообщений в диалоге
             */
            unreadMessagesCount?: number; // int32
            /**
             * Статус обращения
             */
            status?: DialogStatus;
            /**
             * Сообщения беседы
             */
            messages?: /* Сообщение диалога обратной связи */ IPhDlgMsg[] | null;
        }

        /**
         * Сообщение диалога обратной связи
         */
        export interface IPhDlgMsg {
            /**
             * Время создания
             */
            created?: string; // date-time
            /**
             * Текст сообщения
             */
            text?: string | null;
            /**
             * Имя автора
             */
            fullName?: string | null;
        }

        /**
         * GoodSetItem
         */
        export interface IPhGoodSetItem {
            /**
             * SourceUid товара
             */
            sourceUid?: string; // uuid
            /**
             * Количество входящее в набор
             */
            amount?: number; // int32
        }

        /**
         * Организация по приему вопросов и претензий
         */
        export interface ClaimsOrganization {
            /**
             * Наименование
             */
            nameClaims?: string | null;
            /**
             * Страна
             */
            countryClaims?: string | null;
            /**
             * Адрес
             */
            addressClaims?: string | null;
            /**
             * Номер телефона
             */
            numberClaims?: string | null;
            /**
             * Электрнная почта
             */
            emailClaims?: string | null;
            /**
             * Сайт
             */
            websiteClaims?: string | null;
        }

        /**
         * Акционный набор товаров
         */
        export interface IPhGoodSetWithPriceAndRest {
            /**
             * Цена
             */
            readonly price?: number; // double
            /**
             * Цена без скидки
             */
            readonly noDiscPrice?: number; // double
            /**
             * Выгода
             */
            readonly profit?: number; // double
            /**
             * Id
             */
            id?: string | null;
            /**
             * Имя
             */
            name?: string | null;
            /**
             * Комментарий
             */
            comment?: string | null;
            /**
             * Товары
             */
            items?: /* GoodSetItem */ IPhGoodSetItem[] | null;
            /**
             * Id группы товаров
             */
            itemGroupId?: string | null;
            /**
             * Пути до фото
             */
            fileInst?: /* Фото объекта */ FileInst[] | null;
            /**
             * Человекочитаемый Url
             */
            humanableUrl?: string | null;
            /**
             * Количество единиц данного товара в корзине
             */
            amountInCart?: number; // int32
            /**
             * Рецептурный препарат
             */
            prescriptionDrug?: boolean;
            /**
             * Благ. фонд
             */
            fund?: boolean;
            /**
             * Оригинальный препарат
             */
            notGeneric?: boolean;
            /**
             * Рецепт остаётся в аптеке
             */
            recipeInPh?: boolean;
            /**
             * Витаминок будет начислено
             */
            readonly vitaminsToBeCredited?: number;
            /**
             * Процент скидки
             */
            readonly discountPercent?: number;
        }

        /**
         * Лимит на товар по промо-коду
         */
        export interface IPhPromoGood {
            /**
             * Uid товара
             */
            uid?: string; // uuid
            /**
             * Лимит на товар
             */
            limit?: number; // int32
        }

        /**
         * Модель промо-кода
         */
        export interface IPhPromoModel {
            /**
             * Текст промо-кода
             */
            name?: string | null;
            /**
             * Дата начала действия промо-кода
             */
            startDate?: string; // date-time
            /**
             * Дата окончания действия промо-кода
             */
            endDate?: string; // date-time
            /**
             * Разовый промо-код
             */
            onceOnly?: boolean;
        }

        /**
         * Информация о препарейшине
         */
        export interface IPharmInsInfo {
            /**
             * Имя
             */
            name?: string | null;
            /**
             * Урл
             */
            url?: string | null;
        }

        /**
         * Инструкция
         */
        export interface IPharmInsModel {
            /**
             * Uid инструкции
             */
            uid?: string; // uuid
            /**
             * Путь до инструкции
             */
            urlPath?: string | null;
            /**
             * Наименование инструкции
             */
            name?: string | null;
            /**
             * Производитель
             */
            vendor?: string | null;
            /**
             * Страна происхождения
             */
            country?: string | null;
            /**
             * Группа товаров
             */
            goodGroup?: string | null;
            /**
             * Особенности продажи
             */
            goodMove?: string | null;
            /**
             * Общее описание
             */
            genDesc?: string | null;
            /**
             * Комплектация
             */
            goodKit?: string | null;
            /**
             * Форма выпуска
             */
            release?: string | null;
            /**
             * Описание лек форомы
             */
            dosDesc?: string | null;
            /**
             * Фармакологическое действие
             */
            pharmAct?: string | null;
            /**
             * Фармакокинетика
             */
            pharmKin?: string | null;
            /**
             * Особые условия
             */
            cautions?: string | null;
            /**
             * Состав
             */
            struct?: string | null;
            /**
             * Показания
             */
            indic?: string | null;
            /**
             * Противопоказания
             */
            contrIndic?: string | null;
            /**
             * Способы применения
             */
            dosage?: string | null;
            /**
             * Применение при беременности и кормлении грудью
             */
            pregnancy?: string | null;
            /**
             * Побочные действия
             */
            sideEff?: string | null;
            /**
             * Взаимодействие с другими лек средствами
             */
            drugInter?: string | null;
            /**
             * Взаимодействие с алкоголем
             */
            alcoInter?: string | null;
            /**
             * Передозировка
             */
            overdose?: string | null;
            /**
             * Особые условия хранения
             */
            keepSpecial?: string | null;
            /**
             * Синонимы
             */
            synonym?: string | null;
            /**
             * Хранить в сухом месте
             */
            keepDry?: boolean;
            /**
             * Хранить при температуре от
             */
            keepTempFrom?: null | number; // int32
            /**
             * Хранить при температуре до
             */
            keepTempTo?: null | number; // int32
            /**
             * Беречь от детей
             */
            keepChild?: boolean;
            /**
             * Хранить в защищенном от света месте
             */
            keepLight?: boolean;
            /**
             * Жизненно важные
             */
            vitImport?: boolean;
            /**
             * Не дженерик
             */
            notGeneric?: boolean;
            category?: /* Данные по каталогу */ Shared.GroupInfo;
            goodGroupInfo?: /* Данные по каталогу */ Shared.GroupInfo;
        }

        /**
         * Модель города
         */
        export interface IPharmTownModel {
            /**
             * Id объекта
             */
            id?: string | null;
            /**
             * Название города
             */
            name?: string | null;
            /**
             * Длина диагонали
             */
            diagonalDistance?: number; // double
            /**
             * Дата прайслиста
             */
            priceListDate?: string | null; // date-time
        }

        /**
         * Размер картинки
         */
        export interface ImageSize {
            /**
             * Ширина
             */
            width?: number; // int32
            /**
             * Высота
             */
            height?: number; // int32
        }

        /**
         * Действующее вещество
         */
        export interface InterNames {
            /**
             * Имя действующего вещества
             */
            name?: string | null;
        }

        /**
         * Приведеный пользователь
         */
        export interface InvitedUser {
            /**
             * Дата регистрации
             */
            regDate?: string | null; // date-time
            /**
             * Дата первого заказа
             */
            orderDate?: string | null; // date-time
            /**
             * Номер телефона пользователя
             */
            custPhone?: string | null;
            /**
             * Учавствует в программе до
             */
            validUntil?: string; // date-time
        }

        /**
         * Атрибут группы товаров
         */
        export interface ItemGroupAttrib {
            /**
             * Урл атрибута
             */
            url?: string | null;
            /**
             * Урлы значений атрибута
             */
            urlValues?: string[] | null;
            /**
             * Числовые значения
             */
            numValues?: /* Числовое значение атрибута */ ItemGroupAttributeNumValue[] | null;
            catalogType?: /* Тип каталога для поиска. */ Search.CatalogType;
        }

        /**
         * Числовое значение атрибута
         */
        export interface ItemGroupAttributeNumValue {
            /**
             * Минимальное значение
             */
            min?: number; // double
            /**
             * Максимальное значение
             */
            max?: number; // double
        }

        /**
         * asdf
         */
        export interface ItemInfo {
            /**
             * Идентификатор товара
             */
            id?: string | null;
            /**
             * Наименование товара.
             */
            name?: string | null;
            /**
             * Производитель товара
             */
            vendor?: string | null;
            brand?: /* Информация по бренду */ BrandInfo;
            line?: /* Информация по линейке бренда */ LineInfo;
            /**
             * Показания к применению
             */
            indic?: string | null;
            /**
             * Рейтинг
             */
            itemRankValue?: number; // double
            /**
             * Bool. По товару стоит метка "Благотв. Фонд"
             */
            fund?: boolean;
            /**
             * Bool. По товару стоит метка «Не Дженерик»
             */
            notGeneric?: boolean;
            /**
             * Промо витаминки
             */
            promoVits?: number; // int32
            /**
             * Витаминок будет начислено при выполнении заказа
             */
            vitaminsToBeCredited?: number; // int32
            /**
             * Рецептурный препарат
             */
            prescriptionDrug?: boolean;
            /**
             * Bool. Рецепт остается в аптеке
             */
            recipeInPh?: boolean;
            /**
             * Ссылка из свойства ссылкан а видео
             */
            videoLink?: string | null;
            /**
             * Bool. Ограничение по кол-ву для продажи. Значение кол-ва - в пакете с прайсом
             */
            saleLimit?: boolean;
            photoPack?: /* 3D фото упаковки */ PhotoPack;
            category?: /* Модель категории для товара */ GoodItemCategory;
            goodGroupInfo?: /* Тематический каталог товаров. */ GoodGroup;
            /**
             * Набор фотографий по товару
             */
            fileInst?: /* Фото объекта */ FileInst[] | null;
            /**
             * Набор значений вариантов данного товара.
             */
            variantValues?: {
                [name: string]: string;
            } | null;
            /**
             * Действующие в-ва по товару
             */
            interNames?: string[] | null;
            packInfo?: /* Информация по упаковке */ PackPriceInfo;
            /**
             * Цена за товар с учётом дисконтной скидки (если она есть)
             */
            readonly price?: number; // double
            /**
             * Цена за товар без учёта скидок
             */
            readonly noDiscPrice?: number; // double
            /**
             * Скидка на товар с учётом дисконтной скидки (если она есть)
             */
            readonly profit?: number; // double
            /**
             * Процент скидки
             */
            readonly discountPercent?: number;
            /**
             * Дата ожидаемого прихода
             */
            readonly incoming?: string; // string($date-time)
            /**
             * Количество единиц данного товара в корзине
             */
            amountInCart?: number; // int32
            /**
             * Кол-во данного товара в наборе
             */
            amountInGoodSet?: number; // int32
            /**
             * Человекочитаемый Url
             */
            humanableUrl?: string | null;
            /**
             * Товар в избранном пользоватееля
             */
            isInFavorites?: boolean;
            /**
             * Товар для открытия на странице по умолчанию.
             */
            default?: boolean;
            iPhGoodSetsIds?: string[] | null;
            iPhGoodSetsDetails?: GoodSetDetails[] | null;
            outOfStock?: boolean;
            /**
             * Есть активная прогрессирующая скидка
             */
            hasProgressiveDiscount?: boolean;
            /**
             * Uid товара
             */
            sourceUid?: string; // uuid
        }

        /**
         * Дополнительная информация по наборам
         */
        export interface GoodSetDetails {
            /**
             * Идентификатор набора
             */
            id?: string | null;
            /**
             * Наименование набора
             */
            name?: string | null;
            /**
             * Цена набора
             */
            readonly price?: number; // double
            /**
             * Цена за набор без учёта скидок
             */
            readonly noDiscPrice?: number; // double
            /**
             * Фотографии товаров
             */
            photos?: /* Пути до фотографий */ Photo | null;
        }

        /**
         * Информация об уровне вариантов товаров в группе
         */
        export interface ItemInfoLevel {
            /**
             * Текстовое описание уровня
             */
            levelDescription?: string | null;
            /**
             * Текстовое название уровня
             */
            levelName?: string | null;
            /**
             * Краткое текстовое название уровня
             */
            levelNameShort?: string | null;
            levelType?: /* Тип уровня варианта */ VariantType;
            /**
             * Набор товаров, входящих в этот уровень.
             */
            itemInfos?: /* asdf */ ItemInfo[] | null;
            /**
             * Выбранный по умолчанию уровень товаров.
             */
            default?: boolean;
            /**
             * Общие св-ва
             */
            commonProperties?: /* Св-ва товара */ ItemPropertyInfo[] | null;
        }

        /**
         * Св-ва товара
         */
        export interface ItemPropertyInfo {
            /**
             * Название свойства
             */
            description?: string | null;
            /**
             * Значение свойства
             */
            name?: string | null;
            /**
             * Короткая версия значения св-ва
             */
            nameShort?: string | null;
            type?: /* Тип уровня варианта */ VariantType;
        }

        /**
         * Модель отзыва на товар
         */
        export interface ItemReviewModel {
            /**
             * Id отзыва
             */
            id?: string | null;
            /**
             * Id товара
             */
            itemId?: string | null;
            /**
             * Оценка
             */
            rating?: number; // int32
            /**
             * Комментарий
             */
            review?: string | null;
            /**
             * Дата составления отзыва
             */
            created?: string; // date-time
            /**
             * Покупал ли пользователь этот товар у нас
             */
            boughtFromUs?: boolean;
            /**
             * Дата последнего редактирования отзыва
             */
            edited?: string | null; // date-time
        }

        /**
         * Тип
         */
        export type ItemType = 'GoodVendor' | 'IPhGoodSet';

        /**
         * Линейка бренда
         */
        export interface Line {
            /**
             * Имя
             */
            name?: string | null;
            /**
             * Урл линейки
             */
            url?: string | null;
        }

        /**
         * Информация по линейке бренда
         */
        export interface LineInfo {
            /**
             * Наименование линейки бренда
             */
            name?: string | null;
            /**
             * Url линейки бренда
             */
            url?: string | null;
        }

        /**
         * Мини маркет
         */
        export interface MiniShopModel {
            /**
             * Id
             */
            id?: string | null;
            /**
             * Активен
             */
            active?: boolean;
            /**
             * Название миинмаркета. (Заголовок на странице такового)
             */
            caption?: string | null;
            /**
             * Урл
             */
            urlPath?: string | null;
            /**
             * Перечень идентификаторов товаров и их групп для отображения на странице минимаркета.
             */
            items?: string[] | null;
            /**
             * Categories
             * Для изменения набора используйте метод SetItems
             */
            categories?: /* Модель категории для товара */ GoodItemCategory[] | null;
            /**
             * GoodGroups
             * Для изменения набора используйте метод SetItems
             */
            goodGroups?: /* Тематический каталог товаров. */ GoodGroup[] | null;
            /**
             * Дата начала
             */
            startDate?: string | null; // date-time
            /**
             * Дата конца
             */
            endDate?: string | null; // date-time
            /**
             * Сортировка
             */
            sortOrder?: number; // int32
            /**
             * Подробное текстовое описание минимаркета. Поддерживает простейшие HTML тэги.
             */
            body?: string | null;
            /**
             * Большая фотография минимаркета
             */
            fullPhotoPath?: string | null;
            /**
             * Маленькая фотография минимаркета
             */
            smallPhotoPath?: string | null;
            /**
             * Дата создания
             */
            creationDate?: string; // date-time
            /**
             * Дата удаления
             */
            deleted?: string | null; // date-time
            /**
             * Разделенные группы
             */
            splitedGroups?: boolean;
        }

        /**
         * Потребность для каталога
         */
        export interface NeedIaCatModel {
            /**
             * Наименование
             */
            name?: string | null;
            /**
             * Url для потребности
             */
            url?: string | null;
            /**
             * Фото объекта
             */
            fileInst?: /* Фото объекта */ FileInst[] | null;
        }

        /**
         * Лимиты Олекстры на товары
         */
        export interface OlekstraLimit {
            /**
             * Id товара
             */
            productId?: string | null;
            /**
             * Uid товара
             */
            productUid?: string; // uuid
            /**
             * Лимит
             */
            limit?: number; // int32
        }

        /**
         * Модель запроса на блокировку заказа
         */
        export interface OrderBlockModel {
            /**
             * Дата запроса на блокировку
             */
            needBlock?: string; // date-time
            status?: /* Статус блокировки заказа */ OrderBlockStatus;
            /**
             * Дата окончания блокировки
             */
            blockedTill?: string | null; // date-time
        }

        /**
         * Статус блокировки заказа
         */
        export type OrderBlockStatus = 'Sending' | 'Sended' | 'Success' | 'Error';

        /**
         * Позиция в заказе
         */
        export interface OrderItem {
            itemType?: /* Тип */ ItemType;
            /**
             * Id объекта-товара
             */
            itemId?: string | null;
            /**
             * Имя товара
             */
            itemName?: string | null;
            /**
             * Производитель
             */
            vendor?: string | null;
            /**
             * Кол-во товара
             */
            amount?: number; // int32
            /**
             * Конечная цена товара
             */
            price?: number; // double
            /**
             * Цена товара без скидок
             */
            noDiscPrice?: number; // double
            /**
             * Цена с учетом дисконтной карты и прогрессирующей скидки
             */
            cartPrice?: number; // double
            /**
             * Выгода с учетом дисконтной карты и прогрессирующей скидки
             */
            cartProfit?: number; // double
            /**
             * Выгода с учетом дисконтной карты и прогрессирующей скидки (в процентах)
             */
            cartProfitPercent?: number; // double
            /**
             * Кол-во доп витаминок по позиции
             */
            extraVits?: number; // int32
            fileInst?: /* Фото объекта */ FileInst;
            goodSetInfo?: /* GoodSetInfo */ GoodSetInfo;
            /**
             * Рецепт остаётся в аптеке
             */
            recipeInPh?: boolean;
            /**
             * Рецептруный препарат
             */
            prescriptionDrug?: boolean;
        }

        /**
         * Модель заказа
         */
        export interface OrderModel {
            /**
             * Id объекта
             */
            id?: string | null;
            /**
             * Активный заказ.
             */
            inWork?: boolean;
            /**
             * Id города
             */
            iPharmTownId?: string | null;
            /**
             * Наименование города ИА
             */
            iPharmTownName?: string | null;
            /**
             * Наименование филиала
             */
            regBodyName?: string | null;
            /**
             * Id пункта доставки
             */
            autoDestId?: string | null;
            /**
             * Адрес пункта доставки
             */
            autoDestAddr?: string | null;
            /**
             * Время работы пункта выдачи
             */
            autoDestWorkTime?: string | null;
            /**
             * Наличие безналичной оплаты
             */
            autoDestCashless?: boolean | null;
            /**
             * Имя сайта, на котором был оформлен заказ
             */
            fromSite?: string | null;
            /**
             * Номер заказа
             */
            orderNum?: string | null;
            /**
             * Дата заказа
             */
            orderDate?: string; // date-time
            /**
             * Дата удаления заказа
             */
            deleted?: string | null; // date-time
            /**
             * Дата подтверждения валидности заказа для отправки на склад
             */
            confirmed?: string | null; // date-time
            /**
             * Дата + время редактирования заказа
             */
            edit?: string | null; // date-time
            /**
             * Прогнозируемая дата и время доставки
             */
            plannedShippedDate?: string | null; // date-time
            /**
             * Прогнозируемая дата и время доставки (окончание)
             */
            plannedEndShippedDate?: string | null; // date-time
            /**
             * Заказ отправлен на склад
             */
            onDepot?: string | null; // date-time
            /**
             * Заказ отправлен на склад (source)
             */
            trackingOnDepot?: string | null; // date-time
            /**
             * По заказу выведены сборочные листы
             */
            asmList?: string | null; // date-time
            /**
             * Прогнозируемое время доставки в пункт выдачи
             */
            deliveryTime?: string | null; // date-time
            /**
             * Прогнозируемое время доставки в пункт выдачи (source)
             */
            trackingDeliveryTime?: string | null; // date-time
            /**
             * Заказ доставлен в пункт выдачи
             */
            orderShipped?: string | null; // date-time
            shipped?: string | null; // date-time
            /**
             * Заказ доставлен в пункт выдачи (source)
             */
            trackingOrderShipped?: string | null; // date-time
            status?: /* Статус заказа */ OrderStatus;
            /**
             * Имя промо-кода
             */
            promoCodeName?: string | null;
            /**
             * Пользователь хочет обратного звонка
             */
            needCall?: boolean;
            /**
             * Требуется уведомление о статусах на email
             */
            needEmail?: boolean;
            /**
             * Дата совершения звонка
             */
            callMaded?: string | null; // date-time
            /**
             * Пользовательское название для заказа
             */
            customName?: string | null;
            /**
             * Список имен промо-кодов
             */
            readonly promoCodes?: string[] | null;
            /**
             * Список товаров
             */
            items?: /* Позиция в заказе */ OrderItem[] | null;
            /**
             * Общая сумма по корзине
             */
            totalSum?: number; // double
            /**
             * Общая выгода по корзине
             */
            totalProfit?: number; // double
            /**
             * Общая сумма по корзине без учета скидок
             */
            totalNoDiscSum?: number; // double
            /**
             * Суммарные скидки
             */
            totalAppliedDiscounts?: AppliedDiscount[];
            /**
             * Витаминок будет начислено при выполнении заказа
             */
            vitaminsToBeCredited?: number; // int32
            /**
             * Дата начисления витаминок
             */
            vitaminsCredited?: string | null; // date-time
            /**
             * Планируемая дата начисления витаминок
             */
            readonly plannedVitaminsCredited?: string | null; // date-time
            /**
             * Витаминок будет списано
             */
            vitaminsUsed?: number; // int32
            /**
             * Промо витаминок будет начислено при выполнении заказа
             */
            extraVits?: number; // int32
            /**
             * Дата выкупа товара
             */
            done?: string | null; // date-time
            /**
             * Получение заказа подтверждено
             */
            receiptConfirmed?: string | null; // date-time
            /**
             * Партнер заказа
             */
            salePartner?: /* Партнер заказа */ OrderSalePartner[] | null;
            /**
             * Порядковый номер заказа у пользователя
             */
            usersIndexNumber?: number; // int64
            /**
             * История изменения заказа для суперпользователя.
             */
            readonly orderHistory?:
                /* набор позиций по заказу в определённый момент времени жизни заказа. */
                OrderState[] | null;
            /**
             * SU - создатель заказа
             */
            superCreatorName?: string | null;
            /**
             * Срок хранения заказа в аптеке
             */
            retentionDate?: string | null;
        }

        /**
         * Примененная скидка
         */
         export interface AppliedDiscount {
            /**
             * Описание
             */
            description: string | null;
            /**
             * Процент скидки
             */
            percent: number;
            /**
             * Сумма скидки
             */
            sum: number;
            /**
             * Тип скидки
             */
            type: string | null;
        }

        /**
         * Промо-код из заказа
         */
        export interface OrderPromoCode {
            /**
             * Uid промо-кода
             */
            uid?: string; // uuid
            /**
             * Имя промо-кода
             */
            name?: string | null;
            siteChannel?: /* Каналы сайта */ SiteChannel;
        }

        /**
         * Партнер заказа
         */
        export type OrderSalePartner = 'MnogoRu' | 'Olekstra';

        /**
         * набор позиций по заказу в определённый момент времени жизни заказа.
         */
        export interface OrderState {
            /**
             * Дата среза состояния заказа
             */
            date?: string; // date-time
            /**
             * Набор позиций в указанное время по заказу
             */
            items?: /* Позиция в заказе */ OrderItem[] | null;
            /**
             * Сумма по заказу
             */
            totalSum?: number; // double
            /**
             * Общая выгода по корзине
             */
            totalProfit?: number; // double
            /**
             * Общая сумма по корзине без учета скидок
             */
            totalNoDiscSum?: number; // double
            /**
             * Источник изменений
             */
            changeSource?: string | null;
            /**
             * Дата последнего изменения состава заказа
             */
            lastCompositionChange?: string | null; // date-time
        }

        /**
         * Статус заказа
         */
        export type OrderStatus =
            'Created'
            | 'CallMaded'
            | 'OnDepot'
            | 'AsmListReady'
            | 'Delivering'
            | 'Shipped'
            | 'Deleted'
            | 'Received'
            | 'HasReturn';

        /**
         * Описание фасовки
         */
        export interface PackDesc {
            /**
             * Название в винительном падеже
             */
            accusative?: string | null;
            /**
             * Не отображать 1 в расчёте на сайте
             */
            hideOne?: boolean;
        }

        /**
         * Информация по упаковке
         */
        export interface PackPriceInfo {
            /**
             * Цена за единицу
             */
            packCalcPrice?: number; // double
            /**
             * Название в винительном падеже
             */
            desc?: string | null;
        }

        /**
         * Модель номера телефона
         */
        export interface Phone {
            /**
             * Номер телефона
             */
            number?: string | null;
            /**
             * Примечание к номеру телефону
             */
            comment?: string | null;
        }

        /**
         * Пути до фотографий
         */
        export interface Photo {
            /**
             * Оригинальный размер
             */
            original?: string | null;
            /**
             * 300 * 300
             */
            medium?: string | null;
            /**
             * 150 * 150
             */
            small?: string | null;
            /**
             * 60 * 60
             */
            preview?: string | null;
            /**
             * конвертированная фотка из svg (128*128)
             */
            png?: string | null;
            /**
             * Оригинальный размер
             */
            originalWebp?: string | null;
            /**
             * 300 * 300
             */
            mediumWebp?: string | null;
            /**
             * 150 * 150
             */
            smallWebp?: string | null;
            /**
             * 60 * 60
             */
            previewWebp?: string | null;
        }

        /**
         * 3D фото упаковки
         */
        export interface PhotoPack {
            /**
             * Массив фотографий
             */
            photos?: /* Пути до фотографий */ Photo[] | null;
        }

        /**
         * Класс, описывающий точку на карте
         */
        export interface Point {
            /**
             * Широта
             */
            latitude?: number | null; // double
            /**
             * Долгота
             */
            longitude?: number | null; // double
        }

        /**
         * Информация о цене
         */
        export interface PriceAndRestInfo {
            /**
             * дата ожидаемого прихода из документа
             */
            incoming?: string | null; // date-time
            /**
             * Минимальная цена
             */
            minPrice?: number; // double
            /**
             * Выгода
             */
            profit?: number; // double
        }

        /**
         * Цена и остаток
         */
        export interface PriceAndRestModel {
            /**
             * Id
             */
            id?: string | null;
            /**
             * Id города
             */
            iPharmTownId?: string | null;
            /**
             * Id филиала
             */
            regBodyId?: string | null;
            /**
             * Id товара
             */
            itemId?: string | null;
            /**
             * Uid товара
             */
            itemUid?: string; // uuid
            /**
             * Рейтинг
             */
            itemRank?: number; // double
            /**
             * Величина дополнительной скидки. Число. Дробное (может быть отрицательным)
             */
            addDisc?: number | null; // double
            /**
             * Дата обновления цены
             */
            priceDate?: string | null; // date-time
            /**
             * Цена без скидок
             */
            noDiscPrice?: number | null; // double
            /**
             * Цена
             */
            dbPrice?: number | null; // double
            /**
             * Есть цена
             */
            readonly hasPrice?: boolean;
            /**
             * Дисконты
             */
            discounts?: /* Цены со скидками */ Discount[] | null;
            /**
             * Промо коды
             */
            promoCodePrices?: /* Цены со скидками */ Discount[] | null;
            /**
             * Выгода
             */
            dbProfit?: number | null; // double
            /**
             * Количество разрешенное к продаже веществ с SaleLimit
             */
            restrictionQuantity?: null | number; // int32
            /**
             * Дата обновления ожидаемого прихода
             */
            incomingDate?: string | null; // date-time
            /**
             * Дата ожидаемого прихода
             */
            incoming?: string | null; // date-time
            /**
             * Дата обновления остатков
             */
            restDate?: string | null; // date-time
            /**
             * Остаток
             */
            rest?: null | number; // int32
            /**
             * Дата последней нормализации
             */
            lastNormalized?: string | null; // date-time
            /**
             * Срок годности
             */
            lifeTime?: string | null; // date-time
            /**
             * Распродано
             */
            outOfStock?: string | null; // date-time
            /**
             * Рассчитанная базовая цена с учётом дисконта
             */
            calcedPrice?: number | null; // double
            /**
             * Рассчитанная базовая цена с учётом дисконта
             */
            calcedProfit?: number | null; // double
        }

        /**
         * Статус отчета
         */
        export type ReportStatus = 'Pending' | 'InProcess' | 'Ready' | 'Error';
        /**
         * Оценка при обратной связи
         */
        export type RequestLabel = 'Positive' | 'Negative';
        /**
         * Типы запроса обратной связи
         */
        export type RequestType = 'Idea' | 'Error' | 'Question';
        /**
         * Тип отзывов
         */
        export type ReviewType = 'RvTpNone' | 'RvTpClient' | 'RvTpAll';

        /**
         * Товар в партнерской программе
         */
        export interface SaleProgramProduct {
            /**
             * Uid товара
             */
            uid?: string; // uuid
            saleType?: /* Тип скидки по партнерской программе */ SaleType;
            /**
             * Размер скидки
             */
            saleValue?: number; // double
        }

        /**
         * Тип скидки по партнерской программе
         */
        export type SaleType = 'FixPrice' | 'FixDiscount' | 'PercentDiscount';

        /**
         * Модель хинта
         */
        export interface SearchHintModel {
            /**
             * Id
             */
            id?: string | null;
            /**
             * Сам хинт
             */
            searchHint: string;
            /**
             * Дата начала
             */
            startDate: string; // date-time
            /**
             * Дата конца
             */
            endDate: string; // date-time
            hintType: /* Тип хинта */ HintType;
        }

        /**
         * Модель данных бренда (короткий вариант)
         */
        export interface ShortBrandModel {
            /**
             * Имя
             */
            name?: string | null;
            /**
             * Url бренда
             */
            url?: string | null;
            /**
             * Пути до фотографий
             */
            readonly photo?: string | null;
        }

        /**
         * Данные по пользователю
         */
        export interface ShortUserInfo {
            /**
             * Id объекта
             */
            id?: string | null;
            /**
             * Номер телефона
             */
            custPhone?: string | null;
            /**
             * Id города
             */
            cityId?: string | null;
            /**
             * Id пункта доставки
             */
            readonly autoDestId?: string | null;
        }

        /**
         * Каналы сайта
         */
        export type SiteChannel = 'Kcs' | 'Vtb' | 'Sberbank' | 'Site' | 'Olekstra';

        /**
         * Описание товаров для GoodSet
         */
        export interface SourceItem {
            /**
             * SourceUid
             */
            uid?: string; // uuid
            /**
             * Цена
             */
            price?: number; // double
            /**
             * Цена без скидок
             */
            noDiscPrice?: number; // double
            /**
             * Величина дополнительной скидки. Число. Дробное (может быть отрицательным)
             */
            addDisc?: number | null; // double
            /**
             * Сроковый товар.
             */
            badLife?: boolean;
            /**
             * Сумма скидки по позиции за 1шт мимо БЕ
             */
            siteDiscSum?: number | null; // double
        }

        /**
         * Подписки пользователя
         */
        export interface Subscriptions {
            /**
             * Промо-рассылки (email)
             */
            promotionEmail?: boolean;
            /**
             * Промо-рассылки (sms)
             */
            promotionSms?: boolean;
            /**
             * Промо-рассылки (телефон)
             */
            promotionPhone?: boolean;
            /**
             * Оставьте отзыв (email)
             */
            reviewRequestEmail?: boolean;
            /**
             * Сервисные сообщения (email)
             */
            serviceMessagesEmail?: boolean;
            /**
             * Удовлетворение работой сервиса (email)
             */
            satisfactionEmail?: boolean;
            /**
             * Удовлетворение работой сервиса (телефон)
             */
            satisfactionPhone?: boolean;
        }

        /**
         * Информация о товаре в группе из одного товара.
         */
        export interface UniqueItemInfo {
            /**
             * Идентификатор товара
             */
            id?: string | null;
            goodNaming?: /* Составное наименование товара */ GoodNaming;
        }

        /**
         * Развернутая группа товаров
         */
        export interface UnwoundItemGroupModel {
            /**
             * Орининальный препарат
             */
            notGeneric?: boolean;
            /**
             * Рецептурный препарат
             */
            prescriptionDrug?: boolean;
            /**
             * Рецепт остаётся в аптеке
             */
            recipeInPh?: boolean;
            /**
             * Фотографии товаров
             */
            photos?: /* Пути до фотографий */ Photo[] | null;
            /**
             * Человекочитаемый Url
             */
            humanableUrl?: string | null;
            /**
             * Id группы
             */
            id?: string | null;
            /**
             * Поступление
             */
            incoming?: string | null; // date-time
            /**
             * Действующие вещества
             */
            interNames?: string[] | null;
            /**
             * Количество товаров в группе
             */
            itemsCount?: number; // int32
            uniqueItemInfo?: /* Информация о товаре в группе из одного товара. */ UniqueItemInfo;
            /**
             * Отображаемое наименование товара с указанием линейки бренда
             */
            tradeName?: string | null;
            /**
             * Наименование для сайта user-friendly
             */
            humanReadableName?: string | null;
            /**
             * Производитель
             */
            vendor?: string | null;
            /**
             * Максимальное количество витаминок
             */
            maxPromoVits?: null | number; // int32
            brand?: /* Бренд в товаре */ GoodVendorBrand;
            line?: /* Линейка бренда в товаре */ GoodVendorLine;
            /**
             * Фотографии товаров
             */
            fileInsts?: string[] | null;
            itemGroupType?: /* Тип */ ItemType;
            /**
             * Минимальная цена
             */
            readonly minPrice?: number; // double
            /**
             * Выгода
             */
            readonly profit?: number; // double
            /**
             * Товары в корзине пользователя
             */
            itemsInCart?: /* Позиция в корзине пользователя */ Shared.ItemInCart[] | null;
            /**
             * Участвует в благотворительной программе
             */
            fund?: boolean;
            /**
             * В группе есть товар с прогрессирующей скидкой
             */
            hasProgressiveDiscount: boolean;
            /**
             * Варианты группировки
             */
            itemVariantsInfo?: ItemVariantsInfo[];
            /**
             * Отладочная инфомрация по группе товаров
             */
            debugInfo?: DebugInfo;
        }

        /**
         * Варианты группировки
         */
        export interface ItemVariantsInfo {
            /**
             * Id товара
             */
            id: string;
            /**
             * Наименование варианта группировки
             */
            name: string;
        }

        /**
         * Тип уровня варианта
         */
        export type VariantType = 'Packing' | 'Dosage' | 'FormRelease' | 'Custom' | 'Variant' | 'PrimaryPacking';
        /**
         * Типы событий, связанных с движением витаминок
         */
        export type VitaminsEventType = 'Other' | 'Order' | 'ReferrlProgram' | 'SuperUser' | 'Charity' | 'Wipe';

        /**
         * Запись в логе движения витаминок
         */
        export interface VitaminsLogModel {
            /**
             * Время события
             */
            eventTime?: string; // date-time
            /**
             * Коментарий к событию
             */
            description?: string | null;
            /**
             * Кол-во витаминок
             */
            value?: number; // int32
            eventType?: /* Типы событий, связанных с движением витаминок */ VitaminsEventType;
            /**
             * Id пользователя, за заказ которого начислены витаминки
             */
            invitedUserId?: string | null;
            /**
             * Супер-юзер внёсший изменения
             */
            superUserName?: string | null;
        }

        /**
         * Модель статического текста
         */
        export interface StaticTextModel {
            /**
             * Идентификатор
             */
            id: string | null;
            /**
             * Название
             */
            name: string | null;
            /**
             * Текст
             */
            text: string | null;
            /**
             * Урл
             */
            url: string | null;
        }
    }
    namespace Olekstra {
        /**
         * Информация о пункте выдачи
         */
        export interface DeliveryPoint {
            /**
             * Guid пункта доставки
             */
            uuid?: string; // uuid
            /**
             * Наименование пункта доставки
             */
            name?: string | null;
            /**
             * Адрес пункта доставки
             */
            address?: string | null;
            /**
             * Долгота (значение)
             */
            longitude?: number; // double
            /**
             * Широта (значение)
             */
            latitude?: number; // double
            /**
             * Примечание
             */
            note?: string | null;
            /**
             * ИНН
             */
            inn?: string | null;
        }

        /**
         * Актуальная цена на товар
         */
        export interface GoodVendorPrice {
            /**
             * Название
             */
            readonly name?: string | null;
            /**
             * Базовая цена
             */
            readonly price?: number; // double
            /**
             * Цена по программе
             */
            readonly specialPrice?: number; // double
        }

        /**
         * Город с актуальными ценами на товары
         */
        export interface IPharmTownInfo {
            /**
             * Наименование города
             */
            readonly name?: string | null;
            /**
             * Товары с ценами
             */
            readonly prices?: {
                [name: string]: /* Актуальная цена на товар */ GoodVendorPrice;
            } | null;
        }

        /**
         * Статус заказа в Олекстра
         */
        export type OlekstraOrderStatus = 'N' | 'S' | 'K' | 'B' | 'D' | 'A' | 'R';

        /**
         * Ошибка при вызове метода
         */
        export interface OlekstraResponseError {
            /**
             * Код ошибки
             */
            readonly code?: string | null;
            /**
             * Сообщение
             */
            readonly message?: string | null;
        }

        /**
         * Статус
         */
        export type OlekstraResponseStatus = 'OK' | 'FAIL';

        /**
         * Информация о заказе для Олекстра
         */
        export interface OrderData {
            /**
             * Номер заказа
             */
            readonly number?: string | null;
            status?: /* Статус заказа в Олекстра */ OlekstraOrderStatus;
            /**
             * Дата создания заказа
             */
            readonly created_at?: string; // date-time
            price?: /* Итоги по заказу */ OrderPrice;
            user?: /* Пользователь, совершивший заказ */ OrderUser;
            /**
             * Позиции в заказе
             */
            readonly positions?: {
                [name: string]: /* Позиция в заказе */ OrderPosition;
            } | null;
            delivery_point?: /* Информация о пункте выдачи */ DeliveryPoint;
        }

        /**
         * Полезная нагрузка
         */
        export interface OrderInfo {
            /**
             * Номер заказа
             */
            readonly number?: string | null;
            status?: /* Статус заказа в Олекстра */ OlekstraOrderStatus;
            /**
             * Дата создания заказа
             */
            readonly created_at?: string; // date-time
            price?: /* Итоги по заказу */ OrderPrice;
        }

        /**
         * Позиция в заказе
         */
        export interface OrderPosition {
            /**
             * Название
             */
            name?: string | null;
            /**
             * Кол-во в заказе
             */
            quantity?: number; // int32
            price?: /* Цена на товар */ OrderPositionPrice;
            /**
             * Промо-код Олекстра
             */
            promo_code?: string | null;
        }

        /**
         * Цена на товар
         */
        export interface OrderPositionPrice {
            /**
             * Базовая цена
             */
            initial?: number; // double
            /**
             * Базовая сумма
             */
            initialTotal?: number; // double
            /**
             * Скидка по промо коду Олекстра
             */
            discount?: number; // double
            /**
             * Другие скидки Аптека.ру
             */
            additionalDiscount?: number; // double
            /**
             * Итог
             */
            result?: number; // double
        }

        /**
         * Итоги по заказу
         */
        export interface OrderPrice {
            /**
             * Общая скидка по промокодам Олекстра
             */
            discount?: number; // double
            /**
             * Итоговая цена за заказ
             */
            result?: number; // double
        }

        /**
         * Ответ на запрос информации о заказе
         */
        export interface OrderResponse {
            data?: /* Информация о заказе для Олекстра */ OrderData;
            status?: /* Статус */ OlekstraResponseStatus;
            /**
             * Ошибки
             */
            readonly errors?: /* Ошибка при вызове метода */ OlekstraResponseError[] | null;
        }

        /**
         * Пользователь, совершивший заказ
         */
        export interface OrderUser {
            /**
             * Номер телефона
             */
            phone?: string | null;
        }

        /**
         * Ответ на запрос информации о заказах
         */
        export interface OrdersResponse {
            /**
             * Полезная нагрузка
             */
            readonly data?: /* Полезная нагрузка */ OrderInfo[] | null;
            status?: /* Статус */ OlekstraResponseStatus;
            /**
             * Ошибки
             */
            readonly errors?: /* Ошибка при вызове метода */ OlekstraResponseError[] | null;
        }

        /**
         * Ответ на запрос цен на товары по партнерской программе
         */
        export interface PricesResponse {
            /**
             * Полезная нагрузка
             */
            readonly data?: {
                [name: string]: /* Город с актуальными ценами на товары */ IPharmTownInfo;
            } | null;
            status?: /* Статус */ OlekstraResponseStatus;
            /**
             * Ошибки
             */
            readonly errors?: /* Ошибка при вызове метода */ OlekstraResponseError[] | null;
        }

        /**
         * Запрос на регистрацию номера телефона пользователя в Олекстра
         */
        export interface UserPhoneRequest {
            /**
             * Список товаров
             */
            limits?: {
                [name: string]: number; // int32
            } | null;
        }

        /**
         * Ответ на запрос регистрации пользователя в Олекстра
         */
        export interface UserPhoneResponse {
            data?: /* Полезная нагрузка */ UsersPhoneData;
            status?: /* Статус */ OlekstraResponseStatus;
            /**
             * Ошибки
             */
            readonly errors?: /* Ошибка при вызове метода */ OlekstraResponseError[] | null;
        }

        /**
         * Полезная нагрузка
         */
        export interface UserTokenData {
            /**
             * Валидный номер телефона
             */
            readonly token?: string | null;
            /**
             * Срок годности
             */
            readonly expired_at?: string; // date-time
        }

        /**
         * Ответ на запрос авторизационного токена
         */
        export interface UserTokenResponse {
            data?: /* Полезная нагрузка */ UserTokenData;
            status?: /* Статус */ OlekstraResponseStatus;
            /**
             * Ошибки
             */
            readonly errors?: /* Ошибка при вызове метода */ OlekstraResponseError[] | null;
        }

        /**
         * Полезная нагрузка
         */
        export interface UsersPhoneData {
            /**
             * Новый пользователь
             */
            readonly is_new?: boolean;
        }
    }
    namespace Order {
        /**
         * Справочная информация о заказе
         */
        export interface BaseOrderInfo {
            /**
             * Номер заказа
             */
            orderNum: string;
            /**
             * Дата заказа
             */
            orderDate: string;
            /**
             * Дата заказа
             */
            itemsCount: number;
            /**
             * Сумма заказов
             */
            totalSum: number;
        }
        /**
         * Параметры запроса о подтверждении получения заказа
         */
        export interface PostReceiptConfirmRequest {
            /**
             * Id заказа
             */
            orderId?: string | null;
            /**
             * (SuperUser only) Id пользователя, в контексте которого ведется работа
             */
            userId?: string | null;
            /**
             * Код подтверждения получения заказа
             */
            confirmCode?: string | null;
        }

        /**
         * Параметры запроса на переименование заказа
         */
        export interface PutCustomNameRequest {
            /**
             * Id заказа
             */
            orderId?: string | null;
            /**
             * Требуемое наименование
             */
            customName?: string | null;
        }

        /**
         * Параметры запроса на повторение заказа
         */
        export interface PutOrderRepeatRequest {
            /**
             * Id заказа
             */
            orderId?: string | null;
            /**
             * Id пользователя
             */
            userId?: string | null;
        }

        /**
         * Параметры запроса на оформление заказа
         */
        export interface PutOrderRequest {
            /**
             * Email пользователя для уведомлений
             */
            email?: string | null;
            /**
             * Необходимо выслать email подтверждение на мыло
             */
            needEmail?: boolean;
            /**
             * Пользователь хочет обратного звонка
             */
            needCall?: boolean;
            /**
             * Пользовательское название для заказа
             */
            customName?: string | null;
            /**
             * Ид карты много ру
             */
            mnogoRuCardId?: string | null;
            /**
             * (SuperUser only) Id пользователя, в контексте которого ведется работа
             */
            userId?: string | null;
            device?: /* Тип устройства */ Utils.DeviceType;
        }

        /**
         * Список заказов
         */
        export interface PutOrderResponse {
            /**
             * Id заказа
             */
            readonly orderId?: string | null;
            /**
             * Номер заказа
             */
            readonly orderNum?: string | null;
            order?: /* Модель заказа */ Mongo.OrderModel;
        }

        /**
         * Информация о заказе в урезанном виде
         */
        export interface ShortOrderInfo {
            /**
             * Id заказа
             */
            readonly orderId?: string | null;
            /**
             * Номер заказа
             */
            readonly orderNum?: string | null;
            /**
             * Дата заказа
             */
            readonly orderDate?: string; // date-time
            /**
             * Пользовательское название для заказа
             */
            readonly customName?: string | null;
            status?: /* Статус заказа */ Mongo.OrderStatus;
        }

        /**
         * Заказы за которые будут начислены витаминки
         */
        export interface WaitingVitamins {
            /**
             * Номер заказа
             */
            readonly orderNum?: string | null;
            /**
             * Количество витаминок
             */
            readonly vitaminsToBeCredited?: number | null;
            /**
             * Дата начисления
             */
            readonly plannedVitaminsCredited?: string; // date-time
        }

        /**
         * Тип запроса заказов
         */
        export type OrderSearchType = 'All' | 'Active' | 'Old';
        /**
         * Типы сортировки заказов
         */
        export type OrderSortType = 'DateDesc' | 'DateAsc';
    }
    namespace Preparation {
        /**
         * Данные о товаре для SEO
         */
        export interface PreparationInfo {
            iPharmIns?: /* Инструкция */ Mongo.IPharmInsModel;
            searchResult?: /* Результат поиска */ Shared.ItemGroupSearchResult;
            /**
             * Пункты выдачи в городе
             */
            autoDests?: /* Модель пункта доставки */ Mongo.AutoDestModel[] | null;
            townInfo?: /* Информация о городе для SEO */ TownInfo;
        }

        /**
         * Ссылка на инструкцию
         */
        export interface PreparationLink {
            /**
             * Ссылка
             */
            readonly url?: string | null;
            /**
             * Человекочитаемое наименование
             */
            readonly linkName?: string | null;
        }

        /**
         * Информация о городе для SEO
         */
        export interface TownInfo {
            /**
             * Наименование
             */
            name?: string | null;
            /**
             * Имя в предложном падеже
             */
            prepositionalName?: string | null;
            areaRectangle?: /* Область на карте */ Mongo.AreaRectangle;
            /**
             * Кол-во пунктов выдачи в городе
             */
             autoDestCount?: number; // int32
            /**
             * Урл города
             */
            url?: string;
        }
    }
    namespace Reports {
        /**
         * Данные отчета
         */
        export interface ReportInfo {
            reportStatus?: /* Статус отчета */ Mongo.ReportStatus;
            /**
             * Отчет
             */
            readonly url?: string | null;
        }

        /**
         * Общая сумма витаминок
         */
        export interface TotalVitBalance {
            /**
             * Сумма
             */
            sum?: number; // int64
            /**
             * Дата формирования отчета
             */
            date?: string; // date-time
            /**
             * Статус
             */
            status?: string | null;
        }
    }
    namespace Search {
        /**
         * Тип каталога для поиска.
         */
        export type CatalogType = 'Category' | 'GoodGroup';
    }
    namespace Settings {
        /**
         * Модель настроек витаминок
         */
        export interface VitaminsModel {
            /**
             * Сумма заказа на витаминку
             */
            readonly sumForVit?: number; // int32
            /**
             * Кол-во дней, после которых происходит начисление витаминок
             */
            readonly daysBeforeAccrual?: number; // int32
            /**
             * Кол-во витаминок для получения 1% скидки
             */
            readonly vitaminsPerPercent?: number; // int32
            /**
             * Месяцев с последнего заказа до сброса витаминок
             */
            readonly downtimeInMonthToWipe?: number; // int32
            /**
             * Вознаграждение за приглашение в реферальной программе
             */
            readonly referralProgramParentReward?: number; // int32
            /**
             * Вознаграждение за регистрацию по реферальной программе
             */
            readonly referralProgramJoiningReward?: number; // int32
            /**
             * Продолжительность действия вознаграждения по реферальной программе
             */
            readonly referralInviteValidInDays?: number; // int32
            /**
             * Минимальная доступная скидка в %
             */
            readonly minDiscount?: number; // int32
            /**
             * Максимальная доступная скидка в %
             */
            readonly maxDiscount?: number; // int32
        }
    }
    namespace Shared {
        /**
         * Стандартный класс для описания ошибки
         */
        export interface ActionError {
            type?: /* Типы ошибок в UerrService */ User.ErrorTypesReferralError;
            /**
             * Сообщение
             */
            readonly message?: string | null;
        }

        /**
         * Стандартизированная обертка над результатом выполнения метода
         */
        export interface ActionResult {
            error?: /* Стандартный класс для описания ошибки */ ActionError;
        }

        /**
         * Данные по каталогу
         */
        export interface GroupInfo {
            /**
             * Url раздела
             */
            url?: string | null;
            /**
             * Имя раздела
             */
            name?: string | null;
            /**
             * SEO имя раздела
             */
            seoName: string | null;
            /**
             * Подкатегория
             */
            subGroup?: GroupInfo;
        }

        /**
         * Урл и имя значения атрибута
         */
        export interface ItemGroupMultiChoice {
            /**
             * Урл
             */
            url?: string | null;
            /**
             * Имя
             */
            name?: string | null;
            /**
             * Человеческие атрибуты
             */
            humanName?: string | null;
            /**
             * Used
             */
            used?: boolean;
            /**
             * Есть товары
             */
            hasGoods?: boolean;
        }

        /**
         * Урл и имя значения атрибута
         */
        export interface ItemGroupNumChoice {
            /**
             * Min
             */
            min?: number; // double
            /**
             * Max
             */
            max?: number; // double
            /**
             * UseMin
             */
            usedMin?: number; // double
            /**
             * UseMax
             */
            usedMax?: number; // double
        }

        /**
         * Результат поиска
         */
        export interface ItemGroupSearchResult {
            /**
             * Время выполнения запроса
             */
            took?: number; // int64
            /**
             * Es took ms
             */
            esTook?: number; // int64
            /**
             * Страница
             */
            page?: number; // int32
            /**
             * Чисто карточек на странице
             */
            pageSize?: number; // int32
            /**
             * Число найденных карточек для выбранной страницы
             */
            currentCount?: number; // int32
            /**
             * Число найденных карточек вообще
             */
            totalCount?: number; // int64
            /**
             * Результат группировки поиска
             */
            result?: /* Развернутая группа товаров */ Mongo.UnwoundItemGroupModel[] | null;
            /**
             * Количество доступных товаров по атрибутам категории
             */
            attributes?: /* Количество атрибутов */ OutputItemGroupAttributeObject[] | null;
            groupInfo?: /* Данные по каталогу */ GroupInfo;
            /**
             * Категории в результатах поиска
             */
            categories?: /* Категория с подкатегорией */ Catalog.UnwoundCategory[] | null;
            /**
             * Поисковые тэги.
             */
            searchTags?: string[] | null;
            /**
             * Возможно вы имели ввиду ***
             */
            suggestion?: string | null;
            /**
             * Минимальная цена в группе
             */
            minGroupPrice?: number; // double
            /**
             * в списке избранного
             */
            inFavoriteList?: string[];
        }

        /**
         * Позиция в корзине пользователя
         */
        export interface ItemInCart {
            /**
             * Id товара
             */
            readonly itemId?: string | null;
            /**
             * Имя товара
             */
            readonly itemName?: string | null;
            goodNaming?: /* Составное наименование товара */ Mongo.GoodNaming;
            /**
             * Количество
             */
            readonly amount?: number; // int32
            /**
             * Цена с применением всех скидок
             */
            readonly price?: number; // double
            fileInst?: /* Фото объекта */ Mongo.FileInst;
            /**
             * Товар отложен
             */
            readonly deferred?: boolean;
            /**
             * Уведомить о появлении
             */
            readonly notifyAppearance?: boolean;
            /**
             * Кол-во для ограничения продажи особых товаров
             */
            readonly restrictionQuantity?: number; // int32
        }

        /**
         * Количество атрибутов
         */
        export interface OutputItemGroupAttributeObject {
            /**
             * Наименование
             */
            readonly name?: string | null;
            /**
             * Url атрибута
             */
            readonly attributeUrl?: string | null;
            type?: /* Тип атрибута категории */ Mongo.FilterAttributeType;
            /**
             * Значения
             */
            multiValues?: /* Урл и имя значения атрибута */ ItemGroupMultiChoice[] | null;
            numValues?: /* Урл и имя значения атрибута */ ItemGroupNumChoice;
        }

        /**
         * Страница с данными для пагинации
         */
        export interface ResponsePage<T> {
            /**
             * Номер текущей страницы
             */
            currentPage?: number; // int32
            /**
             * Кол-во на текущей странице
             */
            readonly count?: number; // int32
            /**
             * Кол-во страниц
             */
            totalPages?: number; // int32
            /**
             * Общее кол-во
             */
            totalCount?: number; // int64
            /**
             * Данные
             */
            data?: /* Акция */ T[] | null;
        }

        /**
         * Типы запроса отзывов
         */
        export type ReviewsPageType = 'All' | 'Positive' | 'Negative';
        /**
         * Варианты сортировки результатов поиска
         */
        export type SearchSortType =
            'Default'
            | 'DefaultAsc'
            | 'ByName'
            | 'ByNameDesc'
            | 'ByPrice'
            | 'ByPriceDesc'
            | 'None';

        /**
         * Результат работы метода сервиса
         */
        export interface ServiceOperationResult {
            code?: Net.HttpStatusCode;
            /**
             * Сообщение, в случае ошибки
             */
            readonly message?: string | null;
        }
    }
    namespace User {
        /**
         * Типы ошибок в UerrService
         */
        export type ErrorTypesReferralError = 'IncorrectParams' | 'UserHasNotOrders' | 'RequestedPhoneError';

        /**
         * Параметры запроса на добавление номера телефона в программу "Приведи друга"
         */
        export interface AddReferredNumberRequest {
            /**
             * Номер телефона
             */
            phoneNumber?: string | null;
        }

        /**
         * Класс входных параметров для метода /AuthController/SetPassword
         */
        export interface ChangePasswordRequest {
            /**
             * Старый пароль
             */
            oldPassword?: string | null;
            /**
             * Новый пароль
             */
            newPassword?: string | null;
            /**
             * Новый пароль (проверка)
             */
            newPasswordRepeat?: string | null;
        }

        /**
         * Подписки пользователя
         */
        export interface ChangeSubscriptionsRequest {
            /**
             * Промо-рассылки (email)
             */
            promotionEmail?: boolean;
            /**
             * Промо-рассылки (sms)
             */
            promotionSms?: boolean;
            /**
             * Оставьте отзыв (email)
             */
            reviewRequestEmail?: boolean;
            /**
             * Оставьте отзыв (sms)
             */
            reviewRequestSms?: boolean;
            /**
             * Сервисные сообщения (email)
             */
            serviceMessagesEmail?: boolean;
            /**
             * Сервисные сообщения (sms)
             */
            serviceMessagesSms?: boolean;
        }

        /**
         * Отладочная информация о пользователе
         */
        export interface UserDebugInfo {
            /**
             * Дата получения бана
             */
            readonly banned?: string | null; // date-time
        }

        /**
         * Запрос на обновление адреса электронной почты
         */
        export interface UserEmailRequest {
            /**
             * Адрес электронной почты
             */
            email?: string | null;
        }

        /**
         * Ответ на запрос информации по пользователю
         */
        export interface UserFullInfo {
            /**
             * ФИО
             */
            readonly fio?: string | null;
            /**
             * Дата рождения
             */
            readonly birthDate?: string | null; // date-time
            gender?: /* Пол */ Mongo.Gender;
            /**
             * Адрес электронной почты
             */
            readonly email?: string | null;
            /**
             * Адрес электронной почты подтвержден
             */
            readonly emailVerified?: boolean;
            /**
             * Кол-во товаров в избранном
             */
            readonly favoritesCount?: number; // int64
            /**
             * Кол-во рецептов пользователя
             */
            readonly prescriptionsCount?: number; // int64
            /**
             * Кол-во непрочитанных сообщений
             */
            readonly unreadMessagesCount?: number;
            /**
             * Карта много.ру
             */
            readonly mnogoRuCardId?: string | null;
            /**
             * У пользоателя есть заказы
             */
            readonly hasOrders?: boolean;
            debugInfo?: /* Отладочная информация о пользователе */ UserDebugInfo;
            userDiscounts?: /* Скидки, доступные пользователю */ UserDiscounts;
        }

        /**
         * Скидки, доступные пользователю
         */
        export interface UserDiscounts {
            /**
            * Персональная скидка
            */
            personalDiscount: string | null;
            /**
             * Персональная скидка
             */
            vitDiscount: number | null;
            prDiscounts?: /* Активная прогрессивная скидка */ UserPrDiscount[] | null;
        }

        /**
         * Активная прогрессивная скидка
         */
        export interface UserPrDiscount {
            /**
             * Наименование скидки
             */
            prDiscountName: string | null;
            /**
             * Id товара
             */
            goodVendorId: string | null;
            /**
             * Наименование товара
             */
            goodVendorName: string | null;
            goodVendorPhotos?: /* Пути до фотографий */ Mongo.Photo[] | null;
        }

        /**
         * Запрос на обновление персональной информации
         */
        export interface UserInfoRequest {
            /**
             * ФИО
             */
            fio?: string | null;
            /**
             * Дата рождения
             */
            birthDate?: string | null;
            gender?: /* Пол */ Mongo.Gender;
        }

        /**
         * Данные о выбранных параметрах пользователя (город / аптека)
         */
        export interface UserPreferences {
            selectedCity?: /* Модель города */ Mongo.CityModel;
            selectedAutoDest?: /* Модель пункта доставки */ Mongo.AutoDestModel;
        }

        /**
         * Детали баланса пользователя
         */
        export interface VitBalanceDetails {
            /**
             * Баланс пользователя
             */
            readonly vitBalance?: number; // int32
            /**
             * Последняя дата начисления витаминок
             */
            readonly lastVitCredited?: string | null; // date-time
            /**
             * Менее двух недель до списания
             */
            readonly twoWeeksBeforeVitWipe?: boolean;
            /**
             * Менее недели до списания
             */
            readonly oneWeekBeforeVitWipe?: boolean;
        }
    }
    namespace Utils {
        /**
         * Тип устройства
         */
        export type DeviceType =
            'MobileSite'
            | 'DesktopSite'
            | 'AndroidPhone'
            | 'AndroidTablet'
            | 'IosPhone'
            | 'IosPad'
            | 'Bot'
            | 'Unknown';
    }
    namespace PrDiscount {
        /**
         * Детальная информация о прогрессирующих скидках
         */
        export interface PrDiscDetails {
            /**
             * Id объекта
             */
            readonly id: string | null;
            /**
             * Имя скидки
             */
            readonly name: string | null;
            /**
             * Наименование товара
             */
            readonly goodVendorName: string | null;
            readonly price: number | null;
            readonly noDiscPrice: number | null;
            readonly profit: number | null;
            /**
             * Дата начала действия скидки
             */
            readonly startDate: string; // $date-time
            /**
             * Дата окончания действия скидки
             */
            readonly endDate: string; // $date-time
            /**
             * Этапы прогрессирующей скидки
             */
            readonly stages: PrDiscStage[] | null;
            /**
             * Номер текущего этапа
             */
            readonly currentStage: number | null;
            /**
             * Кол-во купленного товара у текущего пользователя
             */
            readonly redeemedAmount: number;
            /**
             * Кол-во выкупленного товара у текущего пользователя
             */
            readonly confirmedAmount: number;
        }
        /**
         * Условие скидки
         */
        export interface PrDiscStage {
            /**
             * Количество по условию
             */
            amount: number;
            /**
             * Процент скидки
             */
            readonly discPrc: number;
            /**
             * Сумма скидки
             */
            readonly discSum: number;
        }
    }
}
