enum WindowWidthTypes {
    Mobile = 'Mobile', // width <= 800
    HalfMobile = 'HalfMobile', // 800 < width <= 1024
    Tablet = 'Tablet', // 1024 < width <= 1200
    Desktop = 'Desktop', // width > 1200
}

export {
    WindowWidthTypes,
};
