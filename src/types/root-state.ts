import {RouteLocation} from 'vue-router';
import {GoogleState} from '@/store/google';
import {YandexState} from '@/store/yandex';
interface RootState {
    route?:RouteLocation; // добавляется vuex-router-sync`ом
    google?: GoogleState;
    yandex?: YandexState;
}

export {RootState};
