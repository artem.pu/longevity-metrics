export interface WebSiteProfile {
    key: string;
    label: string;
    googleId?: string|false;
    yandexId?: string|false;
    href?: string;
    available: number; // доступеные юзеру метрики
}

export enum MetricLabels {
    'ga:1dayUsers' = 'Users',
    'ga:sessions' = 'Sessions',
    'ga:pageviews' = 'Views',
    'ym:s:users' = 'Users',
    'ym:s:visits' = 'Sessions',
    'ym:s:pageviews' = 'Views',
}
