import {RouteRecordRaw} from 'vue-router';
import NotFound from '@/views/404.vue';

export default <RouteRecordRaw[]> [
    {
        meta: {title: 'Metrics aggregate'},
        name: 'calculis',
        path: '/',
        alias: '/calculis/',
        component: () => import(
            /* webpackChunkName: "calculis" */
            /* webpackMode: "lazy" */
            '@/views/calculis/index.vue'
        ),
    },
    {
        name: 'not-found',
        path: '/404/',
        alias: '/404',
        component: NotFound,
    },
    {
        path: '/:catchAll(.*)*', // Unrecognized path automatically matches 404
        component: NotFound,
    },
];
