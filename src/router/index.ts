import {
    createRouter,
    createWebHistory,
    createMemoryHistory,
    NavigationGuardNext,
    RouteRecordRaw,
    RouteLocation,
    RouterOptions,
} from 'vue-router';

import routes from './routes';

const scrollBehavior = function routerScrollBehavior(to:RouteLocation, from:RouteLocation):void|{x:number, y:number} {
    let paddingY: number = 75; // высота SiteHeaderPanel

    const paramScrollTo: any = to.params.scrollTo;
    if (paramScrollTo === '') return;
    if (paramScrollTo) {
        const scrollTo = parseInt(paramScrollTo, 10);
        if (!isNaN(scrollTo)) paddingY = scrollTo;
    }

    const position = {x: 0, y: 0};
    if (to.name === 'preparation-analogs') return {x: 0, y: 100};
    if (to.name === 'product' && from.name === 'product') return;
    if (typeof (window) === 'undefined') return position;
    if (window.pageYOffset <= paddingY) return;

    // currentPosition
    window.scrollTo({top: paddingY, behavior: 'smooth'});
    position.x = window.pageXOffset;
    position.y = paddingY;

    return position;
};

const createAppRouter = function (store?:any) {
    const ssr = process.env.APP_TARGET === 'server';
    const base = process.env.PUBLIC_PATH || '/';
    const history = (ssr ? createMemoryHistory : createWebHistory)(base);
    const router = createRouter(<RouterOptions>{
        routes,
        history,
        scrollBehavior,
        sensitive: false,
        strict: false,
    });

    return router;
};

export {createAppRouter};
