import AppVue from './App.vue';
import AppHead from './AppHead.vue';
import {sync} from 'vuex-router-sync';

import {createStore} from '@/store';
import {createAppRouter} from '@/router';
// import {setupInterceptors} from '@/client';
import {createSSRApp, createApp} from 'vue';

import filterDateformat from '@/utils/filter.dateformat';
import filterAmount from '@/utils/filter.amount';
/*
import filterPluralize from '@/utils/filter.pluralize';
import filterCapitalize from '@/utils/filter.capitalize';
import filterPhone from '@/utils/filter.phone';
import filterPrice from '@/utils/filter.price';
import filterRub from '@/utils/filter.rub';

import scrollDirective from '@/utils/scroll.directive';
import clickOutsideDirective from '@/utils/click.directive';

import iconComponent from '@/components/svg-icon.vue';
import componentMoney from '@/components/money.vue';
import appRouterLink from '@/components/app-router-link.vue';
import SquareButton from '@/components/buttons/SquareButton.vue';
*/
import loadingSpinner from '@/components/spinner.vue';

export function createVueApp(ssr:boolean = false) {

    const vue = (ssr ? createSSRApp : createApp)(AppVue);
    const store = createStore();
    const router = createAppRouter(store);

    // sync(store, router, {moduleName: 'route'});
    vue.use(router).use(store);
    // setupInterceptors(store);

    // vue.component('AppHead', AppHead);
    // vue.component('money', componentMoney);
    // vue.component('icon-svg', iconComponent);
    // vue.component('app-link', appRouterLink);
    // eslint-disable-next-line vue/multi-word-component-names
    vue.component('spinner', loadingSpinner);
    // vue.component('qbutton', SquareButton);

    // vue.directive('scroll', scrollDirective);
    // vue.directive('click', clickOutsideDirective);

    vue.config.errorHandler = function (err:any, vm, info) {
        console.error(`Error: ${err.toString()}\nInfo: ${info}\n`, vm);
    };

    vue.config.globalProperties.$f = {
        // rub: filterRub,
        // price: filterPrice,
        // phone: filterPhone,
        // pluralize: filterPluralize,
        dateformat: filterDateformat,
        amount: filterAmount,
        // capitalize: filterCapitalize,
    };

    return {router, store, app: vue};
}