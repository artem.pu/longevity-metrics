import {useSSRContext, defineAsyncComponent} from 'vue';
import {Vue, Options as Component} from 'vue-class-component';
import Sidebar from '@/components/sidebar/sidebar.vue';
import SitePanel from '@/components/SitePanel/SitePanel.vue';
import SiteHeader from '@/components/SiteHeader/SiteHeader.vue';
import SiteFooter from '@/components/SiteFooter/SiteFooter.vue';
import ScrollBackButton from '@/components/SiteFooter/ScrollBackButton.vue';
import CookieNotify from '@/components/CookieNotify/CookieNotify.vue';
import {ResizeObserver, ResizeObserverEntry, ResizeObserverCallback} from '@/types/resizeObserver';
import {CrawlerLabel} from '@/utils/crawler';
import 'simplebar';
import 'simplebar/dist/simplebar.min.css';
import {SSRContext} from '@/types/server';
import {reloadOnNewBuild} from '@/utils/reload-on-new-build';
import {client} from '@/client';
import cookie from '@/utils/cookie';
import CityModel = Models.Mongo.CityModel;
import { WindowWidthTypes } from '@/types/window';
import { AuthState } from '@/types/auth';

// eslint-disable-next-line camelcase
type WithOptimize = {google_optimize?: {get(id: string): string}};

@Component<App>({
    components: {
        SiteHeader,
        SiteFooter,
        SitePanel,
        Sidebar,
        ScrollBackButton,
        CookieNotify,
        Authorizer: defineAsyncComponent(() => import(
            /* webpackChunkName: "authorizer" */
            /* webpackMode: "lazy" */
            '@/components/Authorizer/Authorizer.vue'
        )),
        Autodestor: defineAsyncComponent(() => import(
            /* webpackChunkName: "autodestor" */
            /* webpackMode: "lazy" */
            '@/components/Autodestor/Autodestor.vue'
        )),
        Favouriter: defineAsyncComponent(() => import(
            /* webpackChunkName: "favouriter" */
            /* webpackMode: "lazy" */
            '@/components/Favouriter/Favouriter.vue'
        )),
        ModalVitamin: defineAsyncComponent(() => import(
            /* webpackChunkName: "modal-vitamin" */
            /* webpackMode: "lazy" */
            '@/components/ModalVitamin/ModalVitamin.vue'
        )),
        CitychangeNotify: defineAsyncComponent(() => import(
            /* webpackChunkName: "citychange-notify" */
            /* webpackMode: "lazy" */
            '@/components/CitychangeNotify/CitychangeNotify.vue'
        )),
        TownSelector: defineAsyncComponent(() => import(
            /* webpackChunkName: "town-selector" */
            /* webpackMode: "lazy" */
            '@/components/TownSelector/index.vue'
        )),
        ConfirmEmail: defineAsyncComponent(() => import(
            /* webpackChunkName: "confirm-email" */
            /* webpackMode: "lazy" */
            '@/components/ConfirmEmail/ConfirmEmail.vue'
        )),
        ProgressiveDiscountPopup: defineAsyncComponent(() => import(
            /* webpackChunkName: "progressive-discount-popup" */
            /* webpackMode: "lazy" */
            '@/components/ProgressiveDiscount/ProgressiveDiscountPopup.vue'
        )),
        PrescriptionsPopup: defineAsyncComponent(() => import(
            /* webpackChunkName: "prescriptions-popup" */
            /* webpackMode: "lazy" */
            '@/views/cart/PrescriptionsPopup.vue'
        )),
        ConfirmEmailNotifyPopup: defineAsyncComponent(() => import(
            /* webpackChunkName: "confirm-email-notify-popup" */
            /* webpackMode: "lazy" */
            '@/components/ConfirmEmailNotifyPopup/ConfirmEmailNotifyPopup.vue'
        )),
        Unsubscribe: defineAsyncComponent(() => import(
            /* webpackChunkName: "confirm-email" */
            /* webpackMode: "lazy" */
            '@/components/Unsubscribe/Unsubscribe.vue'
        )),
        SuperuserPanelOrder: defineAsyncComponent(() => import(
            /* webpackChunkName: "superuser-panel-order" */
            /* webpackMode: "lazy" */
            '@/views/superuser/panel-order.vue'
        )),
        SuperuserPanelAlias: defineAsyncComponent(() => import(
            /* webpackChunkName: "superuser-panel-alias" */
            /* webpackMode: "lazy" */
            '@/views/superuser/panel-alias.vue'
        )),
        Survey: defineAsyncComponent(() => import(
            /* webpackChunkName: "survey" */
            /* webpackMode: "lazy" */
            '@/components/Survey/Survey.vue'
        )),
        OrderReviewNotify: defineAsyncComponent(() => import(
            /* webpackChunkName: "order-review" */
            /* webpackMode: "lazy" */
            '@/components/OrderReview/OrderReviewNotify.vue'
        )),
        NotFound: defineAsyncComponent(() => import(
            /* webpackChunkName: "not-found-404" */
            /* webpackMode: "lazy" */
            '@/views/static/NotFound.vue'
        )),
        Questionnaire: defineAsyncComponent(() => import(
            /* webpackChunkName: "questionnaire" */
            /* webpackMode: "lazy" */
            '@/components/questionnaire/Questionnaire.vue'
        )),
        FirstOrderDiscountPopup: defineAsyncComponent(() => import(
            /* webpackChunkName: "firs-order-discount-popup" */
            /* webpackMode: "lazy" */
            '@/components/FirstOrderDiscount/FirstOrderDiscountPopup.vue'
        )),
    },
    async serverPrefetch() {
        const ssrContext: SSRContext = {...((useSSRContext() as SSRContext) || {}), route: this.$route};
        const {token, session, userAgent, experiments, env: processEnv, province: ssrProvince} = ssrContext;
        this.$store.commit('ssrContext', ssrContext);

        // API client config
        const apiBaseUrl = processEnv?.API_INTERNAL_ENDPOINT;
        if (apiBaseUrl) client.defaults.baseURL = apiBaseUrl;
        if (userAgent) client.defaults.headers['user-agent'] = userAgent;

        this.$store.dispatch('userAgentMobile', userAgent); // check mobile UserAgent

        // robot flag & robot view = desktop
        const robot:CrawlerLabel|string|false = await this.$store.dispatch('robot');
        if (robot && robot !== CrawlerLabel.lighthouse) this.observeResize(true);

        // apply server's sessionId & authToken
        this.$store.dispatch('session/start', {sessionId: session, authToken: token});

        // google experiments
        this.$store.dispatch('experiments/on-server', experiments);

        // provinceCity && selectedCity
        const {province, city} = this.$route.params || {};
        if (province || city) await this.$store.dispatch('city/province', ssrProvince || province || city);

        await this.$store.dispatch('user/start');
        await Promise.all([
            this.$store.dispatch('updateSupportPhone', true),
            this.$store.dispatch('search/hints', null),
            this.$store.dispatch('sidebar/start'),
            this.$store.dispatch('auth/info'),
        ]);

        if (!this.$store.state.selectedCity) {
            this.$store.commit('httpStatus', 503);
            console.log('CITY_UNDEFINED', ssrContext.url);
        }
    },
    beforeMount() {

        // robot flag
        this.$store.dispatch('robot');

        // google experiments
        this.$store.dispatch('experiments/on-client');

        // выводим 404 и наблюдаем за сменой роута
        if (this.ssr404notFound) this.observe404drop();

        // запоминаем sid/token в куки
        const sessionId = this.$store.getters['session/id'];
        const authToken = this.$store.getters['session/token'];
        if (sessionId || authToken) this.$store.dispatch('session/cookies');
        else this.$store.dispatch('session/start');

        Promise.all([
            this.$store.dispatch('search/hints'),
            this.$store.dispatch('sidebar/start'),
            this.$store.dispatch('tracking/start', {router: this.$router, store: this.$store}),
            this.$store.dispatch('updateSupportPhone'),
            this.$store.dispatch('userAgentMobile', navigator.userAgent),
            this.$store.dispatch('questionnaire/start', this.$router),
        ]).catch(e => console.error(e));

    },
    async mounted(): Promise<void> {
        this.showCookie = !!cookie.get('acceptCookie') && cookie.get('acceptCookie') === '1';

        console.log('context', {...this.$store.state.ssrContext});
        console.log('session', {...this.$store.state.session});

        this.observeResize(true);

        reloadOnNewBuild();

        await this.$store.dispatch('city/default');

        // FYI: duplicate router.beforeEach here
        const {province: provinceUrl} = this.$route.params;
        if (provinceUrl) await this.$store.dispatch('city/province', provinceUrl);

        await this.$store.dispatch('superuser/start');
        await this.$store.dispatch('user/start');

        const state = this.$store.state;

        if (state.session.newcomer && !state.robot) { // Fresh flesh
            setTimeout(() => this.$store.dispatch('overlay/toggle', 'townSelector'), 250);
        }

        const {UserLogin: showLogin, referal_id: referalId} = this.$route.query;
        if (referalId) this.$store.dispatch('referralCookie', referalId);
        await this.$store.dispatch('auth/info');
        if (showLogin && !state.auth.phone) this.$store.dispatch('auth/overlay', true);

        this.$watch('$store.state.provinceCity', (nv:CityModel, ov:CityModel) => this.onProvinceChange(nv, ov));

        if (!state.auth?.phone && !this.experimentDiscountVersion) {
            const version = (window as WithOptimize).google_optimize?.get(this.experimentDiscountId);
            if (version) this.$store.dispatch('experiments/version', {version, id: this.experimentDiscountId});
        }

        // Lens experiment
        const lensExpId = 'xiIfUJebR_OcLrbj56nRag';
        const lensExpInCookie = cookie.get(`apteka_optimize_${lensExpId}`);
        if (lensExpInCookie) {
            this.$store.commit('experiments/version', {id: lensExpId, version: lensExpInCookie});
        } else {
            const lensExpVersion = (window as WithOptimize).google_optimize?.get(lensExpId);
            if (lensExpVersion) await this.$store.dispatch('experiments/version', {version: lensExpVersion, id: lensExpId});
        }

        await this.$store.dispatch('firstOrderDiscount');
    },

    beforeUnmount() {
        this.observeResize(false);
    },
    data(): {
        tabletView?:boolean;
        mobileView?:boolean;
        } {
        return {
            tabletView: undefined, // <= 1200px
            mobileView: undefined, // <= 1024px
        };
    },
})
export default class App extends Vue {
    showCookie: boolean = true;

    /*
        Эксперимент с первой скидкой
    */
    experimentDiscountId: string = 'Boy9Xrs2RpS_qw_l5uSPxw';
    get experimentDiscountVersion(): string | undefined {
        return this.$store.getters['experiments/byId'](this.experimentDiscountId);
    }

    onProvinceChange(nv:CityModel, ov:CityModel) {
        if (!ov || nv !== null) return;

        const r = {...this.$route};
        delete r.params.province;
        const route = {name: r.name!, params: r.params, query: r.query, hash: r.hash};
        // TODO: this.$router.replace({params});
        this.$router.replace(route);
    }

    get isNewUserMobile(): boolean {
        return !this.userHasOrders && this.isMobileUserAgent && this.isMobile;
    }

    get userHasOrders(): boolean {
        const authHasOrders = (this.$store.state.auth as AuthState).user.hasOrders;
        return authHasOrders || false;
    }

    get isHome(): boolean {
        return this.$route.name === 'home';
    }

    get isMobileUserAgent(): boolean {
        return !!this.$store.state.userAgentMobile;
    }

    get isMobile(): boolean {
        return this.$data.mobileView === undefined ? true : this.$data.mobileView;
    }

    get catalog(): boolean {
        return this.$store.state.catalogOverlayIsVisible;
    }

    get loading(): boolean {
        return this.$store.state.loading;
    }

    get overlayNext(): string | null {
        return this.$store.state.overlay?.next || null;
    }

    get overlayVisible(): string | null {
        return this.$store.state.overlay?.visible || null;
    }

    get sidebarHidden(): boolean {
        return this.$store.state.sidebar?.hidden || false;
    }

    get showConfirmModal(): boolean {
        const q = this.$route.query;
        return !!q.userid && !!q.confirmstr;
    }

    set showConfirmModal(st: boolean) {
        const r = {...this.$route};
        const route = {
            name: r.name || '',
            hash: r.hash,
            params: r.params,
            query: {...r.query, userid: undefined, confirmstr: undefined},
        };
        this.$router.replace(route);
    }

    get showUnsubscribeModal(): boolean {
        const q = this.$route.query;
        return !!(q.operation && q.operation === 'unsubscribe' &&
            q.token && q.token.length &&
            q.mailtype && q.mailtype.length &&
            this.$route.name === 'user-subscribes');
    }

    set showUnsubscribeModal(st: boolean) {
        const r = {...this.$route};
        const route = {
            name: r.name || '',
            hash: r.hash,
            params: r.params,
            query: {...r.query, operation: undefined, token: undefined, mailtype: undefined},
        };
        this.$router.replace(route);
    }

    /**
     * Если ssr отдал 404 - смотрим за сменой роута и сбрасываем статус
     */
    observe404drop() {
        console.warn('view 404 not found');
        const reset = () => (this.$store.commit('httpStatus', 200), unwatch());
        const unwatch:any = this.$watch('$route', reset, {deep: true});
    }

    get ssr404notFound():boolean {
        const ssrContext:undefined|SSRContext = this.$store.state.ssrContext;
        return ssrContext?.httpStatus === 404;
    }

    /**
     * переключение wide/tablet/mobile
     */
    resizeObserver:ResizeObserver|null = null;
    observeResize(enable:boolean = true) {

        if (!this.$el || typeof window === 'undefined') return this.onresize();

        if (!enable) {
            if (this.resizeObserver) this.resizeObserver.disconnect();
            return this.resizeObserver = null;
        }

        if (!(window as any).ResizeObserver) return this.onresize();

        if (!this.resizeObserver) {
            const callback:ResizeObserverCallback = (entries:ResizeObserverEntry[]) => {
                /*
                const entry = entries[0] as ResizeObserverEntry;
                let contentBoxSize = entry.contentBoxSize;
                contentBoxSize = Array.isArray(contentBoxSize) ? contentBoxSize[0] : contentBoxSize;
                this.onresize(contentBoxSize.inlineSize);
                */
                this.onresize();
            };
            this.resizeObserver = new (window as any).ResizeObserver(callback);
        }
        this.resizeObserver?.observe(this.$el as HTMLElement);
    }

    onresize() {
        const width = typeof window === 'undefined' ? 1201 : window.innerWidth;
        this.$data.tabletView = width <= 1200;
        this.$data.mobileView = width <= 1024;

        const windowType = this.$store.state.windowWidthType;
        let newWindowType = null;

        if (width <= 800 && windowType !== WindowWidthTypes.Mobile) {
            newWindowType = WindowWidthTypes.Mobile;
        } else if (
            width > 800 && width <= 1024
            && windowType !== WindowWidthTypes.HalfMobile
        ) {
            newWindowType = WindowWidthTypes.HalfMobile;
        } else if (
            width > 1024 && width <= 1200
            && windowType !== WindowWidthTypes.Tablet
        ) {
            newWindowType = WindowWidthTypes.Tablet;
        } else if (width > 1200 && windowType !== WindowWidthTypes.Desktop) {
            newWindowType = WindowWidthTypes.Desktop;
        }

        if (newWindowType) this.$store.dispatch('changeWindowWidthType', newWindowType);
    }
}
