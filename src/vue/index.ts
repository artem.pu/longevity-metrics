import {Vue, Options} from 'vue-class-component';
import {useSSRContext, defineAsyncComponent, defineComponent, shallowReactive} from 'vue';
import {Prop, Watch, Emit, mixins, Inject, Provide} from 'vue-property-decorator';
import eventBus from '@/utils/eventBus';
export default Vue;
export {
    Vue,
    Options,
    Options as Component,
    Emit,
    Prop,
    Watch,
    mixins as Mixins,
    useSSRContext,
    defineAsyncComponent,
    defineComponent,
    shallowReactive,
    eventBus as ebus,
    Provide,
    Inject,
};
