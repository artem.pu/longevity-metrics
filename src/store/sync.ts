import {MutationPayload, Store} from 'vuex';
import {MT as rootMutation} from '@/store/mutation-type';
import {MutationTypes as basketMutation} from '@/store/basket/mutation-types';
import {RootState} from '@/types/root-state';

interface SyncData {
    type:string;
    payload:MutationPayload;
    windowId:string;
}

class SharedMutationsPlugin {
    debug:boolean = false;
storageKey:string = 'app.tabSync';
private readonly windowId:string;
private $storage:Storage;
private $store:Store<RootState>;
private preventCrossUpdate:boolean = false;
constructor(store:Store<RootState>) {
    this.windowId = window.sessionStorage.getItem('windowId') || '';
    if (!this.windowId) {
        const windowId = Math.random().toString(36).substr(2, 8);
        window.sessionStorage.setItem('windowId', this.windowId = windowId);
    }
    this.trace('\u267A');
    this.$store = store;
    this.$storage = window.localStorage;
    window.addEventListener('storage', (se:StorageEvent) => {
        if (se.key === this.storageKey) this.storageChanged();
    });
}

private encodeMutation(type:string, payload:MutationPayload) {
    const data:SyncData = {
        type,
        payload,
        windowId: this.windowId,
    };
    return JSON.stringify(data);
}

private static decodeMutation(value:string):SyncData {
    return JSON.parse(value);
}

public mutationsHandler(type:string, payload:MutationPayload, state:RootState) {
    if (this.preventCrossUpdate) {
        this.trace(`BREAK "${type}"`);
        this.preventCrossUpdate = false;
    } else {
        this.trace(`EMIT: "${type}"`, payload);
        this.$storage.setItem(this.storageKey, this.encodeMutation(type, payload));
    }
}

private storageChanged() {
    const storageData = this.$storage.getItem(this.storageKey) || '';
    const syncData:SyncData = SharedMutationsPlugin.decodeMutation(storageData);
    this.preventCrossUpdate = true;
    const {type, payload} = syncData;
    this.trace(`INCOMING "${type}"`, payload, syncData);
    this.$store.commit(type, payload, {});
}

private trace(...argumentz:any) {
    if (!this.debug) return;
    const args = argumentz.map((arg:any) => {
        if (typeof arg === 'string' && arg.length > 120) return arg.slice(0, 24);
        return arg;
    });
    args.unshift('color:darkgreen;font-size:100%');
    args.unshift(`%c[TabSync#${this.windowId}]`);
    console.log(...args);
}
}
export default (store:Store<RootState>) => {
    // вызывается после инициализации хранилища
    if (typeof window === 'undefined') return;
    const sharedMutations:string[] = [
        rootMutation.FAVORITES_COUNT,
        rootMutation.SELECT_CITY,
        rootMutation.SELECT_SHOP,
        `basket/${basketMutation.short}`,
        `basket/${basketMutation.load}`,
        'auth/drop',
        'auth/login',
        'auth/token',
        'auth/session',
        'superuser/token',
        'superuser/order',
        'superuser/additionalItems',
    ];
    const plugin = new SharedMutationsPlugin(store);

    store.subscribe((mutation:{ type:string; payload:MutationPayload; }, state:RootState) => {
        // вызывается после каждой мутации
        const { type, payload } = mutation;
        if (sharedMutations.indexOf(type) === -1) return;
        plugin.mutationsHandler(type, payload, state);
    });
};
