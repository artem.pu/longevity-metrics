import {Module} from 'vuex';
import {RootState} from '@/types/root-state';
import {MetricLabels} from '@/types/google';

import $gapi, {UserInfo} from './api.google';

interface GoogleState {
    userinfo: UserInfo|null;
}

const state = (): GoogleState => ({
    userinfo: null,
});

const vuexModule: Module<GoogleState, RootState> = {
    state,
    actions: {
        async initialize({state, dispatch, commit}) {
            const options = {loginCallback: (userinfo:UserInfo|null) => commit('userinfo', userinfo)};
            const userinfo = await $gapi.initialize(options);
            commit('userinfo', userinfo);
            return userinfo;
        },

        async login({commit}) {
            $gapi.login();
        },

        loginCallback({commit}, userinfo:UserInfo) {
            commit('userinfo', userinfo);
        },

        logout({}) {
            $gapi.logout();
        },

        async getUserCounters() {
            const {result: profiles, error} = await $gapi.fetchAccountCounters();
            // console.log(webproperties.filter((ct:any) => !ct.defaultProfileId));

            // const cn = profiles.find((ct:any) => ct.websiteUrl.indexOf('www.cn.ru') >= 0);
            // console.log('CN.RU', cn);
            // console.log(profiles);
            if (error) return console.error(error.message), [];
            return profiles.map((profile:any) => ({
                id: profile.id,
                site: profile.websiteUrl,
                name: profile.name,
                data: profile,
            }));
        },

        async fetchPageviews({getters}, payload:{viewId:string}) {
            // https://ga-dev-tools.web.app/account-explorer/ - counters access
            // https://ga-dev-tools.web.app/dimensions-metrics-explorer/page-tracking
            // const VIEW_ID = '64480905';
            console.log('viewId', payload.viewId);
            const body = {
                reportRequests: [
                    {
                        viewId: payload.viewId.toString(),
                        dateRanges: [
                            {startDate: '7daysAgo', endDate: 'yesterday'},
                        ],
                        dimensions: [
                            {name: 'ga:date'},
                        ],
                        metrics: [
                            {expression: 'ga:1dayUsers'},
                            {expression: 'ga:sessions'},
                            {expression: 'ga:pageviews'},
                        ],
                    },
                ],
            };
            const root = 'https://analyticsreporting.googleapis.com/';
            const params = {body, root, path: '/v4/reports:batchGet'};
            const reports = await $gapi.request(params);
            return reports;
        },
    },
    mutations: {
        userinfo(state, userinfo?:UserInfo) {
            // console.log('userinfo', userinfo);
            state.userinfo = userinfo || null;
        },
    },
    namespaced: true,
};

export {
    GoogleState,
    vuexModule,
};
