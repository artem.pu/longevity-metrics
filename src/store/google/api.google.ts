// API: https://ga-dev-tools.web.app/ga4/query-explorer/
import {loadScript} from '@/utils/load';
// import JWT from '@/utils/jwt';

const userdata = {
    resourceName: 'people/108381392662990226590',
    etag: '%EiMBAgMFBgcICQoLDA0ODxATFBUWGSEiIyQlJicuNDU3PT4/QBoEAQIFBw==',
    names: [
        {
            metadata: {
                primary: true,
                source: {
                    type: 'PROFILE',
                    id: '108381392662990226590',
                },
                sourcePrimary: true,
            },
            displayName: 'Artem Pu',
            familyName: 'Pu',
            givenName: 'Artem',
            displayNameLastFirst: 'Pu, Artem',
            unstructuredName: 'Artem Pu',
        },
    ],
    photos: [
        {
            metadata: {
                primary: true,
                source: {
                    type: 'PROFILE',
                    id: '108381392662990226590',
                },
            },
            url: 'https://lh3.googleusercontent.com/a-/AOh14GgkOzGPdQlFE1vaUyVF4hPSYR4RFc4nSdXyjyrT=s100',
        },
    ],
    emailAddresses: [
        {
            metadata: {
                primary: true,
                verified: true,
                source: {
                    type: 'ACCOUNT',
                    id: '108381392662990226590',
                },
                sourcePrimary: true,
            },
            value: 'artempoo@gmail.com',
        },
    ],
};

interface InitialData {accessToken?:string, userinfo?:UserInfo, expires?:string}
interface UserInfo {
    name?:string;
    email?:string;
    photo?:string;
}

enum localStorageKeys {
    GOOGLE_INITIAL_DATA = 'app.googleInitialData',
}

interface TokenResponse {
    // eslint-disable-next-line camelcase
    access_token: string;
    authuser: '0';
    /** The token will become rotten after N seconds */
    // eslint-disable-next-line camelcase
    expires_in: number;
    prompt: 'none';
    scope: string;
    // eslint-disable-next-line camelcase
    token_type: 'Bearer';
}

const API_KEY = 'AIzaSyAuJEqPsgNGkaODIvGmsodr01-UXZyTSsU';
const CLIENT_ID = '344143488429-5ijfbui52u5dve9icv4h0ja9n5cjqbjc.apps.googleusercontent.com';
const SCOPE = [
    'https://www.googleapis.com/auth/userinfo.profile',
    'https://www.googleapis.com/auth/analytics.readonly',
    // 'https://www.googleapis.com/auth/user.emails.read',
    // 'https://www.googleapis.com/auth/analytics',
    // 'https://www.googleapis.com/auth/analytics.edit',
];
const DDOCS = [ // discoveryDocs
    'https://www.googleapis.com/discovery/v1/apis/people/v1/rest',
    'https://analyticsreporting.googleapis.com/$discovery/rest?version=v4',
];

class GoogleApi {
    // constructor() {}
    private tokenClient:any; // google oauth2 TokenClient
    private $google:any; // google api (accounts.google.com/gsi/client)
    private $gapi:any; // google-api-client (apis.google.com/js/api.js)
    private accessToken:string|null = null;
    private loginCallback?:Function;

    public async initialize(options?:{loginCallback?:Function}):Promise<UserInfo|null> {

        if (options?.loginCallback) this.loginCallback = options?.loginCallback;

        const userinfo = this.initialData.userinfo || null;
        if (userinfo) this.loginCallback?.(userinfo);

        await this.initializeGapi();
        // userinfo = await this.fetchUserInfo();
        return userinfo;
    }

    private loading:null|Promise<void> = null;
    private async initializeGapi() {
        console.log('INIT');
        // let loadingResolve:(value: void | PromiseLike<void>) => void;
        let loadingResolve:any;
        this.loading = new Promise((resolve) => loadingResolve = resolve);

        const params = {apiKey: API_KEY, discoveryDocs: DDOCS};
        const accessToken = this.initialData?.accessToken; // полученный ранее токен
        const gapi = await loadScript('https://apis.google.com/js/api.js', 'gapi');
        await new Promise((resolve, reject) => gapi.load('client', {callback: resolve, onerror: reject}));
        await new Promise((resolve) => gapi.client.load('analytics', 'v3', (x:any) => {
            console.log('analytics', x);
            resolve(1);
        }));
        await gapi.client.init(params)
            .catch((err:Error) => console.error(err));
        if (accessToken) gapi.auth.setToken({access_token: accessToken});

        this.$gapi = gapi;
        this.loading = null;

        console.log('loadingResolve');
        loadingResolve();
    }

    private async initAuthClient() {
        const google = await loadScript('https://accounts.google.com/gsi/client', 'google');
        this.tokenClient = google.accounts.oauth2.initTokenClient({
            client_id: CLIENT_ID,
            scope: SCOPE.join(' '),
            callback: (res:TokenResponse) => this.commitTokenResponse(res),
        });
        console.log(this.tokenClient);

        this.$google = google;
        this.$google.accounts.id.initialize({client_id: CLIENT_ID});
        // this.renderButton(google, 'googleAuthButton');
    }

    async commitTokenResponse(response:TokenResponse) {
        if (!this.$gapi) await this.initializeGapi();

        const {access_token: accessToken, expires_in: expiresIn} = (response || {});
        console.log('tokenResponse', response);

        const expires = Date.now() + ((expiresIn || 3600) * 1e3); // 1h
        const userinfo = await this.fetchUserInfo();
        this.initialData = {
            accessToken,
            expires: new Date(expires).toString(),
            userinfo: userinfo || undefined,
        };

        if (this.loginCallback) this.loginCallback(userinfo);
    }

    private renderButton(google:any, id:string) {
        const button = document.getElementById(id);
        const custom = {
            type: 'standard',
            theme: 'outline',
            shape: 'rectangular',
            size: 'large',
        };
        google.accounts.id.renderButton(button, custom);
    }

    private async fetchUserInfo():Promise<UserInfo|null> {
        // https://developers.google.com/people/api/rest/v1/people/get?hl=en_US
        const personFields = ['emailAddresses', 'names', 'photos'];
        const query = `personFields=${personFields.join(',')}`;
        const root = 'https://people.googleapis.com/';
        const path = `/v1/people/me?${query}`;
        const params = {root, path, method: 'GET'};

        let res = null;
        let error:any = null;
        try {
            res = await this.$gapi.client.request(params)
                .then((result:any) => result)
                .catch((err:any) => error = err.result.error, null);
        } catch (err) {
            console.log('ERROR at client.request', err);
        }

        if (error) return console.error(`FAILURE ${error.message}`), null;
        if (!res?.result) return console.error('USER', res?.result), null;

        const userdata = res?.result;
        const getPrimary = function (items?:any[]) {
            if (!items?.length) return undefined;
            if (items.length === 1) return items[0];
            return items.find(v => v.metadata.primary) || items[0];
        };

        return <UserInfo>{
            name: getPrimary(userdata.names)?.displayName || undefined,
            email: getPrimary(userdata.emailAddresses)?.value || undefined,
            photo: getPrimary(userdata.photos)?.url || undefined,
        };
    }

    public async login() {
        if (!this.tokenClient) await this.initAuthClient();
        this.tokenClient.requestAccessToken();
    }

    public async logout() {
        this.initialData = null;
        this.loginCallback?.(null);
        // this.$google.accounts.oauth2.revoke(access_token);
    }

    private set initialData(payload:InitialData|null) {
        const key = localStorageKeys.GOOGLE_INITIAL_DATA;
        if (payload === null) window.localStorage.removeItem(key);
        else {
            const initialData = {...this.initialData, ...payload};
            const data = JSON.stringify(initialData);
            window.localStorage.setItem(key, data);
        }
    }

    private get initialData():InitialData {
        const data = window.localStorage.getItem(localStorageKeys.GOOGLE_INITIAL_DATA);
        const initialData:InitialData = data ? JSON.parse(data) : {};
        const expires = new Date(initialData.expires!);

        if (Date.now() > expires.getTime()) {
            console.log('outdated');
            return this.initialData = null, {};
        }

        return initialData;
    }

    public async request(params:any):Promise<{result:null|object; error:Error}> {
        if (!this.initialData.accessToken) {
            const error = new Error('403');
            return {error, result: null};
        }

        const reqParams = {
            method: 'POST',
            ...params,
        };

        // const result = {data: null, error: null};
        let result:any = null;
        let error:any = null;
        await this.$gapi.client.request(reqParams)
            // .then(this.requestCallback.bind(this))
            // .catch(this.requestError.bind(this));
            .then((response:any) => {
                result = response.status === 200 ? response.result : null;
                error = response.status === 200 ? null : {message: `Error ${response.status}`};
            })
            .catch((response:any) => error = response.result.error);

        if (error) console.error('request', error.message || error);
        return {result, error};
    }

    public async fetchAccountCounters() {
        // if (!this.$gapi) await this.initializeGapi();
        if (!this.$gapi) console.warn('waiting $gapi'), await this.loading;
        if (!this.initialData.accessToken) return {error: new Error('403'), result: []};

        // if (!this.$gapi.client.analytics?.management) {
        //     console.error('WAIT!', await this.loading);
        //     console.error('WAIT done');
        // }

        let error = null;
        const profiles:any = await new Promise((resolve) => {
            var request = this.$gapi.client.analytics.management.profiles.list({
                accountId: '~all',
                webPropertyId: '~all',
            });
            request.execute((response:any) => {
                if (response?.error) {
                    error = new Error(response.error.message);
                    console.error(response.error);
                }
                resolve(response?.items || []);
            });
        });

        return {
            error,
            result: !profiles?.length ? [] : profiles,
        };
    }

    private requestError(err:any) {
        console.error('FAIL', err.result.error.message);
        return null;
    }

    private requestCallback(response:any) {
        console.log('DATA', response.result.reports[0]);
        // var formattedJson = JSON.stringify(response.result, null, 2);
        // document.getElementById('query-output').value = formattedJson;
        return response.result.reports;
    }

}
const gapi = new GoogleApi();
export {
    UserInfo,
    gapi,
    gapi as default,
};