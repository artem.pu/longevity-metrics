enum MT {
    ssrContext = 'ssrContext',
    prefetch = 'prefetch',
    userAgentMobile = 'userAgentMobile',
    FAVORITES_COUNT = 'FAVORITES_COUNT',
    LOAD_PRODUCT = 'LOAD_PRODUCT',
    REGION_TOWN = 'REGION_TOWN',
    SELECT_CITY = 'SELECT_CITY',
    SELECT_SHOP = 'SELECT_SHOP',
    LOAD_SIDEBAR = 'LOAD_SIDEBAR',
    USER_COMMIT = 'USER_COMMIT',
    HIDE_MODAL = 'HIDE_MODAL',
    SHOW_MODAL = 'SHOW_MODAL',
    TOGGLE_CATALOG_OVERLAY = 'SHOW_CATALOG_OVERLAY',
    loading = 'loading',
    message = 'message',
    vitamins = 'vitamins',
    vitaminsDetails = 'vitaminsDetails',
    pageData = 'pageData',
    history = 'history',
    robot = 'robot',
    surveyModal = 'surveyModal',
    supportPhone = 'supportPhone',
    httpStatus = 'httpStatus',
    progressiveDiscountPopup = 'progressiveDiscountPopup',
    progressiveDiscount = 'progressiveDiscount',
    progressiveRecipe = 'progressiveRecipe',
    prescriptionsPopup = 'prescriptionsPopup',
    confirmEmailNotifyPopup = 'confirmEmailNotifyPopup',
    windowWidthType = 'windowWidthType',
    referralPopup = 'referralPopup',
    firstOrderDiscount = 'firstOrderDiscount',
    firstOrderDiscountPopup = 'firstOrderDiscountPopup',
}

export {MT};
