import cookies from '@/utils/cookie';

class AppStorage {

    set token(value: string | undefined) {
        console.log('set token', value);
    }

    get token(): string | undefined {
        return undefined;
    }

    set selectedShop(drugstoreId: string | null) {
        if (drugstoreId) cookies.set('selectedShop', drugstoreId);
        else cookies.del('selectedShop');
    }

    get selectedShop(): string | null {
        const drugstoreId = cookies.get('selectedShop');
        return drugstoreId || null;
    }
}

export default new AppStorage();
