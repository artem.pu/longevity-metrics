import {ActionTree} from 'vuex';
import {RootState} from '@/types/root-state';
import {
    WebSiteProfile,
} from '@/types/metrics';

export default <ActionTree<RootState, RootState>>{
    async fetchPageviews({state, dispatch}, counter:WebSiteProfile) {

        let yandexReport = null;
        let googleReport = null;
        const errors = [];

        if (counter.googleId) {
            const ymParams = {viewId: counter.googleId};
            const {error, result}:{result:any; error?:Error} = await dispatch('google/fetchPageviews', ymParams);
            if (error) console.error('fetch Greport error', error), errors.push(error);
            else googleReport = result?.reports?.[0] || null;
        }

        if (counter.yandexId) {
            const gaParams = {ids: [counter.yandexId]};
            const {error, result}:{result:any; error?:Error} = await dispatch('yandex/fetchPageviews', gaParams);
            if (error) console.error('fetch Yreport error', error), errors.push(error);
            else yandexReport = result || null;
        }

        return {yandexReport, googleReport, errors};
    },
};
