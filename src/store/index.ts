import Vuex, {Store} from 'vuex';
import {RootState} from '@/types/root-state';
import {vuexModule as google} from './google';
import {vuexModule as yandex} from './yandex';
import {vuexModule as counters} from './counters';

import actions from './actions';

export function createStore(): Store<RootState> {
    return new Vuex.Store({
        actions,
        mutations: {},
        strict: true,
        state: (): RootState => ({}),
        modules: {
            google,
            yandex,
            counters,
        },
    });
}
