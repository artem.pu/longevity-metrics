import {MutationTree} from 'vuex';
import {RootState} from '@/types/root-state';
import {MT} from '@/store/mutation-type';
import {VariantItem} from '@/types/variant';
import {SSRContext, ServerEnvVars} from '@/types/server';
import AutoDestModel = Models.Mongo.AutoDestModel;
import GoodVendorModel = Models.Mongo.GoodVendorModel;
import CityModel = Models.Mongo.CityModel;
import {VitBalanceDetails} from '@/types/vitamins';
import PrDiscDetails = Models.PrDiscount.PrDiscDetails;

export default <MutationTree<RootState>>{
    [MT.SELECT_CITY]: (state: RootState, city?: CityModel) => {
        state.selectedCity = city || null;
    },
    [MT.SELECT_SHOP]: (state: RootState, shop?: AutoDestModel) => {
        state.selectedShop = shop || null;
    },
    [MT.prefetch]: (state: RootState, data: { [key: string]: any }) => {
        const prefetchData = {...state.prefetch, ...data};
        state.prefetch = prefetchData;
    },
    [MT.userAgentMobile]: (state: RootState, isMobile: boolean) => {
        state.userAgentMobile = isMobile;
    },
    [MT.pageData]: (state: RootState, data) => {
        state.pageData = data || undefined;
    },
    [MT.ssrContext]: (state: RootState, ssrContext: SSRContext) => {
        const ssrContextData: SSRContext = {...ssrContext};
        for (const k in ssrContextData) {
            const valType = typeof ssrContextData[k];
            if (k === 'env') continue;
            else if (k === 'styles') delete ssrContextData[k];
            else if (valType === 'function') delete ssrContextData[k];
            else if (valType === 'object') delete ssrContextData[k];
        }

        delete ssrContextData['styles'];

        // светить весь process.env несекьюрно. Посему - убрать всё, кроме ServerEnvVars
        const processEnv = {...ssrContextData.env};
        const whitelist:Array<keyof ServerEnvVars> = ['API_INTERNAL_ENDPOINT', 'API_EXTERNAL_ENDPOINT'];
        ssrContextData.env = Object.keys(processEnv)
            .filter(key => whitelist.includes(key as any))
            .reduce((obj, key) => (obj[key] = processEnv[key], obj), {});

        state.ssrContext = ssrContextData;
    },

    [MT.FAVORITES_COUNT]: (state: RootState, delta: number) => {
        // изменяет счётчик "избранного" на величину delta
        if (!state.auth) return;
        const count = state.auth.user.favoritesCount;
        const value = (count || 0) + delta;
        (state.auth.user as any).favoritesCount = value > 0 ? value : 0;
    },
    [MT.vitamins]: (state: RootState, amount: number | null) => {
        state.vitamins = amount;
    },
    [MT.vitaminsDetails]: (state: RootState, info: VitBalanceDetails | undefined) => {
        state.vitaminsDetails = info;
    },
    [MT.REGION_TOWN]: (state: RootState, city?: CityModel) => {
        state.provinceCity = !city ? null : city;
        // state.provinceTown = !city ? null : city.url;
    },
    [MT.history]: (state: RootState, history: VariantItem[]) => {
        state.history = history;
    },
    [MT.LOAD_PRODUCT]: (state: RootState, product: GoodVendorModel) => {
        state.product = product;
    },
    [MT.HIDE_MODAL]: (state: RootState) => {
        state.modal = {itemId: undefined, visible: false};
    },
    [MT.SHOW_MODAL]: (state: RootState, id: string) => {
        state.modal = {itemId: id, visible: true};
    },
    [MT.robot]: (state: RootState, robot: false | string) => {
        state.robot = robot;
    },
    [MT.surveyModal]: (state: RootState, surveyModal: boolean) => {
        state.surveyModal = surveyModal;
    },
    [MT.supportPhone]: (state: RootState, phone: string) => {
        state.supportPhone = phone;
    },
    [MT.progressiveDiscountPopup]: (state: RootState, val: boolean) => {
        state.progressiveDiscountPopup = val;
    },
    [MT.progressiveDiscount]: (state: RootState, data: PrDiscDetails | undefined) => {
        state.progressiveDiscount = data;
    },
    [MT.prescriptionsPopup]: (state: RootState, val: boolean) => {
        state.prescriptionsPopup = val;
    },
    [MT.confirmEmailNotifyPopup]: (state: RootState, val: boolean) => {
        state.confirmEmailNotifyPopup = val;
    },
    [MT.progressiveRecipe]: (state: RootState, val: boolean) => {
        state.progressiveRecipe = val;
    },
    [MT.httpStatus]: (state: RootState, status: number) => {
        if (state.ssrContext) state.ssrContext.httpStatus = status;
    },
    [MT.referralPopup]: (state: RootState, val: boolean) => {
        state.referralPopup = val;
    },
    [MT.windowWidthType]: (state: RootState, type) => {
        state.windowWidthType = type;
    },
    [MT.firstOrderDiscount]: (state: RootState, discount: number) => {
        state.firstOrderDiscount = discount;
    },
    [MT.firstOrderDiscountPopup]: (state: RootState, val: boolean) => {
        state.firstOrderDiscountPopup = val;
    },
    loading: (state: RootState, value: boolean) => {
        state.loading = value;
    },
};
