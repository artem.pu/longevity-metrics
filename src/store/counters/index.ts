interface CountersState {
    counters: any;
}

const state = ():CountersState => ({
    counters: null,
});

const vuexModule = {
    namespaced: true,
    actions: {},
    mutations: {},
    state,
};

export {
    vuexModule,
};
