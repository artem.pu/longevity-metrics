/* eslint-disable camelcase */
interface YandexTokenData {
    access_token?:string;
    token_type?:'bearer';
    expires_in?:string;
}
interface YandexUserInfo {
    login: string;
    old_social_login: string;
    id: string;
    client_id: string;
    is_avatar_empty: boolean;
    default_avatar_id: string;
    openid_identities: string[];
    psuid: string;
    default_email: string;
    display_name: string;
    emails: string[];
    first_name: string;
    last_name: string;
    real_name: string;
    sex: 'male'|'female';
}
/* eslint-enable camelcase */

enum YandexAvaSize {
    's28' = 'islands-small', // 28×28 пикселей.
    's34' = 'islands-34', // — 34×34 пикселей.
    's42' = 'islands-middle', // — 42×42 пикселей.
    's50' = 'islands-50', // — 50×50 пикселей.
    's56' = 'islands-retina-small', // — 56×56 пикселей.
    's68' = 'islands-68', // — 68×68 пикселей.
    's75' = 'islands-75', // — 75×75 пикселей.
    's84' = 'islands-retina-middle', // — 84×84 пикселей.
    's100' = 'islands-retina-50', // — 100×100 пикселей.
    's200' = 'islands-200', // — 200×200 пикселей.
}

export interface UserInfo {
    name?:string;
    email?:string;
    photo?:string;
}

interface InitialData {
    accessToken:string,
    userinfo:UserInfo,
    expires:string,
}

enum localStorageKeys {
    YANDEX_INITIAL_DATA = 'app.yandexInitialData',
}

export class YandexApi {
    private accessToken:string|null = null;
    private loginCallback?:Function;
    public async initialize(options?:{loginCallback?:Function}):Promise<UserInfo|null> {

        // this.throwLoginMessage(); moved to client-entry.ts

        if (options?.loginCallback) this.loginCallback = options?.loginCallback;

        const {accessToken, userinfo} = this.initialData || {};
        if (userinfo) {
            this.accessToken = accessToken!;
            this.loginCallback?.(userinfo);
        } else this.initialData = null;

        // await this.initializeYapi();
        return this.initialData?.userinfo || null;
    }

    private throwLoginMessage() {
        // this is callback redirect from yandex OAuth form
        // throw message to parent, then close
        const hash = (window.location.hash || '').substring(1);
        if (window.opener && hash.indexOf('access_token=') !== -1) {
            window.opener.postMessage(hash, location.origin);
            window.close();
        }
    }

    public logout() {
        this.initialData = null;
        this.loginCallback?.(null);
    }

    public login() {
        const redirectUri = location.origin.concat(location.pathname);
        const params = {
            // force_confirm: '1',
            response_type: 'token',
            client_id: 'f0512f33ef924ba886dd92b74f3abeea',
            redirect_uri: redirectUri,
            display: 'popup',
        };

        const query = new URLSearchParams(params).toString();
        const url = 'https://oauth.yandex.ru/authorize?'.concat(query);
        const winParams = `scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,
            width=360,height=500,left=100,top=100`;
        window.open(url, 'yandexOauth', winParams);
        window.addEventListener('message', (ev:MessageEvent) => this.commitTokenResponse(ev.data as string), false);
    }

    private async commitTokenResponse(oauthHash:string) {
        const params = new URLSearchParams(oauthHash);
        const token:YandexTokenData = {};
        for (const [key, value] of params.entries()) token[key] = value;

        const expires = new Date;
        if (!token.expires_in) expires.setHours(expires.getHours() + 1);
        else expires.setSeconds(parseInt(token.expires_in, 10));

        // const tokenType = params.get('token_type');
        const accessToken = params.get('access_token');
        const userinfo = !accessToken ? null : await this.fetchUserInfo(accessToken);

        if (!userinfo) return console.error('!userinfo'), this.initialData = null;
        if (!accessToken) return console.error('!token'), this.initialData = null;

        this.accessToken = accessToken;
        this.initialData = {
            userinfo,
            accessToken,
            expires: expires.toISOString(),
        };

        this.loginCallback?.(userinfo);
    }

    private set initialData(payload:InitialData|null) {
        const key = localStorageKeys.YANDEX_INITIAL_DATA;
        if (payload === null) window.localStorage.removeItem(key);
        else {
            const initialData = {...this.initialData, ...payload};
            const data = JSON.stringify(initialData);
            window.localStorage.setItem(key, data);
        }
        console.log('set', payload, this.initialData);
    }

    private get initialData():InitialData|null {
        const data = window.localStorage.getItem(localStorageKeys.YANDEX_INITIAL_DATA);
        const initialData:InitialData = data ? JSON.parse(data) : {};
        const expires = new Date(initialData.expires!);

        if (Date.now() > expires.getTime()) {
            console.warn('initialData outdated');
            return this.initialData = null;
        }

        return initialData;
    }

    async fetchMetrikaData(counterId:string = '62730118', type:string = '1') {
        // const token = 'AQAAAAABndwlAAfo78KQfItNvUEgpPEd5XYZcsA';
        // const token = 'AQAAAAABndwlAAfr-EzlevqRwkN_gN7p6udLzt4';
        if (!this.accessToken) return console.error('!token'), null;

        const baseURL = 'https://api-metrika.yandex.net';
        let path = `/management/v1/counter/${counterId}`; // данные о счётчике
        let params = {};

        if (type) { // просмотры и визиты за неделю
            path = '/stat/v1/data/bytime';
            params = {
                ids: [counterId, counterId].join(','),
                metrics: ['ym:s:pageviews', 'ym:s:visits'].join(','),
                date1: '6daysAgo',
                date2: 'today',
                group: 'day',
            };
        }

        const init = {headers: {Authorization: `OAuth ${this.accessToken}`}};
        const query = new URLSearchParams(params).toString();
        if (query.length) path = path.concat(`?${query}`); // decodeURIComponent(query)
        const res = await window.fetch(`${baseURL}${path}`, init).then(r => r.json());
        return console.log('RES', res), res;
    }

    private async fetchUserInfo(token?:string):Promise<UserInfo|null> {

        const oauthToken = token || this.accessToken;
        if (!oauthToken) return console.error('!token'), null;

        const params = {
            oauth_token: oauthToken,
            with_openid_identity: '1',
            format: 'json', // response.json()
            // format: 'jwt', // response.text()
        };

        let path = 'https://login.yandex.ru/info';
        const init:RequestInit = {mode: 'cors', headers: {'Content-Type': 'application/json'}};
        const query = new URLSearchParams(params).toString();

        if (query.length) path = path.concat(`?${query}`); // decodeURIComponent(query)
        const response:YandexUserInfo|null = await window.fetch(`${path}`, init)
            .then(response => response.status !== 200 ? null : response.json())
            .catch(error => (console.error('FUI_ERROR', error), null));

        const userinfo:UserInfo|null = !response ? null : {
            name: response.display_name,
            email: response.default_email,
            photo: `https://avatars.yandex.net/get-yapic/${response.default_avatar_id}/${YandexAvaSize.s100}`,
        };

        console.log('UU>', userinfo);
        return userinfo;
    }

    private async fetch() {
        const headers = new Headers();
        headers.set('Authorization', `OAuth ${'AQAAAAABndwlAAfr-EzlevqRwkN_gN7p6udLzt4'}`);
        // headers.set('Access-Control-Allow-Origin', location.origin);

        console.log(headers);
        const init:RequestInit = {
            headers,
            cache: 'no-cache',
            mode: 'no-cors',
            // mode: 'cors',
            method: 'GET',
        };
        const query:Record<string, string> = {
            format: 'json',
            // jwt_secret: '', // Cекретный ключ
            // with_openid_identity: '', // '1'
        };

        const path = 'https://login.yandex.ru/info';
        const url = `${path}?${(new URLSearchParams(query)).toString()}`;
        const r = await window.fetch(new Request(url, init));

        console.log('RES', r);
    }

    public async metrika(path:string, params?:Record<string, string>):Promise<{response:any; error?:Error}> {

        const headers = new Headers();
        headers.set('Authorization', `OAuth ${'AQAAAAABndwlAAfr-EzlevqRwkN_gN7p6udLzt4'}`);
        const init:RequestInit = {
            headers,
            // cache: 'no-cache',
            // mode: 'no-cors',
            method: 'GET',
        };

        const query = !params ? '' : ((new URLSearchParams(params)).toString() || '');

        let error:Error|undefined = undefined;

        const baseURL = 'https://api-metrika.yandex.net';
        const url = `${baseURL}${path}?${query}`;
        const response = await window.fetch(new Request(url, init))
            .then(res => res.status === 200 ? res.json() : null)
            .catch(err => (error = err, null));

        return {response, error};
    }

    public async request(path:string, params?:Record<string, string>):Promise<{response:any; error?:Error}> {
        if (!this.accessToken) return {response: null, error: new Error('403')};

        const baseURL = 'https://api-metrika.yandex.net';
        const init = {headers: {Authorization: `OAuth ${this.accessToken}`}};
        const query = new URLSearchParams(params).toString();
        if (query.length) path = path.concat(`?${query}`); // decodeURIComponent(query)

        let status = 200;
        const errors = [];
        const response = await window.fetch(`${baseURL}${path}`, init)
            .then(r => (status = r.status, r.json()))
            .catch((err:Error) => (errors.push(err.message), null));
        console.log('RES', response);
        // r.status === 200 ? r.json() : (, console.log(r), null)
        if (status !== 200) errors.push(`Error ${status}`);
        if (response.errors?.length) errors.push(...response.errors.map((er:Error) => er.message));
        console.log('error>', errors);

        if (errors.length) console.log('ERR', errors);
        console.log('RES', response);

        const error = errors.length ? new Error(errors.join('\n')) : undefined;
        return {error, response: error ? null : response};
    }
}