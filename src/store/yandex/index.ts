import {Module} from 'vuex';
import {RootState} from '@/types/root-state';

import {YandexApi, UserInfo} from './api.yandex';
const $yapi = new YandexApi();

interface YandexState {
    userinfo: UserInfo|null;
}

const state = (): YandexState => ({
    userinfo: null,
});

const vuexModule: Module<YandexState, RootState> = {
    state,
    actions: {
        async initialize({state, dispatch, commit}) {
            const options = {loginCallback: (userinfo:UserInfo|null) => commit('userinfo', userinfo)};
            const userinfo = await $yapi.initialize(options);
            return commit('userinfo', userinfo), state.userinfo;
        },

        async login({commit, dispatch}) {
            $yapi.login();
        },

        logout({}) {
            $yapi.logout();
        },

        /** https://yandex.ru/dev/metrika/doc/api2/management/counters/counters.html */
        async getUserCounters({state, commit}):Promise<any> {
            const {response, error} = await $yapi.metrika('/management/v1/counters');
            if (error) return console.error(error), [];
            return (response?.counters || []).map((ct:any) => ({
                id: ct.id.toString(),
                site: ct.site,
                name: ct.name,
            }));

        },

        async fetchPageviews({}, params:Record<string, any>) {
            console.log('fetchViews', params);

            // const token = 'AQAAAAABndwlAAfo78KQfItNvUEgpPEd5XYZcsA';
            // let path = `/management/v1/counter/${counterId}`; // данные о счётчике
            // let params = {};

            // посетители, визиты и просмотры за неделю
            // https://yandex.ru/dev/metrika/doc/api2/api_v1/metrics/visits/basic.html

            // const mins = -new Date().getTimezoneOffset();
            // const tz = Math.abs(mins);
            // const [h, m] = [Math.floor(tz / 60), tz % 60];
            // const timezone = encodeURIComponent(mins < 0 ? '-' : '+')
            //     .concat(`${h > 10 ? h : `0${h}`}:${m > 10 ? m : `0${m}`}`);

            const ids = params.ids ? [...params.ids] : [];
            delete params.ids;

            const path = '/stat/v1/data/bytime'; // /stat/v1/data/drilldown MAYBE?
            const query = {
                // timezone,
                ids: ids.join(','),
                metrics: ['ym:s:users', 'ym:s:visits', 'ym:s:pageviews'].join(','),
                date1: '7daysAgo',
                date2: 'yesterday',
                group: 'day',
                ...params,
            };

            const {error, response} = await $yapi.request(path, query);
            return {error, result: response};
        },
    },
    mutations: {
        userinfo(state, userinfo?:UserInfo) {
            state.userinfo = userinfo || null;
        },
    },
    namespaced: true,
};

export {
    YandexState,
    vuexModule,
};
